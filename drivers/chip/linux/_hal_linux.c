#include "drivers/chip/_hal.h"

#define SYS_LOG_DOMAIN "hal"
#undef CONFIG_SYS_LOG_DBG
#include "sys_log.h"

#include <stdlib.h>

void drv_hal_init(void)
{
    /* sys clk */
    do
    {
    } while (0);

    /* GPIO */
    do
    {
    } while (0);

    /* console */
    do
    {
        drv_uart_init(0, NULL);
    } while (0);

    /* wake up source */
    do
    {
    } while (0);
}

void drv_hal_debug_enable(void)
{
    SYS_LOG("");
}

void drv_hal_debug_disable(void)
{
    SYS_LOG("");
}

void drv_hal_sys_reset(void)
{
    SYS_LOG("");
}

void drv_hal_sys_exit(int status)
{
    SYS_LOG("");
    drv_uart_deinit(0);
    exit(status);
}
