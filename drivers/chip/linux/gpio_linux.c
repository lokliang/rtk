#include "drivers/chip/gpio.h"

#define SYS_LOG_DOMAIN "hal"
#include "sys_log.h"

static int pin_value[32];

void drv_gpio_enable_all(void)
{
}

void drv_gpio_disable_all(void)
{
}

int drv_gpio_pin_configure(uint8_t pin, gpio_dir_t dir, gpio_pud_t pull)
{
    if (pin >= 255)
    {
        return -1;
    }
    pin_value[pin] = (pull == _GPIO_PUD_PULL_UP);
    return 0;
}

int drv_gpio_pin_read(uint8_t pin)
{
    if (pin >= 255)
    {
        return -1;
    }
    return pin_value[pin];
}

void drv_gpio_pin_write(uint8_t pin, int value)
{
    if (pin >= 255)
    {
        return;
    }
    pin_value[pin] = value;
}

void drv_gpio_exti_callback_enable(gpio_exti_cb_fn cb)
{
}

void drv_gpio_exti_callback_disable(void)
{
}

int drv_gpio_exti_get_pin(void)
{
    return -1;
}
