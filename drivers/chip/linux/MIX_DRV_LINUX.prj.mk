MIX_DRV_LINUX-INC-ALL ?= y
MIX_DRV_LINUX-SRC-ALL ?= y

inc-$(MIX_DRV_LINUX-INC-ALL) += 
src-$(MIX_DRV_LINUX-SRC-ALL) += 

src-$(MIX_SHELL) += ext/bash_shell.c

src-$(MIX_FATFS) += ext/disk_fatfs_img.c
src-$(MIX_FATFS) += ext/disk_fatfs_ram.c

