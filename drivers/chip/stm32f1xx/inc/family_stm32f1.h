#ifndef __FAMILY_STM32F1_H__
#define __FAMILY_STM32F1_H__

#if defined(STM32F101x6) || defined(STM32F102x6) || defined(STM32F103x6)

#define FAMILY_STM32F1 1

#elif defined(STM32F100xB) || defined(STM32F101xB) || defined(STM32F102xB) || defined(STM32F103xB)

#define FAMILY_STM32F1 2

#elif defined(STM32F100xE) || defined(STM32F101xE) || defined(STM32F103xE) || defined(STM32F101xG) || defined(STM32F103xG)

#define FAMILY_STM32F1 3

#elif defined(STM32F105xC) || defined(STM32F107xC)

#define FAMILY_STM32F1 4

#else

#error "Undefined STM32F1xx used"

#endif

#endif
