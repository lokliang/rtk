#ifndef __REMAP_STM32F1_H__
#define __REMAP_STM32F1_H__

#include "sys_types.h"
#include "stm32f1xx.h"

typedef enum
{
    AI = 0x0,    //模拟输入
    FI = 0x4,    //浮空输入
    PI = 0x8,    //带拉电阻输入
    GOP2 = 0x1,  //通用推挽输出 2MHz
    GOP10 = 0x2, //通用推挽输出 10MHz
    GOP50 = 0x3, //通用推挽输出 50MHz
    GOD2 = 0x5,  //通用开漏输出 2MHz
    GOD10 = 0x6, //通用开漏输出 10MHz
    GOD50 = 0x7, //通用开漏输出 50MHz
    AOP2 = 0x9,  //复用推挽输出 2MHz
    AOP10 = 0xa, //复用推挽输出 10MHz
    AOP50 = 0xb, //复用推挽输出 50MHz
    AOD2 = 0xd,  //复用开漏输出 2MHz
    AOD10 = 0xe, //复用开漏输出 10MHz
    AOD50 = 0xf, //复用开漏输出 50MHz
} GPIO_MODE;

#define SPI1_REMAP(n) writebits(&AFIO->MAPR, 0, 1, n)
#define I2C1_REMAP(n) writebits(&AFIO->MAPR, 1, 1, n)
#define USART1_REMAP(n) writebits(&AFIO->MAPR, 2, 1, n)
#define USART2_REMAP(n) writebits(&AFIO->MAPR, 3, 1, n)
#define USART3_REMAP(n) writebits(&AFIO->MAPR, 4, 2, n)
#define TIMER1_REMAP(n) writebits(&AFIO->MAPR, 6, 2, n)
#define TIMER2_REMAP(n) writebits(&AFIO->MAPR, 8, 2, n)
#define TIMER3_REMAP(n) writebits(&AFIO->MAPR, 10, 2, n)
#define TIMER4_REMAP(n) writebits(&AFIO->MAPR, 12, 1, n)
#define CAN_REMAP(n) writebits(&AFIO->MAPR, 13, 2, n)
#define PD01_REMAP(n) writebits(&AFIO->MAPR, 15, 1, n)
#define TIMER5CH4_IREMAP(n) writebits(&AFIO->MAPR, 16, 1, n)
#define ADC1_ETRGINJ_REMAP(n) writebits(&AFIO->MAPR, 17, 1, n)
#define ADC1_ETRGREG_REMAP(n) writebits(&AFIO->MAPR, 18, 1, n)
#define ADC2_ETRGINJ_REMAP(n) writebits(&AFIO->MAPR, 19, 1, n)
#define ADC2_ETRGREG_REMAP(n) writebits(&AFIO->MAPR, 20, 1, n)
#define ETH_REMAP(n) writebits(&AFIO->MAPR, 21, 1, n)
#define CAN2_REMAP(n) writebits(&AFIO->MAPR, 22, 1, n)
#define MII_RMII_REMAP(n) writebits(&AFIO->MAPR, 23, 1, n)
#define SWJ_CFG(n) writebits(&AFIO->MAPR, 24, 3, n)
#define SPI3_REMAP(n) writebits(&AFIO->MAPR, 28, 1, n)
#define TIMER2ITR1_REMAP(n) writebits(&AFIO->MAPR, 29, 1, n)
#define PTP_PPS_REMAP(n) writebits(&AFIO->MAPR, 30, 1, n)

#define TIMER9_REMAP(n) writebits(&AFIO->MAPR2, 5, 1, n)
#define TIMER10_REMAP(n) writebits(&AFIO->MAPR2, 6, 1, n)
#define TIMER11_REMAP(n) writebits(&AFIO->MAPR2, 7, 1, n)
#define TIMER13_REMAP(n) writebits(&AFIO->MAPR2, 8, 1, n)
#define TIMER14_REMAP(n) writebits(&AFIO->MAPR2, 9, 1, n)
#define EXMC_NADV(n) writebits(&AFIO->MAPR2, 10, 1, n)

#endif
