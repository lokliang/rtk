 #include "drivers/chip/misc.h"
 #include "sys_types.h"
 #include "stm32f1xx.h"

 volatile unsigned *drv_misc_bitband(void *mem)
 {
     return ((volatile unsigned *)((((unsigned)(mem)&0x60000000) | 0x02000000) + (((unsigned)(mem)&0xFFFFF) * 32)));
 }

 unsigned drv_misc_read_mcuid(void *dest)
 {
     unsigned i;
     for (i = 0; i < 12; i++)
     {
         ((u8_t *)dest)[i] = ((u8_t *)0x1FFFF7E8)[i];
     }
     return (i);
 }

 void drv_misc_set_vector(void *vector)
 {
     SCB->VTOR = (uint)vector;
 }
