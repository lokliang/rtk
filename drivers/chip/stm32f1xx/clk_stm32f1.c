#include "drivers/chip/clk.h"
#include "sys_log.h"

#include "system_stm32f1xx.h"

unsigned drv_clk_get_cpu_clk(void)
{
    SystemCoreClockUpdate();
    return SystemCoreClock;
}
