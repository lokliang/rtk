 #include "drivers/chip/irq.h"
#include "hal_cmsis.h"

#define SYS_LOG_DOMAIN "hal"
#include "sys_log.h"

static unsigned irq_nest = 0;

unsigned drv_irq_disable(void)
{
    if (irq_nest == 0)
    {
        __disable_irq();
    }
    return irq_nest++;
}

void drv_irq_enable(unsigned nest)
{
    irq_nest = nest;
    if (nest == 0)
    {
        __enable_irq();
    }
}

unsigned drv_irq_get_nest(void)
{
    return irq_nest;
}
