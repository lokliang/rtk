#include "drivers/chip/wdt.h"

#define SYS_LOG_DOMAIN "qemu"
#include "sys_log.h"

void wdt_enable(void)
{
    SYS_LOG("");
}

void wdt_disable(void)
{
    SYS_LOG("");
}

void wdt_set_config(wdt_mode_t mode, int int_time, int reset_time)
{
    SYS_LOG("int_time = %d, reset_time = %d", int_time, reset_time);
}

void wdt_reload(void)
{
    SYS_LOG("");
}

unsigned wdt_get_time_cost(void)
{
    SYS_LOG("");
    return 0;
}
