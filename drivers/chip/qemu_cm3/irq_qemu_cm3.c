#include "drivers/chip/irq.h"
#include "hal_cmsis.h"

#define SYS_LOG_DOMAIN "qemu"
#include "sys_log.h"

static unsigned irq_nest = 0;

unsigned drv_irq_disable(void)
{
    __disable_irq();
    return irq_nest++;
}

void drv_irq_enable(unsigned nest)
{
    if (nest == 0)
    {
        irq_nest = nest;
        __enable_irq();
    }
    else
    {
        irq_nest = nest;
    }
}

unsigned drv_irq_get_nest(void)
{
    return irq_nest;
}
