#include "drivers/chip/tim.h"

#define SYS_LOG_DOMAIN "qemu"
#include "sys_log.h"

// /**
//  * @brief 通过时间计算预分频和重装寄存器的值
//  *
//  * @param[out] Prescaler 预分频寄存器
//  * @param[out] Period 重装寄存器
//  * @param ns 预期的时间（纳秒）
//  */
// static void _tim_calc_time(uint16_t *Prescaler, uint16_t *Period, uint32_t ns)
// {
//     SYS_LOG("");
// }

// /**
//  * @brief 通过频率计算预分频和重装寄存器的值
//  *
//  * @param[out] Prescaler 预分频寄存器
//  * @param[out] Period 重装寄存器
//  * @param freq 预期的频率（Hz）
//  */
// static void _tim_calc_freq(uint16_t *Prescaler, uint16_t *Period, uint32_t freq)
// {
// SYS_LOG("");
// }

/** @defgroup time base
 * @{
 */

void drv_tim_enable(hal_id_t id)
{
    SYS_LOG("");
}

void drv_tim_disable(hal_id_t id)
{
    SYS_LOG("");
}

void drv_tim_init(hal_id_t id, unsigned period_ns, unsigned reload_enable)
{
    SYS_LOG("");
}

void drv_tim_deinit(hal_id_t id)
{
    SYS_LOG("");
}

void drv_tim_irq_enable(hal_id_t id, int priority)
{
    SYS_LOG("");
}

void timtim_irq_disable(hal_id_t id)
{
    SYS_LOG("");
}

/**
 * @}
 */

/** @defgroup PWM
 * @{
 */

void drv_pwm_pin_configure(hal_id_t id, uint8_t pin)
{
    SYS_LOG("");
}

void drv_pwm_enable(hal_id_t id)
{
    SYS_LOG("");
}

void drv_pwm_disable(hal_id_t id)
{
    SYS_LOG("");
}

void drv_pwm_init(hal_id_t id, unsigned period_ns, unsigned reload_enable)
{
    SYS_LOG("");
}

void drv_pwm_deinit(hal_id_t id)
{
    SYS_LOG("");
}

void drv_pwm_irq_enable(hal_id_t id, unsigned channel_mask, int priority)
{
    SYS_LOG("");
}

void drv_pwm_irq_disable(hal_id_t id, unsigned channel_mask)
{
    SYS_LOG("");
}

void drv_pwm_start(hal_id_t id, unsigned channel_mask, unsigned active_level, unsigned pulse_ns)
{
    SYS_LOG("");
}

void drv_pwm_stop(hal_id_t id, unsigned channel_mask, unsigned active_level)
{
    SYS_LOG("");
}

bool drv_pwm_is_busy(hal_id_t id)
{
    SYS_LOG("");
    return false;
}

/**
 * @}
 */

/** @defgroup one pluse
 * @{
 */

void drv_pluse_pin_configure(hal_id_t id, uint8_t pin)
{
    SYS_LOG("");
}

void drv_pluse_enable(hal_id_t id)
{
    SYS_LOG("");
}

void drv_pluse_disable(hal_id_t id)
{
    SYS_LOG("");
}

void drv_pluse_init(hal_id_t id)
{
    SYS_LOG("");
}

void drv_pluse_deinit(hal_id_t id)
{
    SYS_LOG("");
}

void drv_pluse_irq_enable(hal_id_t id, unsigned channel_mask, int priority)
{
    SYS_LOG("");
}

void drv_pluse_irq_disable(hal_id_t id, unsigned channel_mask)
{
    SYS_LOG("");
}

void drv_pluse_set(hal_id_t id, unsigned channel_mask, unsigned active_level, unsigned period_ns, unsigned pulse_ns)
{
    SYS_LOG("");
}

bool drv_pluse_is_busy(hal_id_t id, unsigned channel)
{
    SYS_LOG("");
    return false;
}

/**
 * @}
 */

/** @defgroup input capture
 * @{
 */

/**
 * @}
 */
