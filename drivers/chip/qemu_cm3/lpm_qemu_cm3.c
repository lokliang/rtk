#include "drivers/chip/lpm.h"

#define SYS_LOG_DOMAIN "qemu"
#include "sys_log.h"

void drv_lpm_wkup_enable(unsigned id)
{
    SYS_LOG("");
}

void drv_lpm_wkup_disable(unsigned id)
{
    SYS_LOG("");
}

void drv_lpm_sleep(void)
{
    SYS_LOG("");
}

void drv_lpm_deep_sleep(void)
{
    SYS_LOG("");
}
