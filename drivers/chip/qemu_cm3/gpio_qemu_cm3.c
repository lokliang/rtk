#include "drivers/chip/gpio.h"

#define SYS_LOG_DOMAIN "qemu"
#include "sys_log.h"

void drv_gpio_enable_all(void)
{
    SYS_LOG("");
}

void drv_gpio_disable_all(void)
{
    SYS_LOG("");
}

int drv_gpio_pin_configure(uint8_t pin, gpio_dir_t dir, gpio_pud_t pull)
{
    if (pin >= 255)
    {
        return -1;
    }
    SYS_LOG("");
    return 0;
}

int drv_gpio_pin_read(uint8_t pin)
{
    if (pin >= 255)
    {
        return -1;
    }
    return 0;
}

void drv_gpio_pin_write(uint8_t pin, int value)
{
    if (pin >= 255)
    {
        return;
    }
    SYS_LOG("");
}

void drv_gpio_exti_callback_enable(gpio_exti_cb_fn cb)
{
    SYS_LOG("");
}

void drv_gpio_exti_callback_disable(void)
{
    SYS_LOG("");
}

int drv_gpio_exti_get_pin(void)
{
    return -1;
}
