#include "drivers/chip/_hal.h"

#define SYS_LOG_DOMAIN "qemu"
#include "sys_log.h"

void drv_spi_pin_configure_sck(hal_id_t id, uint8_t pin)
{
    if (id >= 0xFF || pin >= 255)
    {
        return;
    }
    SYS_LOG("");
}

void drv_spi_pin_configure_mosi(hal_id_t id, uint8_t pin)
{
    if (id >= 0xFF || pin >= 255)
    {
        return;
    }
    SYS_LOG("");
}

void drv_spi_pin_configure_miso(hal_id_t id, uint8_t pin)
{
    if (id >= 0xFF || pin >= 255)
    {
        return;
    }
    SYS_LOG("");
}

void drv_spi_enable(hal_id_t id)
{
    if (id >= 0xFF)
    {
        return;
    }
    SYS_LOG("");
}

void drv_spi_disable(hal_id_t id)
{
    if (id >= 0xFF)
    {
        return;
    }
    SYS_LOG("");
}

void drv_spi_init(hal_id_t id, const spi_init_t *param)
{
    if (id >= 0xFF)
    {
        return;
    }
    SYS_LOG("");
}

void drv_spi_deinit(hal_id_t id)
{
    if (id >= 0xFF)
    {
        return;
    }
    SYS_LOG("");
}

void drv_spi_irq_enable(hal_id_t id, bool rx, bool tx, int priority)
{
    if (id >= 0xFF)
    {
        return;
    }
    SYS_LOG("");
}

void drv_spi_irq_disable(hal_id_t id, bool rx, bool tx)
{
    if (id >= 0xFF)
    {
        return;
    }
    SYS_LOG("");
}

void drv_spi_dma_enable(hal_id_t id, bool tx_dma, bool rx_dma, unsigned prio_level)
{
    if (id >= 0xFF)
    {
        return;
    }
    SYS_LOG("");
}

void drv_spi_dma_disable(hal_id_t id, bool tx_dma, bool rx_dma)
{
    if (id >= 0xFF)
    {
        return;
    }
    SYS_LOG("");
}

int drv_spi_poll_read(hal_id_t id, void *dest)
{
    if (id >= 0xFF)
    {
        return -1;
    }
    SYS_LOG("");
    return 0;
}

int drv_spi_poll_write(hal_id_t id, uint8_t data)
{
    if (id >= 0xFF)
    {
        return -1;
    }
    SYS_LOG("");
    return 0;
}
