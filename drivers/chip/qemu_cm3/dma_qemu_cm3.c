#include "drivers/chip/dma.h"

#define SYS_LOG_DOMAIN "qemu"
#include "sys_log.h"

void drv_dma_enable(hal_id_t id)
{
    if (id >= 0xFF)
    {
        return;
    }
    SYS_LOG("");
}

void drv_dma_disable(hal_id_t id)
{
    if (id >= 0xFF)
    {
        return;
    }
    SYS_LOG("");
}

int drv_dma_init(hal_id_t id, unsigned channel, unsigned prio_level)
{
    if (id >= 0xFF)
    {
        return -1;
    }
    SYS_LOG("");
    return 0;
}

int drv_dma_deinit(hal_id_t id, unsigned channel)
{
    if (id >= 0xFF)
    {
        return -1;
    }
    SYS_LOG("");
    return 0;
}

void drv_dma_irq_enable(hal_id_t id, unsigned channel, int priority)
{
    if (id >= 0xFF)
    {
        return;
    }
    SYS_LOG("");
}

void drv_dma_irq_disable(hal_id_t id, unsigned channel)
{
    if (id >= 0xFF)
    {
        return;
    }
    SYS_LOG("");
}

void drv_dma_set(hal_id_t id, unsigned channel, void *dest, unsigned dest_bit_width, bool dest_inc_mode, const void *src, unsigned src_bit_width, bool src_inc_mode, unsigned count, dma_dir_t dir)
{
    if (id >= 0xFF)
    {
        return;
    }
    SYS_LOG("");
}

void drv_dma_start(hal_id_t id, unsigned channel)
{
    if (id >= 0xFF)
    {
        return;
    }
    SYS_LOG("");
}

void drv_dma_stop(hal_id_t id, unsigned channel)
{
    if (id >= 0xFF)
    {
        return;
    }
    SYS_LOG("");
}

unsigned drv_dma_get_remain(hal_id_t id, unsigned channel)
{
    if (id >= 0xFF)
    {
        return 0;
    }
    SYS_LOG("");
    return 0;
}
