#include "drivers/chip/adc.h"

#define SYS_LOG_DOMAIN "qemu"
#include "sys_log.h"

void drv_adc_enable(hal_id_t id)
{
    if (id >= 0xFF)
    {
        return;
    }
    SYS_LOG("");
}

void drv_adc_disable(hal_id_t id)
{
    if (id >= 0xFF)
    {
        return;
    }
    SYS_LOG("");
}

void drv_adc_init(hal_id_t id, unsigned channel_mask, uint32_t sampling_length)
{
    if (id >= 0xFF || channel_mask == 0)
    {
        return;
    }
    SYS_LOG("");
}

void drv_adc_deinit(hal_id_t id, unsigned channel_mask)
{
    if (id >= 0xFF || channel_mask == 0)
    {
        return;
    }
    SYS_LOG("");
}

void drv_adc_start(hal_id_t id, unsigned channel_mask)
{
    if (id >= 0xFF || channel_mask == 0)
    {
        return;
    }
    SYS_LOG("");
}

bool drv_adc_is_busy(hal_id_t id, uint8_t channel_id)
{
    if (id >= 0xFF)
    {
        return false;
    }
    SYS_LOG("");
    return false;
}

bool drv_adc_is_ready(hal_id_t id, uint8_t channel_id)
{
    if (id >= 0xFF)
    {
        return true;
    }
    SYS_LOG("");
    return true;
}

int drv_adc_read(hal_id_t id, uint8_t channel_id)
{
    if (id >= 0xFF || channel_id > 31)
    {
        return 0;
    }
    SYS_LOG("");
    return 0;
}
