#include "drivers/chip/tick.h"
#include "drivers/chip/clk.h"

#include "hal_cmsis.h"

void drv_tick_enable(unsigned freq)
{
    SysTick_Config(drv_clk_get_cpu_clk() / freq);
}

void drv_tick_disable(void)
{
    SysTick->CTRL = 0;
}

unsigned drv_tick_get_counter(void)
{
    return SysTick->VAL;
}
