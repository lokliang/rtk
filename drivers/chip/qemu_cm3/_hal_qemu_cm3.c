#include "board_config.h"
#include "drivers/chip/_hal.h"

#define SYS_LOG_DOMAIN "qemu"
#include "sys_log.h"

void drv_hal_init(void)
{
    /* sys clk */
    do
    {
    } while (0);

    /* GPIO */
    do
    {
    } while (0);

    /* console */
    do
    {
        uart_param_t param = {
            .br = g_board_uart_cons.br,
            .length = 8,
            .stop_bit = 1,
            .parity = 0,
        };
        drv_uart_enable(g_board_uart_cons.id);
        drv_uart_init(g_board_uart_cons.id, &param);
    } while (0);

    /* wake up source */
    do
    {
    } while (0);
}

void drv_hal_debug_enable(void)
{
    SYS_LOG("");
}

void drv_hal_debug_disable(void)
{
    SYS_LOG("");
}

void drv_hal_sys_reset(void)
{
    SYS_LOG("");
}

void drv_hal_sys_exit(int status)
{
    SYS_LOG("");
    void exit(int status);
    exit(status);
}
