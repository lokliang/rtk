MIX_STORAGE_SFUD-ALL ?= y

inc-$(MIX_STORAGE_SFUD-ALL) += sfud/inc/
src-$(MIX_STORAGE_SFUD-ALL) += sfud/src/
src-$(MIX_STORAGE_SFUD-ALL) += nor_sfud.c
src-$(MIX_STORAGE_SFUD-ALL) += port_sfud.c
