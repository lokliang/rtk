MIX_XZ-INC-ALL ?= y
MIX_XZ-SRC-ALL ?= y

inc-$(MIX_XZ-INC-ALL) += inc/
inc-$(MIX_XZ-SRC-ALL) += src/
src-$(MIX_XZ-SRC-ALL) += src/

MIX-AND-yy := y
src-$(MIX-AND-$(MIX_SHELL)$(MIX_DRV_LINUX)) += xz_shell_linux.c

obj-n += 
