MIX_FATFS-ALL ?= y

inc-$(MIX_FATFS-ALL) += 
src-$(MIX_FATFS-ALL) += fs.c
src-$(MIX_FATFS-ALL) += fs_fatfs.c
src-$(MIX_FATFS-ALL) += fs_init_fatfs.c

inc-$(MIX_FATFS-ALL) += FatFs/source/
src-$(MIX_FATFS-ALL) += FatFs/source/

src-$(MIX_SHELL) += fatfs_shell.c

obj-n += 
