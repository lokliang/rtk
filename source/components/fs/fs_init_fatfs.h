#ifndef __FS_INIT_FATFS_H__
#define __FS_INIT_FATFS_H__

#include "drivers/ext/disk_fatfs.h"

int f_disk_regist(device_disk_fatfs_t dev, const char *volume_name, int id);
int f_disk_unregist(const char *volume_name);

int f_disk_get_id(device_disk_fatfs_t dev);
int f_disk_get_id_by_name(const char *volume_name);

device_disk_fatfs_t f_disk_get_dev(int id);

const TCHAR *f_disk_get_volume(int id);

#endif
