#include "os/os.h"
#include "os_util.h"
#include "rtk.h"

os_state os_work_q_create(os_work_q_t *work_q_handle,
                          const char *name,
                          size_t stack_size,
                          os_priority priority)
{
    if (rtk_work_q_create((rtk_work_q_t *)work_q_handle,
                          name,
                          stack_size,
                          priority) == RTK_OK)
        return OS_OK;
    else
        return OS_FAIL;
}

os_state os_work_q_delete(os_work_q_t *work_q_handle)
{
    if (rtk_work_q_delete((rtk_work_q_t *)work_q_handle) == RTK_OK)
        return OS_OK;
    else
        return OS_FAIL;
}

bool os_work_q_is_valid(os_work_q_t *work_q_handle)
{
    return rtk_work_q_is_valid((rtk_work_q_t *)work_q_handle);
}

bool os_work_q_delayed_state(os_work_q_t *work_q_handle)
{
    return rtk_work_q_delayed_state((rtk_work_q_t *)work_q_handle);
}

bool os_work_q_ready_state(os_work_q_t *work_q_handle)
{
    return rtk_work_q_ready_state((rtk_work_q_t *)work_q_handle);
}

os_state os_work_create(os_work_t *work_handle, const char *name, os_work_fn work_route, void *arg, uint8_t sub_prior)
{
    if (rtk_work_create((rtk_work_t *)work_handle, name, (rtk_work_fn)work_route, arg, sub_prior) == RTK_OK)
        return OS_OK;
    else
        return OS_FAIL;
}

os_state os_work_create_default(os_work_t *work_handle, const char *name, os_work_fn work_route, void *arg, uint8_t sub_prior, os_time_t delay_ms)
{
    if (!os_work_is_valid(work_handle))
    {
        os_state ret = os_work_create(work_handle, name, work_route, arg, sub_prior);
        if (ret == OS_OK)
        {
            os_work_submit(default_os_work_q_hdl, work_handle, delay_ms);
        }
        return ret;
    }
    else
    {
        return OS_OK;
    }
}

void os_work_delete(os_work_t *work_handle)
{
    rtk_work_delete((rtk_work_t *)work_handle);
}

bool os_work_is_valid(os_work_t *work_handle)
{
    return rtk_work_is_valid((rtk_work_t *)work_handle);
}

bool os_work_is_pending(os_work_t *work_handle)
{
    return rtk_work_is_pending((rtk_work_t *)work_handle);
}

os_time_t os_work_time_remain(os_work_t *work_handle)
{
    return rtk_ticks_to_msec(rtk_work_time_remain((rtk_work_t *)work_handle));
}

void os_work_submit(os_work_q_t *work_q_handle, os_work_t *work_handle, os_time_t delay_ms)
{
    rtk_work_submit((rtk_work_q_t *)work_q_handle, (rtk_work_t *)work_handle, rtk_msec_to_ticks(delay_ms));
}

void os_work_resume(os_work_t *work_handle, os_time_t delay_ms)
{
    rtk_work_resume((rtk_work_t *)work_handle, rtk_msec_to_ticks(delay_ms));
}

void os_work_suspend(os_work_t *work_handle)
{
    rtk_work_suspend((rtk_work_t *)work_handle);
}

void os_work_yield(os_time_t ms)
{
    rtk_work_yield(rtk_msec_to_ticks(ms));
}

void os_work_sleep(os_time_t ms)
{
    rtk_work_sleep(rtk_msec_to_ticks(ms));
}

void os_work_later(os_time_t ms)
{
    rtk_work_later(rtk_msec_to_ticks(ms));
}

void os_work_later_until(os_time_t ms)
{
    rtk_work_later_until(rtk_msec_to_ticks(ms));
}
