#include "os/os.h"
#include "os_util.h"
#include "rtk.h"

void *os_malloc(size_t size)
{
    return rtk_heap_malloc(size, 0);
}

void *os_calloc(size_t size)
{
    return rtk_heap_calloc(size, 0);
}

void *os_realloc(void *p, size_t size)
{
    return rtk_heap_realloc(p, size, 0);
}

void os_free(void *p)
{
    rtk_heap_free(p);
}

void os_heap_info(size_t *used_size, size_t *free_size, size_t *max_block_size)
{
    rtk_heap_info(used_size, free_size, max_block_size);
}
