#include "os/os.h"
#include "os_util.h"
#include "rtk.h"

#include "prv_rtk_macro.h"

os_work_q_t *default_os_work_q_hdl;

int os_start(void *heap_mem, size_t heap_size)
{
    default_os_work_q_hdl = (os_work_q_t *)default_rtk_work_q_hdl;
    return rtk_entry(heap_mem, heap_size);
}

void os_int_entry(void)
{
    rtk_int_entry();
}

void os_int_exit(void)
{
    rtk_int_exit();
}

bool os_is_isr_context(void)
{
    return rtk_is_isr_context();
}

void os_interrupt_disable(void)
{
    rtk_int_save();
}

void os_interrupt_enable(void)
{
    rtk_int_restore_auto();
}

void os_scheduler_suspend(void)
{
    rtk_sched_suspend();
}

void os_scheduler_resume(void)
{
    rtk_sched_resume_auto();
}

bool os_scheduler_is_running(void)
{
    return (rtk_scheduler_state() == RTK_SCHED_STATE_RUNNING);
}

void os_sys_print_info(void)
{
    rtk_service_print_sysinfo();
}

os_time_t os_get_sys_time(void)
{
    unsigned sys_ticks = rtk_get_sys_ticks();
    return rtk_ticks_to_msec(sys_ticks);
}

size_t os_get_sys_ticks(void)
{
    return rtk_get_sys_ticks();
}

os_time_t os_calc_ticks_to_msec(size_t ticks)
{
    return rtk_ticks_to_msec(ticks);
}

size_t os_calc_msec_to_ticks(os_time_t msec)
{
    return rtk_msec_to_ticks(msec);
}

size_t os_cpu_usage(void)
{
    return rtk_get_cpu_usage();
}

int os_get_err(void)
{
    return rtk_thread_get_errno();
}

void os_set_err(int err)
{
    rtk_thread_set_errno(err);
}
