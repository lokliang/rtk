#include "os/os.h"
#include "os_util.h"
#include "rtk.h"

os_state os_timer_create(os_timer_t *timer, os_timer_cb_fn cb, void *arg)
{
    if (rtk_timer_create((rtk_timer_t *)timer, cb, arg, 0) == RTK_OK)
        return OS_OK;
    else
        return OS_FAIL;
}

os_state os_timer_delete(os_timer_t *timer)
{
    if (rtk_timer_delete((rtk_timer_t *)timer) == RTK_OK)
        return OS_OK;
    else
        return OS_FAIL;
}

os_state os_timer_set_period(os_timer_t *timer, os_timer_type_t type, os_time_t period_ms)
{
    if (rtk_timer_set_period((rtk_timer_t *)timer,
                             type == OS_TIMER_ONCE ? false : true,
                             rtk_msec_to_ticks(period_ms)) == RTK_OK)
        return OS_OK;
    else
        return OS_FAIL;
}

os_state os_timer_start(os_timer_t *timer)
{
    if (rtk_timer_start((rtk_timer_t *)timer, rtk_timer_get_period((rtk_timer_t *)timer)) == RTK_OK)
        return OS_OK;
    else
        return OS_FAIL;
}

os_state os_timer_stop(os_timer_t *timer)
{
    if (rtk_timer_stop((rtk_timer_t *)timer) == RTK_OK)
        return OS_OK;
    else
        return OS_FAIL;
}

bool os_timer_is_pending(os_timer_t *timer)
{
    return rtk_timer_is_pending((rtk_timer_t *)timer);
}

bool os_timer_is_valid(os_timer_t *timer)
{
    return rtk_timer_is_valid((rtk_timer_t *)timer);
}
