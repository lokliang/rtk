#include "os/os.h"
#include "os_util.h"
#include "rtk.h"

os_state os_thread_init(os_thread_t *thread,
                        const char *name,
                        os_thread_entry_t entry,
                        void *arg,
                        void *stack_base,
                        size_t stack_size,
                        os_priority priority)
{
    if (rtk_thread_init((rtk_thread_t *)thread,
                        name,
                        entry,
                        arg,
                        stack_base,
                        stack_size,
                        priority,
                        0) != NULL)
        return OS_OK;
    else
        return OS_FAIL;
}

os_state os_thread_create(os_thread_t *thread,
                          const char *name,
                          os_thread_entry_t entry,
                          void *arg,
                          size_t stack_size,
                          os_priority priority)
{
    if (rtk_thread_create((rtk_thread_t *)thread,
                          name,
                          entry,
                          arg,
                          stack_size,
                          priority,
                          0) != NULL)
        return OS_OK;
    else
        return OS_FAIL;
}

os_state os_thread_delete(os_thread_t *thread)
{
    if (rtk_thread_delete((rtk_thread_t *)thread) == RTK_OK)
        return OS_OK;
    else
        return OS_FAIL;
}

void os_thread_sleep(os_time_t msec)
{
    rtk_thread_sleep(rtk_msec_to_ticks(msec));
}

void os_thread_yield(void)
{
    rtk_thread_yield();
}

os_thread_handle_t os_thread_get_self(void)
{
    return (os_thread_handle_t)rtk_thread_get_self()->hdl;
}

void os_thread_suspend(os_thread_t *thread)
{
    rtk_thread_set_suspend((rtk_thread_t *)thread);
}

void os_thread_resume(os_thread_t *thread)
{
    rtk_thread_resume((rtk_thread_t *)thread);
}

const char *os_thread_get_name(os_thread_t *thread)
{
    return rtk_thread_get_name((rtk_thread_t *)thread);
}

size_t os_thread_stack_min(os_thread_t *thread)
{
    return rtk_thread_stack_empty_size((rtk_thread_t *)thread);
}

bool os_thread_is_valid(os_thread_t *thread)
{
    return rtk_thread_is_valid((rtk_thread_t *)thread);
}
