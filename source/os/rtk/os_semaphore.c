#include "os/os.h"
#include "os_util.h"
#include "rtk.h"

os_state os_sem_create(os_sem_t *sem, size_t init_value, size_t max_value)
{
    if (rtk_sem_create((rtk_sem_t *)sem, "Sem", init_value, max_value, RTK_SEM_TYPE_PRIOR) == RTK_OK)
        return OS_OK;
    else
        return OS_FAIL;
}

os_state os_sem_delete(os_sem_t *sem)
{
    if (rtk_sem_delete((rtk_sem_t *)sem) == RTK_OK)
        return OS_OK;
    else
        return OS_FAIL;
}

os_state os_sem_take(os_sem_t *sem, os_time_t wait_ms)
{
    if (rtk_sem_take((rtk_sem_t *)sem, rtk_msec_to_ticks(wait_ms)) == RTK_OK)
        return OS_OK;
    else
        return OS_FAIL;
}

os_state os_sem_release(os_sem_t *sem)
{
    if (rtk_sem_release((rtk_sem_t *)sem) == RTK_OK)
        return OS_OK;
    else
        return OS_FAIL;
}

bool os_sem_is_valid(os_sem_t *sem)
{
    return rtk_sem_is_valid((rtk_sem_t *)sem);
}
