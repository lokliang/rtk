#ifndef __OS_UTIL_H__
#define __OS_UTIL_H__

#include "os/os_common.h"

#include "rtk_config.h"

#define OS_TICK_RATE CONFIG_RTK_TICKS_HZ

#endif /* __OS_UTIL_H__ */
