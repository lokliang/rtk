#include "os/os.h"
#include "os_util.h"
#include "rtk.h"

os_state os_fifo_q_create(os_fifo_t *fifo_handle)
{
    if (rtk_fifo_q_create((rtk_fifo_t *)fifo_handle, "FIFO", RTK_SEM_TYPE_FIFO) == RTK_OK)
        return OS_OK;
    else
        return OS_FAIL;
}

os_state os_fifo_q_delete(os_fifo_t *fifo_handle)
{
    if (rtk_fifo_q_delete((rtk_fifo_t *)fifo_handle) == RTK_OK)
        return OS_OK;
    else
        return OS_FAIL;
}

os_state os_fifo_q_clr(os_fifo_t *fifo_handle)
{
    if (rtk_fifo_q_clr((rtk_fifo_t *)fifo_handle) == RTK_OK)
        return OS_OK;
    else
        return OS_FAIL;
}

void os_fifo_q_regist(os_fifo_t *fifo_handle, os_work_t *work_handle, int delay_ticks)
{
    rtk_fifo_q_regist((rtk_fifo_t *)fifo_handle, (rtk_work_t *)work_handle, delay_ticks);
}

void os_fifo_q_unregist(os_fifo_t *fifo_handle)
{
    rtk_fifo_q_unregist((rtk_fifo_t *)fifo_handle);
}

void *os_fifo_alloc(size_t size)
{
    return rtk_fifo_alloc(size, 0);
}

os_state os_fifo_put(os_fifo_t *fifo_handle, void *fifo_data)
{
    if (rtk_fifo_put((rtk_fifo_t *)fifo_handle, fifo_data) == RTK_OK)
        return OS_OK;
    else
        return OS_FAIL;
}

void *os_fifo_take(os_fifo_t *fifo_handle, os_time_t wait_ms)
{
    return rtk_fifo_take((rtk_fifo_t *)fifo_handle, rtk_msec_to_ticks(wait_ms));
}

os_state os_fifo_free(void *fifo_data)
{
    if (rtk_fifo_free(fifo_data) == RTK_OK)
        return OS_OK;
    else
        return OS_FAIL;
}

void *os_fifo_peek_head(os_fifo_t *fifo_handle)
{
    return rtk_fifo_peek_head((rtk_fifo_t *)fifo_handle);
}

void *os_fifo_peek_tail(os_fifo_t *fifo_handle)
{
    return rtk_fifo_peek_tail((rtk_fifo_t *)fifo_handle);
}

bool os_fifo_q_is_valid(os_fifo_t *fifo_handle)
{
    return rtk_fifo_q_is_valid((rtk_fifo_t *)fifo_handle);
}

os_state os_queue_create(os_queue_t *queue_handle, size_t queueLen, size_t itemSize)
{
    if (rtk_queue_create((rtk_queue_t *)queue_handle,
                         "Queue",
                         RTK_SEM_TYPE_PRIOR,
                         queueLen,
                         itemSize) == RTK_OK)
        return OS_OK;
    else
        return OS_FAIL;
}

os_state os_queue_delete(os_queue_t *queue_handle)
{
    if (rtk_queue_delete((rtk_queue_t *)queue_handle) == RTK_OK)
        return OS_OK;
    else
        return OS_FAIL;
}

os_state os_queue_clr(os_queue_t *queue_handle)
{
    if (rtk_queue_clr((rtk_queue_t *)queue_handle) == RTK_OK)
        return OS_OK;
    else
        return OS_FAIL;
}

void os_queue_regist(os_queue_t *queue_handle, os_work_t *work_handle, int delay_ticks)
{
    rtk_queue_regist((rtk_queue_t *)queue_handle, (rtk_work_t *)work_handle, delay_ticks);
}

void os_queue_unregist(os_queue_t *queue_handle)
{
    rtk_queue_unregist((rtk_queue_t *)queue_handle);
}

os_state os_queue_send(os_queue_t *queue_handle, const void *item, os_time_t wait_ms)
{
    if (rtk_queue_send((rtk_queue_t *)queue_handle, (void *)item, rtk_msec_to_ticks(wait_ms)) == RTK_OK)
        return OS_OK;
    else
        return OS_E_TIMEOUT;
}

os_state os_queue_recv(os_queue_t *queue_handle, void *item, os_time_t wait_ms)
{
    if (rtk_queue_recv((rtk_queue_t *)queue_handle, item, rtk_msec_to_ticks(wait_ms)) == RTK_OK)
        return OS_OK;
    else
        return OS_E_TIMEOUT;
}

void *os_queue_peek_head(os_queue_t *queue_handle)
{
    return rtk_queue_peek_head((rtk_queue_t *)queue_handle);
}

void *os_queue_peek_tail(os_queue_t *queue_handle)
{
    return rtk_queue_peek_tail((rtk_queue_t *)queue_handle);
}

size_t os_queue_get_item_size(os_queue_t *queue_handle)
{
    return rtk_queue_get_item_size((rtk_queue_t *)queue_handle);
}

bool os_queue_is_valid(os_queue_t *queue_handle)
{
    return rtk_queue_is_valid((rtk_queue_t *)queue_handle);
}

os_state os_pipe_create(os_pipe_t *pipe_handle, size_t pipe_size)
{
    if (rtk_pipe_create((rtk_pipe_t *)pipe_handle, "Pipe", pipe_size, RTK_SEM_TYPE_FIFO) == RTK_OK)
        return OS_OK;
    else
        return OS_FAIL;
}

os_state os_pipe_delete(os_pipe_t *pipe_handle)
{
    if (rtk_pipe_delete((rtk_pipe_t *)pipe_handle) == RTK_OK)
        return OS_OK;
    else
        return OS_FAIL;
}

os_state os_pipe_clr(os_pipe_t *pipe_handle)
{
    if (rtk_pipe_clr((rtk_pipe_t *)pipe_handle) == RTK_OK)
        return OS_OK;
    else
        return OS_FAIL;
}

void os_pipe_regist(os_pipe_t *pipe_handle, os_work_t *work_handle, int delay_ticks)
{
    rtk_pipe_regist((rtk_pipe_t *)pipe_handle, (rtk_work_t *)work_handle, delay_ticks);
}

void os_pipe_unregist(os_pipe_t *pipe_handle)
{
    rtk_pipe_unregist((rtk_pipe_t *)pipe_handle);
}

size_t os_pipe_poll_write(os_pipe_t *pipe_handle, uint8_t data)
{
    return rtk_pipe_poll_write((rtk_pipe_t *)pipe_handle, data);
}

size_t os_pipe_fifo_fill(os_pipe_t *pipe_handle, const void *data, size_t size)
{
    return rtk_pipe_fifo_fill((rtk_pipe_t *)pipe_handle, data, size);
}

size_t os_pipe_poll_read(os_pipe_t *pipe_handle, uint8_t *data)
{
    return rtk_pipe_poll_read((rtk_pipe_t *)pipe_handle, data);
}

size_t os_pipe_fifo_read(os_pipe_t *pipe_handle, void *data, size_t size)
{
    return rtk_pipe_fifo_read((rtk_pipe_t *)pipe_handle, data, size);
}

bool os_pipe_is_ne(os_pipe_t *pipe_handle)
{
    return rtk_pipe_is_ne((rtk_pipe_t *)pipe_handle);
}

size_t os_pipe_get_valid_size(os_pipe_t *pipe_handle)
{
    return rtk_pipe_get_valid_size((rtk_pipe_t *)pipe_handle);
}

size_t os_pipe_get_empty_size(os_pipe_t *pipe_handle)
{
    return rtk_pipe_get_empty_size((rtk_pipe_t *)pipe_handle);
}

void os_pipe_peek_valid(os_pipe_t *pipe_handle, void **dst_base, size_t *dst_size)
{
    rtk_pipe_peek_valid((rtk_pipe_t *)pipe_handle, dst_base, dst_size);
}

void os_pipe_peek_empty(os_pipe_t *pipe_handle, void **dst_base, size_t *dst_size)
{
    rtk_pipe_peek_empty((rtk_pipe_t *)pipe_handle, dst_base, dst_size);
}

bool os_pipe_is_valid(os_pipe_t *pipe_handle)
{
    return rtk_pipe_is_valid((rtk_pipe_t *)pipe_handle);
}
