MIX_OS-INC-ALL ?= y
MIX_OS-SRC-ALL ?= y

inc-$(MIX_RTK) += rtk/
src-$(MIX_RTK) += rtk/

inc-$(MIX_FREERTOS-SRC) += FreeRTOS/
src-$(MIX_FREERTOS-SRC) += FreeRTOS/

src-$(MIX_SHELL) += os_shell.c

obj-n += 
