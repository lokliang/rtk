#include "components/shell.h"
#include "os/os.h"

SHELL_CMD_FN(_kernel_info);

SH_DEF_SUB_CMD(
    _cmd_kernel_sublist,
    SH_SETUP_CMD("info", "Show all the kernel information", _kernel_info, NULL), //
);

SHELL_REGISTER_CMD(
    register_kernel_command,
    SH_SETUP_CMD("kernel", "The real-time kernel function", NULL, _cmd_kernel_sublist), //
);

SHELL_CMD_FN(_kernel_info)
{
    os_sys_print_info();
    return 0;
}

SHELL_CMD_CP_FN(_kernel_fifo_cp)
{
}
