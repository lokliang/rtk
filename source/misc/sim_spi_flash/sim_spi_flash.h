/**
 * @file sim_spi_flash.h
 * @author LokLiang (lokliang@163.com)
 * @brief 模拟一个 SPI 和 FLASH 的硬件的部分功能，用于在纯软件环境中满足对 SPI FLASH 的需求
 * @version 0.1
 * @date 2023-04-12
 *
 * @copyright Copyright (c) 2023
 *
 */

#ifndef __SIM_SPI_FLASH_H__
#define __SIM_SPI_FLASH_H__

void sim_sf_set_cs(void);
void sim_sf_reset_cs(void);
void sim_sf_spi_write(const void *src, int len_bytes);
void sim_sf_spi_read(void *dst, int len_bytes);

#endif
