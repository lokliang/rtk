include $(OUT_DIR)/autoconf.mk
include $(MK_FILE_CC)
include $(MKFILE)

.PHONY: cc ar cc_clean ar_clean

MKFILE_DIR := $(dir $(MKFILE))
LIB_OBJ_DIRS := $(MKFILE_DIR:$(COMMON_PATH)%=$(OUT_DIR)%)
CUR_LIB_OBJS := $(filter $(LIB_OBJ_DIRS)%.o,$(LIB_OBJS))
CUR_LIB_INSTALL_FILE := $(PRJ_PATH)/$(lib-y)

INCLUDE_PATHS := $(filter -I$(MKFILE_DIR)%,$(INCLUDE_PATHS))

cc: $(CUR_LIB_OBJS)

cc_clean:
	$(Q)-$(RM) -r $(dir $(CUR_LIB_OBJS))

ifneq ($(lib-y),)

ar: $(CUR_LIB_INSTALL_FILE)

ar_clean:
	$(Q)-$(RM) $(CUR_LIB_INSTALL_FILE)

$(CUR_LIB_INSTALL_FILE): $(CUR_LIB_OBJS)
	@$(ECHO) "  AR      \033[35m$(@:$(PRJ_PATH)/%=%)\033[0m"
	$(Q)$(MD) $(dir $@)
	$(Q)$(AR) -crs $@ $^

endif # ifneq ($(lib-y),)
