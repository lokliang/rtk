include $(OUT_DIR)/autoconf.mk
include $(MK_FILE_CC)
include $(MKFILE)

.PHONY: cc cc_clean

MKFILE_DIR := $(dir $(MKFILE))
SUB_OBJ_DIRS := $(MKFILE_DIR:$(COMMON_PATH)%=$(OUT_DIR)%)
CUR_SUB_OBJS := $(filter $(SUB_OBJ_DIRS)%.o,$(SUB_OBJS))

INCLUDE_PATHS := $(filter -I$(MKFILE_DIR)%,$(INCLUDE_PATHS))

cc: $(CUR_SUB_OBJS)

cc_clean:
	$(Q)-$(RM) -r $(dir $(CUR_SUB_OBJS))
