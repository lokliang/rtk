#!/bin/bash

function --jlink_config() {
	# 安装并配置 JLink
	# 其中将作以下更改：
	# * 安装 JLink 驱动
	# * 在 JLink 的安装文件夹下添加 FLASH 的烧写算法
	#
	# param: 1      要配置的设备名
	# param: 2      配置参数的文件的路径
	# param: 3      FLASH烧写算法的保存位置
	# param: 4      来源烧写算法的路径
	#
	# https://www.segger.com/downloads/jlink/#J-LinkSoftwareAndDocumentationPack

	local TarFile
	local tmp

	# 检查和安装 JLink 驱动
	if [ ! -f /usr/bin/JLinkExe ]; then
		TarFile=($(find . -name "JLink*$(uname -s)*$(uname -m)*.deb" -type f))
		if ((${#TarFile[@]} > 1)); then
			echo -e "\033[31m[ERROR]\033[0m ${BASH_SOURCE}:${LINENO} 检测到多于一个 JLink 的安装包，请自行选择安装\n"
			return 1
		elif ((${#TarFile[@]} == 1)); then
			echo -e "\033[33m是否安装 '$(basename ${TarFile[@]})'? [N/y] \033[0m\c"
			if [ "$(read tmp && echo $tmp | tr '[A-Z]' '[a-z]')" == "y" ]; then
				echo -e "\033[33m正在安装 '$(basename ${TarFile[0]})'\033[0m"
				sudo dpkg -i ${TarFile[0]} || { echo -e "\033[33m[FAIL]\033[0m ${BASH_SOURCE}:${LINENO} 操作失败\n"; return 1; }

				if [ ! -f /usr/bin/JLinkExe ]; then
					echo -e "\r\033[31m安装失败\033[0m"
					return 1
				fi
			else
				return 0
			fi
		else
			echo -e "\r\033[31m没有本地安装包。\n请从官网下载安装包并重试\033[33m"
			echo "https://www.segger.com/downloads/jlink/#J-LinkSoftwareAndDocumentationPack"
			echo -e "\r\033[0m"
			return 1
		fi

		echo -e "\r\033[33m安装成功\033[0m"
	fi

	echo -e "\033[33m是否配置烧录算法? [N/y] \033[0m\c"
	if [ "$(read tmp && echo $tmp | tr '[A-Z]' '[a-z]')" != "y" ]; then
		return 0
	fi
	TarFile='/opt/SEGGER/JLink/JLinkDevices.xml'
	tmp='.tmp.xml'
	if [ ! -f ${TarFile} ]; then
		echo -e "\033[31m[ERROR]\033[0m ${BASH_SOURCE}:${LINENO} 未发现文件 '${TarFile}'\n请尝试安装正确的版"
		return 1
	fi
	if [ $# -lt 4 ]; then
		${BASH_SOURCE} -l ${FUNCNAME} -a && echo -e "\033[31m[ERROR]\033[0m ${BASH_SOURCE}:${LINENO} 少于 4 个参数\n"
		return 1
	fi

	local DeviceName=$1
	local ConfigFile=$2
	local Vendor=$3
	local FlashScript=$4

	if [ ! -f ${ConfigFile} ]; then
		echo -e "\033[31m[ERROR]\033[0m ${BASH_SOURCE}:${LINENO} 参数2: 不存在的配置文件 '${ConfigFile}'"
		return 1
	fi
	if !(cat ${ConfigFile} | grep -q -e "Name\=\"${DeviceName}\""); then
		echo -e "\033[31m[ERROR]\033[0m ${BASH_SOURCE}:${LINENO} 参数2: 配置文件错误 '${ConfigFile}'"
		return 1
	fi
	if [ ! -f ${FlashScript} ]; then
		echo -e "\033[31m[ERROR]\033[0m ${BASH_SOURCE}:${LINENO} 参数4: 不存在的算法文件 '${FlashScript}'"
		return 1
	fi
	
	if !(cat $TarFile | grep -q -e "Name\=\"${DeviceName}\""); then
		cat ${TarFile} > ${tmp}
		sed -e '/<\/DataBase>/d' -i ${tmp}
		cat ${ConfigFile} >> ${tmp}
		echo '</DataBase>' >> ${tmp}
		sudo chmod 666 ${TarFile}
		sudo cat ${tmp} > ${TarFile}
		sudo chmod 664 ${TarFile}
		if !(cat $TarFile | grep -q -e "Name\=\"${DeviceName}\""); then
			echo -e "\033[31m[ERROR]\033[0m ${BASH_SOURCE}:${LINENO} 配置文件更新失败"
			return 1
		fi
		rm -rf ${tmp}
	fi

	if [ ! -d /opt/SEGGER/JLink/Devices/${Vendor} ]; then
		sudo mkdir -p /opt/SEGGER/JLink/Devices/${Vendor} || { echo -e "\033[31m[ERROR]\033[0m ${BASH_SOURCE}:${LINENO} 操作失败"; return 1; }
	fi
	sudo cp ${FlashScript} /opt/SEGGER/JLink/Devices/${Vendor}/$(basename ${FlashScript})
	if [ ! -f /opt/SEGGER/JLink/Devices/${Vendor}/$(basename ${FlashScript}) ]; then
		echo -e "\033[31m[ERROR]\033[0m ${BASH_SOURCE}:${LINENO} 操作失败"
		return 1
	fi

	echo -e "[SUCCESS]"
}

--jlink_config ${EXEC_DEVICE} ${JLINK_CONFIG_XML} ${VENDOR} ${LOADER}
