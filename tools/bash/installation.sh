#!/bin/bash

sudo apt-get update
sudo apt-get upgrade
sudo apt-get install -y \
make \
gcc \
g++ \
gdb \
gdbserver \
gcc-multilib \
g++-multilib \
\
gcc-arm-none-eabi \
gdb-multiarch \
libusb-0* \
libusb-1* \
openocd \
libreadline5 \
\
