#!/bin/bash

ValArray=(${@:4})
if [ -n "$3" ] && [ -n "${ValArray}" ]; then
    echo -e "\033[35m\$\033[33m(\033[36m$3\033[33m)\033[0m"
    echo ${ValArray[@]} | xargs -n 1

    if [ ! -f $2/mset_log.mk ] || [ -z "$(sed -e 's/\r//g' -n -e "/^mset-$3:$/p" $2/mset_log.mk)" ]; then
        echo -e "# \$($3):" >> $2/mset_log.mk
        for tmp in ${ValArray[@]}; do
            echo -e "# $tmp" >> $2/mset_log.mk
        done
        echo -e "mset-$3:\n\t@\$(ROOT_PATH)/tools/bash/mset_log.sh \$(ROOT_PATH) \$(OUT_DIR) $3 \$($3)\r\n" >> $2/mset_log.mk
    fi
else
    echo -e "未定义变量 \$($3) 或该值为空"
fi
