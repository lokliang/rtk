[简要](../README.md)

# 工程构建

## 目录结构
```bash
$(ROOT_PATH)
└── tools                       # 本目录
    ├── bash                    # shell 脚本目录
    │   ├── build.sh            # 执行编译
    │   ├── gen_config_mk.sh    # 解析 .conf 文件
    │   ├── installation.sh     # 安装基本开发环境
    │   ├── jlink_config.sh     # 配置 JLink 脚本
    │   ├── jlink.sh            # 操作 JLink
    │   └── mset_log.sh         # 查看 make 脚本变量
    └── make                    # make 脚本目录
        ├── config.mk           # make 顶层脚本
        ├── lib.mk              # make 编译子脚本
        └── sub.mk              # make 编译子脚本
```

## 工程构建的主要文件

  - > 本目录为工程构建的核心部分。主体以 `make` 脚本和 linux `shell` 脚本配合构成。如果在 Windows 下编译，还需要安装 `mingw` 等虚拟环境。

  - `./bash/build.sh`
    > 编译选项交互脚本，实现了：
      1. 简单的配置交互脚本
      2. 读、写配置日志文件
      3. 输出调试文件到目录 debug
      4. 执行 make

  - `./bash/gen_config_mk.sh`
    > 合并所有的 .conf 文件，并以如下方法解析文件内容：
      1. 以 '#' 开关的表示为备注，展开时被忽略
      2. 可识别格式为 变量名 = 值
          - 2.1. 值为 'y' 或 'Y' 展开到 autoconf.mk 时原值不变；
          - 2.2. 值为 'y' 或 'Y' 展开到 autoconf.h 时变更为 1；值为 'n' 或 'N' 不在 autoconf.h 中展开；
      3. 所有以 "MIX_" 开头的关键字作为被查找的文件名，将在工程根目录下搜索对应的模块文件（如 MIX_COMMON.prj.mk），并自动把模块文件包含到 make 子脚本中；
      4. 所有符合头文件的宏的格式的变量名展开到 autoconf.h 中；
      5. 所有变量名展开到 autoconf.mk 中；

  - `./make/config.mk`
    > makefile 顶层脚本。
    1. 包含了所有子脚本
    2. 执行 "gen_config_mk.sh" 生成需要的 autoconf.mk 文件
    3. 确定所有变量的值

  - `./make/lib.mk` 和 `./make/sub.mk`
    > sub 类型 和 lib 类型的子脚本

  - 整体编译控制流程：
    1. 由 `./bash/build.sh` 根据用户配置，确定几个固定变量的值，并通过参数传递到顶层的 make 脚本 `./make/config.mk`;
        - 固定设置的变量有: $(ROOT_PATH), $(BOARD_PATH), $(CHIP_PATH), $(ARCH_PATH), $(CC_PREFIX), $(CONF_FILE)
        - 不能被 build.sh 识别的所有参数都将传递到顶层 makefile，如 ./build.sh help
        - 使用 '$ ./build.sh -h' 打印可识别的参数
        - 编译成功后，将自动完成以下动作：
            - 更新配置到日志文件 $(PRJ_PATH)/.build_log
            - 添加命令行到文件 $(PRJ_PATH)/.rebuild.sh
            - 复制部分目标文件到 $(PRJ_PATH)/debug/
    2. 顶层的 make 脚本 `./make/config.mk` 确定所有需要的变量，然后调用子脚本，执行最终编译规则;
        - 大部分目标都依赖文件 $(OUT_DIR)/autoconf.mk，该文件通过目标 $(OUT_DIR)/autoconf.mk 执行 `./bash/gen_config_mk.sh` 生成;
        - 所有源文件默认包含 autoconf.h，该文件由 `./bash/gen_config_mk.sh` 根据 .conf 文件生成;
    3. `./make/config.mk` 中包含了所有主要的：
        - make 子脚本
        - 整个工程的大部源文件

## 特殊目录
  > 受工程构建脚本的控制，有些特定的目录被赋予了特定的功能和结构。

### *$(ROOT_PATH)/architecture/arch*
  - > 用于定义不同平台下的编译规则，需包含以下文件：
    - cc.mk: 目标为中间文件的生成规则（必须）
    - prj.mk: 目标为工程输出文件的生成规则（必须）
    - img.mk: 目标为版本释放文件的生成规则（可选）
  - > 可被 build.sh 识别的特征：目录下包含文件 `cc.mk`

### *$(ROOT_PATH)/architecture/board*
  - > 用于具体的开发板的板级配置
  - > 目录下所有源文件被自动添加到工程
  - > 目录下所有头文件所在的目录被自动添加到工程
  - > 目录下所有 .conf 文件被自动添加到工程
  - > 项目输出目录以该目录名区分
  - > 用户配置日志以该目录名分组记录
  - > 可被 build.sh 识别的特征：目录下包含以 `.mk` 为后缀的文件名

### *$(ROOT_PATH)/architecture/chip*
  - > 用于具体同平台的特有配置，典型的配置包括：
      - 链接脚本
      - 启动代码
      - JLink 烧录程序
      - 模块配置
      - main.c
  - > 目录下所有头文件所在的目录被自动添加到工程
  - > 目录下所有 .conf 文件被自动添加到工程
  - > 需要通过变量 ext-objs-y 包含指定源文件 [参考文件](../architecture/chip/arm/stm32f1xx/chip.mk)
  - > 可通过变量 LINKER_SCRIPT 指定链接脚本，或在 prj.mk 中设置 [参考文件](../architecture/arch/arm/cm3/prj.mk)
  - > 可被 build.sh 识别的特征：目录下包含以 `.mk` 为后缀的文件名

### *$(ROOT_PATH)/architecture/common*
  - > 另外实现的通用底层代码并重定向，包含 malloc 和 printf 等
  - > 在任意 .conf 文件中设置 **MIX_COMMON=y** 可将其添加到工程中

### *$(ROOT_PATH)/include*
  - > 总是被包含的目录，对所有模块可见

### *$(ROOT_PATH)/library*
  - > 可用的静态库的默认目录
  - > 设置变量 EXT_LIBS_DIR 可另外指定其他的静态库文件路径
  - > 在此目录下（子目录除外）的静态库文件始终被链接

### *$(PRJ_PATH)/bin*
  - > 二进制文件输出目录
  - > 输出的默认文件名规则: `项目名-开发板名.bin`
  - > 如果使用 **$ ./build.sh release** 还将输出 `项目名-开发板名-symbols.ld`

### *$(PRJ_PATH)/debug*
  - > 以编辑器 VSCode 为例，在开发时有关的辅助文件的输出目录，包含：
    1. `autoconf.h`: 来自当前 .conf 文件的有效配置，可设置编辑器根据这个文件正确显示活动的代码；
    2. `project.bin`, `project.map` 和 `project.elf`: 通过统一的文件名，方便下载和调试不同的项目时，不必频繁更改设置；
    3. `symbols.txt`: 是输出文件 `项目名-开发板名-symbols.ld` 的解析来源文件；
    4. `project.objdump`: 可使用 **$ ./build.sh objdump** 生成的反汇编文件；

### *$(PRJ_PATH)/release*
  - > 释放版本输出目录
  - > 输出的默认文件名规则: `Release-项目名-开发板名-提交日期-哈希值.bin`

### 可自由定义的目录
  - > 除上述的特殊目录外，可在 $(ROOT_PATH) 和 $(PRJ_PATH) 下创建任意目录，存放任意模块。
  - > 每个可用模块以最少包含一个后缀为 '.mk' 的文件表示，使该模块可被搜索到，其名称格式为：`模块名.模块类型.mk`
    - 模块名: 与 .conf 文件中 'MIX_' 开头的关键字完全一致，当其关键字的值为 'y' 或 'Y' 时，模块将被搜索到并添加到工程中
    - 模块类型: 可分为 prj, sub, lib 三类：
      1. prj: 通用模块，在编译过程中，所有的头文件的搜索路径都对其都可见，固定标识宏 'BUILD_TYPE_PRJ=1'；
      2. sub: 作为子模块，在编译过程中，仅在该路径下和共享路径对其都可见，固定标识宏 'BUILD_TYPE_SUB=1；
      3. lib: 作为静态库，在编译过程中，仅在该路径下和共享路径对其都可见，固定标识宏 'BUILD_TYPE_LIB=1，使用 **$ ./build.sh lib_install** 可输出静态库文件；
    - .mk: 必须以此作为后缀；
    - [参考文件](../source/components/shell/MIX_SHELL.lib.mk)


## 新建项目

  - 在项目目录下的所有源文件和头文件所在的目录被自动添加到编译参数中

  - 在项目目录新建文件 `Makefile`, 并且包含键字段 'include .*/config.mk' 例如：
```make
# toolchain make rules
include $(ROOT_PATH)/tools/make/config.mk
```

  - 在项目目录新建后缀为 `.conf` 的配置文件，默认名为 prj.conf，例如：
```
## debug
CONFIG_SYS_LOG_LEVEL = (DEBUG_BUILD ? SYS_LOG_LEVEL_DBG : SYS_LOG_LEVEL_OFF)

## components
# MIX_SHELL, MIX_FATFS, MIX_XZ 所在的目录将被搜索到，同时其内容被展开到 outdir/board/autoconf.mk 和 outdir/board/autoconf.h 中
MIX_SHELL = y
MIX_FATFS = y
MIX_XZ = y
```

  - 如果需要指定多个配置文件，命令行中可用参数 -p 指定筛选名，例如：
```bash
$ ./build.sh -p prj -p os # 表示有两个配置文件，分别为 prj*.conf 和 os*.conf
```

  - make 脚本常用的变量，结尾不带斜杠，所有 make 子脚本可见，可通过命令行查看，例如 '$ ./build.sh mset=ROOT_PATH'
      - **$(ROOT_PATH)** RTK 根目录的绝对路径
      - **$(PRJ_PATH)** 当前项目的绝对路径
      - **$(ARCH_PATH)** 当前选择的编译规则的目录的绝对路径
      - **$(BOARD_PATH)** 当前选择的开发板的目录的绝对路径
      - **$(CHIP_PATH)** 当前选择的平台的目录的绝对路径

  - 可在任意 make 子脚本额外添加的变量。这些定义将在所有 make 脚本可见（包括所有类型的模块 .prj.mk, .sub.mk, .lib.mk ），例如：
```make
# EXT_LIBS_DIR: 用于添加静态库目录，必须为绝对路径，目录深度为1，一般在 'chip.mk' 中设置，在链接阶段会把所有指定的目录下的静态库链接到最终工程中
EXT_LIBS_DIR += $(ROOT_PATH)/lib/ext

# EXT_INC_DIR: 用于添加头文件可搜索的共享路径，必须为绝对路径
EXT_INC_DIR += $(PRJ_PATH)/inc

# EXT_INC_FILE: 在编译时所有 .c, .cpp 的源文件都强制包含的头文件，必须为绝对路径
EXT_INC_FILE += $(BOARD_PATH)/board_config.h

# EXT_SYMBOLS: 在编译时，定义到所有源文件的宏
EXT_SYMBOLS += -DVERSION='"$(VER)"'

# ext-objs-y: 添加编译的源文件，必须为绝对路径，一般在 'chip.mk' 中设置
ext-objs-y += $(CHIP_PATH)/main.c

# LINKER_SCRIPT: 指定链接脚本文件，必须为绝对路径，一般在 'chip.mk' 中设置，注意只能指定一个文件
LINKER_SCRIPT = $(CHIP_PATH)/linker_script/STM32F103XB_FLASH.ld
```

## 添加模块
  - 在任意的非特殊目录下，出现文件 MIX_*.prj.mk, MIX_*.sub.mk 或 MIX_*.lib.mk 时，表示该目录为子模块的位置（参考 可自由定义的目录）；
  - 如果一个空白的子脚本被选中，将被自动添加一些基本的配置；
  - 在子脚本中，有效的配置值为: inc-y, src-y, obj-n。如果类型为 .lib.mk 则必须配置 lib-y，它们的取值规则为：
    1. inc-y += 编译时的头文件搜索目录，如果以 '/*' 结尾则表示递归目录下所有包含头文件的子目录，前缀路径为文件所在目录；
    2. src-y += 添加单个源文件或目录；如果以 '/*' 结尾则表示递归目录下所有的源文件，前缀路径为文件所在目录；
    3. obj-n += 强制排除的单个源文件，前缀路径为文件所在目录；
    4. lib-y := 静态库的输出路径，前缀路径为 $(ROOT_PATH)；
  - 子脚本支持 if 关键字，特殊逻辑只能由变量的值控制；
  - [例1](../source/components/shell/MIX_SHELL.lib.mk)
  - [例2](../kernel/FreeRTOS/FreeRTOSv10.4.4/MIX_FREERTOS_V1044.prj.mk)

## 使用 JLink 调试
  - 以 VSCode 远程登录虚拟机 Ubuntu 为例
    1. 登录虚拟机
    2. 进入工程要目录
    3. 执行命令 `$ ./build.sh gdb_server`
    4. 启动调试

## VSCode 参考设置

  - launch.json
```json
{
	// 使用 IntelliSense 了解相关属性。
	// 悬停以查看现有属性的描述。
	// 欲了解更多信息，请访问: https://go.microsoft.com/fwlink/?linkid=830387
	"version": "0.2.0",
	"configurations": [

		{
			"name": "gdb",
			"type": "cppdbg",
			"request": "launch",
			"program": "${workspaceFolder}/debug/project.elf",
			"args": [],
			"stopAtEntry": false,
			"cwd": "${workspaceFolder}",
			"environment": [],
			"externalConsole": false,
			"setupCommands": [
				{
					"description": "Enable pretty-printing for gdb",
					"text": "-enable-pretty-printing",
					"ignoreFailures": true
				}
			],
		},

		{
			"name": "(gdb) qemu-cm3",
			"type": "cppdbg",
			"request": "launch",
			"program": "${workspaceFolder}/debug/project.elf",
			"args": [],
			"stopAtEntry": true,
			"cwd": "${workspaceFolder}",
			"environment": [],
			"externalConsole": true,
			"miDebuggerPath": "gdb-multiarch",
			"miDebuggerServerAddress": "localhost:1234",
			"setupCommands": [
				{
					"description": "Enable pretty-printing for gdb",
					"text": "-enable-pretty-printing",
					"ignoreFailures": true
				}
			],
			"preLaunchTask": "qemu",
		},

		{
			"name": "(gdb) JLinkGDBServer",
			"type": "cppdbg",
			"request": "launch",
			"program": "${workspaceFolder}/debug/project.elf",
			"args": [],
			"stopAtEntry": true,
			"cwd": "${workspaceFolder}",
			"environment": [],
			"externalConsole": true,
			"MIMode": "gdb",
			// "miDebuggerPath": "gdb-multiarch",
			"miDebuggerPath": "arm-none-eabi-gdb",
			"miDebuggerServerAddress": "localhost:2331",
			"setupCommands": [
				{
					"description": "Enable pretty-printing for gdb",
					"text": "-enable-pretty-printing",
					"ignoreFailures": true
				}
			],
			"preLaunchTask": "JLinkGDBServer",
			"postDebugTask": "killallJLinkGDBServer",
		},

	]
}
```

  - tasks.json
```json
{
	// See https://go.microsoft.com/fwlink/?LinkId=733558
	// for the documentation about the tasks.json format
	"version": "2.0.0",
	"tasks": [

		{
			"label": "build-extras",
			"type": "shell",
			"command": "./build.sh",
			"args": ["debug_build"],
			"problemMatcher": {
				"owner": "cpp",
				"fileLocation": ["relative", "/"],
				"pattern": [
					{
					  "regexp": ".",
					  "file": 1,
					  "location": 2,
					  "message": 3
					}
				]
			},
			"group": {
				"kind": "build",
				"isDefault": true
			},
			"presentation": {
				"echo": true,
				"reveal": "always",
				"focus": true,
				"panel": "dedicated",
				"showReuseMessage": true,
				"clear": true,
			}
		},

		{
			"label": "qemu",
			// "dependsOn": "build-extras",
			"type":"shell",
			"isBackground":true,
			"runOptions": {
				"instanceLimit": 1
			},
			"command": [
				"qemu-system-arm",
				"-kernel ./debug/project.elf",
				"-machine mps2-an385",
				"-d cpu_reset",
				"-nographic",
				"-monitor null",
				"-semihosting",
				"--semihosting-config enable=on,target=native",
				"-serial stdio",
				"-s",
				"-S",
			],
			"presentation": {
				"echo": true,
				"reveal": "always",
				"focus": true,
				"panel": "dedicated",
				"showReuseMessage": true,
				"clear": true,
			},
			"problemMatcher":
			{
				"owner": "external",
				"pattern": [
					{
						"regexp": ".",
						"file": 1,
						"location": 2,
						"message": 3
					}
				],
				"background": {
					"activeOnStart": true,
					"beginsPattern": ".",
					"endsPattern": "."
				}
			}
		},

		{
			"label": "killallqemu",
			"type":"shell",
			"isBackground": true,
			"command": "killall",
			"args": ["qemu-system-arm"],
			"presentation": {
				"echo": false,
				"reveal": "never",
				"focus": false,
				"showReuseMessage": false,
				"clear": true,
			},
		},

		{
			"label": "JLinkGDBServer",
			"type": "shell",
			"isBackground": true,
			"command": "./build.sh",
			"args": [
				// "flash",
				"debug_cmd",
			],

			// This task is run before some debug tasks.
			// Problem is, it's a watch script, and since it never exits, VSCode
			// complains. All this is needed so VSCode just lets it run.
			"problemMatcher": {
				"owner": "external",
				"pattern": [
					{
						"regexp": ".",
						"file": 1,
						"location": 2,
						"message": 3
					}
				],
				"background": {
					"activeOnStart": true,
					"beginsPattern": ".",
					"endsPattern": "."
				}
			},
			"presentation": {
				"echo": true,
				"reveal": "always",
				"focus": true,
				"panel": "dedicated",
				"showReuseMessage": true,
				"clear": true,
			},
		},

		{
			"label": "killallJLinkGDBServer",
			"type":"shell",
			"isBackground": true,
			"command": "killall",
			"args": ["JLinkGDBServer"],
			"presentation": {
				"echo": false,
				"reveal": "never",
				"focus": false,
				"showReuseMessage": false,
				"clear": true,
			},
		},

	]
}

```

