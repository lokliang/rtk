
## for config.mk
FLASH_SCRIPT = jlink.sh
FLASH_CONFIG = jlink_config.sh
DEBUG_SCRIPT = jlink.sh

## for jlink.sh and jlink_config.sh
EXEC_DEVICE ?= STM32F103RB
JLINK_LOAD_ADDR ?= 0x08000000
export EXEC_DEVICE JLINK_LOAD_ADDR

ext-objs-y += $(CHIP_PATH)/main.c

## for linkerscript and startup
ifdef STM32F100xB
LINKER_SCRIPT = $(CHIP_PATH)/linker_script/STM32F100XB_FLASH.ld
ext-objs-y += $(CHIP_PATH)/startup/startup_stm32f100xb.s
endif

ifdef STM32F100xE
LINKER_SCRIPT = $(CHIP_PATH)/linker_script/STM32F100XE_FLASH.ld
ext-objs-y += $(CHIP_PATH)/startup/startup_stm32f100xe.s
endif

ifdef STM32F101x6
LINKER_SCRIPT = $(CHIP_PATH)/linker_script/STM32F101X6_FLASH.ld
ext-objs-y += $(CHIP_PATH)/startup/startup_stm32f101x6.s
endif

ifdef STM32F101xB
LINKER_SCRIPT = $(CHIP_PATH)/linker_script/STM32F101XB_FLASH.ld
ext-objs-y += $(CHIP_PATH)/startup/startup_stm32f101xb.s
endif

ifdef STM32F101xE
LINKER_SCRIPT = $(CHIP_PATH)/linker_script/STM32F101XE_FLASH.ld
ext-objs-y += $(CHIP_PATH)/startup/startup_stm32f101xe.s
endif

ifdef STM32F101xG
LINKER_SCRIPT = $(CHIP_PATH)/linker_script/STM32F101XG_FLASH.ld
ext-objs-y += $(CHIP_PATH)/startup/startup_stm32f101xg.s
endif

ifdef STM32F102x6
LINKER_SCRIPT = $(CHIP_PATH)/linker_script/STM32F102X6_FLASH.ld
ext-objs-y += $(CHIP_PATH)/startup/startup_stm32f102x6.s
endif

ifdef STM32F102xB
LINKER_SCRIPT = $(CHIP_PATH)/linker_script/STM32F102XB_FLASH.ld
ext-objs-y += $(CHIP_PATH)/startup/startup_stm32f102xb.s
endif

ifdef STM32F103x6
LINKER_SCRIPT = $(CHIP_PATH)/linker_script/STM32F103X6_FLASH.ld
ext-objs-y += $(CHIP_PATH)/startup/startup_stm32f103x6.s
endif

ifdef STM32F103xB
LINKER_SCRIPT = $(CHIP_PATH)/linker_script/STM32F103XB_FLASH.ld
ext-objs-y += $(CHIP_PATH)/startup/startup_stm32f103xb.s
endif

ifdef STM32F103xE
LINKER_SCRIPT = $(CHIP_PATH)/linker_script/STM32F103XE_FLASH.ld
ext-objs-y += $(CHIP_PATH)/startup/startup_stm32f103xe.s
endif

ifdef STM32F103xG
LINKER_SCRIPT = $(CHIP_PATH)/linker_script/STM32F103XG_FLASH.ld
ext-objs-y += $(CHIP_PATH)/startup/startup_stm32f103xg.s
endif

ifdef STM32F105xC
LINKER_SCRIPT = $(CHIP_PATH)/linker_script/STM32F105XC_FLASH.ld
ext-objs-y += $(CHIP_PATH)/startup/startup_stm32f105xc.s
endif

ifdef STM32F107xC
LINKER_SCRIPT = $(CHIP_PATH)/linker_script/STM32F107XC_FLASH.ld
ext-objs-y += $(CHIP_PATH)/startup/startup_stm32f107xc.s
endif
