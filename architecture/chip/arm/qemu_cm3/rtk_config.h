#ifndef __RTK_CONFIG_H__
#define __RTK_CONFIG_H__

#define CONFIG_RTK_ISR_SUPPORT      0                     // 是否使所有接口允许中断使用
#define CONFIG_RTK_LOG_LEVEL        RTK_LOG_LEVEL_INF     // 日志打印等级
#define CONFIG_RTK_MAX_PRIORITY     31                    // 最大线程优先级（值 31 为最优）
#define CONFIG_RTK_TICKS_HZ         1000                  // 默认时钟节拍频率
#define CONFIG_RTK_STACK_CHECK      1                     // 记录线程栈用量
#define CONFIG_RTK_POSIX_ERRNO      0                     // 使用线程错误码

#define CONFIG_RTK_CPU_CLOCK_HZ_FN  cpu_core_clock
#define CONFIG_RTK_SWAP_HOOK_FN     swap_hook

static inline unsigned cpu_core_clock(void)
{
    return 25000000;
}

// static inline void swap_hook(const char *name) { ((void)name); }

#endif
