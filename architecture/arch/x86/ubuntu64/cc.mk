#
# Common rules for GCC Makefile
#

# ----------------------------------------------------------------------------
# options
# ----------------------------------------------------------------------------
ifeq ($(OPTIMIZE), y)
  OPTIMIZE_FLAG := -Os -DDEBUG_BUILD=0 -DNDEBUG
else
  OPTIMIZE_FLAG := -O0 -DDEBUG_BUILD=1 -fno-toplevel-reorder
endif

DBG_FLAG := -g

# ----------------------------------------------------------------------------
# flags for compiler
# ----------------------------------------------------------------------------
# CPU/FPU options
CPU := -m32

CC_FLAGS = $(CPU) -c $(DBG_FLAG) -fno-common -fmessage-length=0 -fshort-enums \
	-enable-threads \
	-fno-exceptions -ffunction-sections -fdata-sections -fomit-frame-pointer \
	-MMD -MP $(OPTIMIZE_FLAG) \
	-Wall -Wextra \
	-Wno-unused-parameter -Wno-sign-compare


# ----------------------------------------------------------------------------
# cross compiler
# ----------------------------------------------------------------------------
AS      := $(CC_PREFIX)as
CC      := $(CC_PREFIX)gcc
CPP     := $(CC_PREFIX)g++
LD      := $(CC_PREFIX)ld
NM      := $(CC_PREFIX)nm
AR      := $(CC_PREFIX)ar
OBJCOPY := $(CC_PREFIX)objcopy
OBJDUMP := $(CC_PREFIX)objdump
SIZE    := $(CC_PREFIX)size
STRIP   := $(CC_PREFIX)strip

# ----------------------------------------------------------------------------
# common rules of compiling objects
# ----------------------------------------------------------------------------
$(OUT_DIR)%.o: $(COMMON_PATH)%.asm
	@$(ECHO) "  CC      \033[32m$(<:$(COMMON_PATH)/%=%)\033[0m"
	$(Q)$(MD) $(dir $@)
	$(Q)$(CC) $(CPU) $(AS_SYMBOLS) $(INCLUDE_PATHS) -c -x assembler-with-cpp -o $@ $<

$(OUT_DIR)%.o: $(COMMON_PATH)%.s
	@$(ECHO) "  CC      \033[32m$(<:$(COMMON_PATH)/%=%)\033[0m"
	$(Q)$(MD) $(dir $@)
	$(Q)$(CC) $(CPU) $(AS_SYMBOLS) $(INCLUDE_PATHS) -c -x assembler-with-cpp -o $@ $<

$(OUT_DIR)%.o: $(COMMON_PATH)%.S
	@$(ECHO) "  CC      \033[32m$(<:$(COMMON_PATH)/%=%)\033[0m"
	$(Q)$(MD) $(dir $@)
	$(Q)$(CC) $(CPU) $(AS_SYMBOLS) $(INCLUDE_PATHS) -c -x assembler-with-cpp -o $@ $<

$(OUT_DIR)%.o: $(COMMON_PATH)%.c
	@$(ECHO) "  CC      \033[32m$(<:$(COMMON_PATH)/%=%)\033[0m"
	$(Q)$(MD) $(dir $@)
	$(Q)$(CC) $(CC_FLAGS) $(CC_SYMBOLS) $(INCLUDE_PATHS) -std=gnu99 -o $@ $<

$(OUT_DIR)%.o: $(COMMON_PATH)%.cpp
	@$(ECHO) "  CC      \033[32m$(<:$(COMMON_PATH)/%=%)\033[0m"
	$(Q)$(MD) $(dir $@)
	$(Q)$(CPP) $(CC_FLAGS) $(CC_SYMBOLS) $(INCLUDE_PATHS) -std=gnu++11 -fno-rtti -o $@ $<

DEPS := $(OBJS:.o=.d)
-include $(DEPS)
