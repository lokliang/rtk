#
# Rules for building project
#

# ----------------------------------------------------------------------------
# common targets and building rules
# ----------------------------------------------------------------------------
ifeq ($(MDK_DBG_EN), y)
  ELF_EXT = axf
else
  ELF_EXT = elf
endif

# .elf file output path
ELF_FILE_PATH := $(OUT_DIR)/$(PRJ_NAME).$(ELF_EXT)

# bin and hex file output path
BIN_FILE ?= $(PWD)/bin/$(PRJ_NAME)-$(notdir $(BOARD_PATH)).bin
HEX_FILE ?= $(PWD)/bin/$(PRJ_NAME)-$(notdir $(BOARD_PATH)).hex

# ----------------------------------------------------------------------------
# extra include path
# ----------------------------------------------------------------------------
include $(MK_FILE_CC)
include $(MK_FILE_IMG)

# ----------------------------------------------------------------------------
# linker script
# ----------------------------------------------------------------------------
# linker script, maybe override by the specific project
LINKER_SCRIPT ?= $(CHIP_PATH)/linker_script/rom.ld

PROJECT_LD ?= $(OUT_DIR)/.project.ld

# ----------------------------------------------------------------------------
# library
# ----------------------------------------------------------------------------
# standard libraries
LIBRARIES += -lstdc++ -lsupc++ -lm -lc -lgcc -lnosys

# ----------------------------------------------------------------------------
# flags for linker
# ----------------------------------------------------------------------------
LD_FLAGS = $(CPU)
# -Wl,--gc-sections: 不链接未用函数
LD_FLAGS += -Wl,--gc-sections
# --specs=nano.specs: nona.specs 将 -lc 替换成 -lc_nano，使有精简版的C库替代标准C库，可以减少最终程序映像的大小
LD_FLAGS += --specs=nano.specs
# -Wl,-Map=$(basename $@).map,--cref: gcc/g++中生成map文件, --cref: Cross Reference的简写，输出交叉引用表（cross reference table）。
LD_FLAGS += -Wl,-Map=$(basename $@).map,--cref


# ----------------------------------------------------------------------------
# project rules
# ----------------------------------------------------------------------------
prj: $(ELF_FILE_PATH)

install: $(BIN_FILE)

install_clean:

objdump: $(ELF_FILE_PATH)
	@$(ECHO) "  DUMP    \033[34m$(basename $(<:$(PWD)/%=%)).objdump\033[0m"
	$(Q)$(OBJDUMP) -Sdh $< > $(basename $(ELF_FILE_PATH)).objdump

size: $(ELF_FILE_PATH)
	@$(ECHO) "  SIZE      "
	@$(ECHO) "---------------------------------------------------"
	$(Q)$(SIZE) $(OUT_DIR:$(shell pwd)/%=%)/$(PRJ_NAME).$(ELF_EXT)
	@$(ECHO) "---------------------------------------------------"

clean:
	@$(ECHO) "  CLEAN   \033[31m$@\033[0m"
	$(Q)-$(RM) \
	$(DEPS) \
	$(OBJS) \
	$(OUT_DIR)/$(PRJ_NAME).* \
	$(OUT_DIR)/$(PROJECT_LD) \
	$(OUT_DIR)/autoconf.h \
	$(OUT_DIR)/autoconf.mk \
	$(BIN_FILE) \
	$(HEX_FILE) \


$(BIN_FILE): $(ELF_FILE_PATH)
	@$(ECHO) "  BIN     \033[34m$(@:$(PWD)/%=%)\033[0m"
	$(Q)$(MD) $(dir $@)
	$(Q)$(OBJCOPY) -O binary -S $< $@

$(HEX_FILE): $(ELF_FILE_PATH)
	@$(ECHO) "  HEX     \033[34m$(@:$(PWD)/%=%)\033[0m"
	$(Q)$(MD) $(dir $@)
	$(Q)$(OBJCOPY) -O ihex $< $@

$(ELF_FILE_PATH): $(OBJS) $(LINKER_SCRIPT) $(MAKEFILE_LIST)
	@$(ECHO) "  LD      \033[34m$(@:$(PWD)/%=%)\033[0m"
	$(Q)$(MD) $(dir $@)
	$(Q)$(CC) -E -P -CC $(INCLUDE_PATHS) $(CC_SYMBOLS) -o $(PROJECT_LD) - < $(LINKER_SCRIPT) && \
	$(CC) $(LD_FLAGS) -o$@ -T$(PROJECT_LD) -L$(OUT_DIR) $(OBJS) $(LIBRARIES)


