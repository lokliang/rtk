#
# Common rules for GCC Makefile
#

MDK_DBG_EN ?= y
HARDFP ?= n

# ----------------------------------------------------------------------------
# cross compiler
# ----------------------------------------------------------------------------
AS      := $(CC_PREFIX)gcc -x assembler-with-cpp
CC      := $(CC_PREFIX)gcc
CPP     := $(CC_PREFIX)g++
LD      := $(CC_PREFIX)ld
NM      := $(CC_PREFIX)nm
AR      := $(CC_PREFIX)ar
OBJCOPY := $(CC_PREFIX)objcopy
OBJDUMP := $(CC_PREFIX)objdump
SIZE    := $(CC_PREFIX)size
STRIP   := $(CC_PREFIX)strip

# ----------------------------------------------------------------------------
# options
# ----------------------------------------------------------------------------
ifeq ($(OPTIMIZE), y)
  OPTIMIZE_FLAG := -Os -DDEBUG_BUILD=0 -DNDEBUG
else
  OPTIMIZE_FLAG := -O0 -DDEBUG_BUILD=1 -fno-toplevel-reorder
endif

ifeq ($(MDK_DBG_EN), y)
  DBG_FLAG := -g -gdwarf-2
else
  DBG_FLAG := -g
endif

ifeq ($(HARDFP), y)
  FLOAT_ABI := hard
else
  FLOAT_ABI := softfp
endif

# ----------------------------------------------------------------------------
# flags for compiler
# ----------------------------------------------------------------------------
# CPU/FPU options
CPU := -mcpu=cortex-m4 -mthumb

CC_FLAGS = $(CPU) -c $(DBG_FLAG) -fno-common -fmessage-length=0 -fshort-enums \
	-fno-exceptions -ffunction-sections -fdata-sections -fomit-frame-pointer \
	-MMD -MP $(OPTIMIZE_FLAG) \
	-Wall -Wextra \
	-Wno-unused-parameter -Wno-sign-compare


# ----------------------------------------------------------------------------
# include config header for all project
# ----------------------------------------------------------------------------
CC_FLAGS += -I$(ARCH_PATH) -include sys/features.h


# ----------------------------------------------------------------------------
# common rules of compiling objects
# ----------------------------------------------------------------------------
$(OUT_DIR)%.o: $(COMMON_PATH)%.asm
	@$(ECHO) "  CC      \033[32m$(<:$(COMMON_PATH)/%=%)\033[0m"
	$(Q)$(MD) $(dir $@)
	$(Q)$(CC) $(CPU) $(AS_SYMBOLS) $(INCLUDE_PATHS) -c -x assembler-with-cpp -o $@ $<

$(OUT_DIR)%.o: $(COMMON_PATH)%.s
	@$(ECHO) "  CC      \033[32m$(<:$(COMMON_PATH)/%=%)\033[0m"
	$(Q)$(MD) $(dir $@)
	$(Q)$(CC) $(CPU) $(AS_SYMBOLS) $(INCLUDE_PATHS) -c -x assembler-with-cpp -o $@ $<

$(OUT_DIR)%.o: $(COMMON_PATH)%.S
	@$(ECHO) "  CC      \033[32m$(<:$(COMMON_PATH)/%=%)\033[0m"
	$(Q)$(MD) $(dir $@)
	$(Q)$(CC) $(CPU) $(AS_SYMBOLS) $(INCLUDE_PATHS) -c -x assembler-with-cpp -o $@ $<

$(OUT_DIR)%.o: $(COMMON_PATH)%.c
	@$(ECHO) "  CC      \033[32m$(<:$(COMMON_PATH)/%=%)\033[0m"
	$(Q)$(MD) $(dir $@)
	$(Q)$(CC) $(CC_FLAGS) $(CC_SYMBOLS) $(INCLUDE_PATHS) -std=gnu99 -o $@ $<

$(OUT_DIR)%.o: $(COMMON_PATH)%.cpp
	@$(ECHO) "  CC      \033[32m$(<:$(COMMON_PATH)/%=%)\033[0m"
	$(Q)$(MD) $(dir $@)
	$(Q)$(CPP) $(CC_FLAGS) $(CC_SYMBOLS) $(INCLUDE_PATHS) -std=gnu++11 -fno-rtti -o $@ $<

DEPS := $(OBJS:.o=.d)
-include $(DEPS)
