IMAGE_LOAD_ENABLE ?= 0  # 使执行装载到目标（0 或 1）
IMAGE_LOAD_CHECK ?= 0   # 仅执行一次装载（0 或 1）
IMAGE_ENTRY_ENABLE ?= 0 # 使执行跳转到目标地址（0 或 1）
IMAGE_ENTRY_CHECK ?= 0  # 使跳转前执行目标数据校验（0 或 1）

IMAGE-PATH ?= $(dir $(BIN_FILE))
IMAGE-NAME ?= $(basename $(notdir $(BIN_FILE)))-image-xz
IMAGE-FILE := $(IMAGE-PATH)$(IMAGE-NAME)

# ----------------------------------------------------------------------------
# image
# ----------------------------------------------------------------------------
image: $(BIN_FILE) size
ifneq ($(IMAGE_VER),)
ifneq ($(IMAGE_PLATFORM_NAME),)
ifneq ($(IMAGE_FIRMWARE_NAME),)
ifneq ($(IMAGE_LOAD_TYPE),)
ifneq ($(IMAGE_LOAD_ADDR),)
ifneq ($(IMAGE_ENTRY_ADDR),)
	@$(ECHO) "  MKIMG   \033[34m$(IMAGE-FILE:$(COMMON_PATH)/%=%).c\033[0m"
	$(Q)$(BOARD_PATH)/mkimg \
		$(IMAGE-FILE) \
		$(BIN_FILE) \
		$(IMAGE_PLATFORM_NAME) \
		$(IMAGE_FIRMWARE_NAME) \
		$(IMAGE_VER) \
		$(IMAGE_LOAD_TYPE) \
		$(IMAGE_LOAD_ADDR) \
		$(IMAGE_ENTRY_ADDR) \
		$(IMAGE_LOAD_ENABLE) \
		$(IMAGE_LOAD_CHECK) \
		$(IMAGE_ENTRY_ENABLE) \
		$(IMAGE_ENTRY_CHECK) \
		$(CHIP_ROM_BASE) \
		$(BOARD_PATH)/boot.bin
endif
endif
endif
endif
endif
endif

image_clean:

image_release: $(RELEASE_FILE).bin $(RELEASE_FILE).hex

$(RELEASE_FILE).bin: $(BIN_FILE)
	@$(ECHO) "  REL     \033[34m$(@:$(PWD)/%=%)\033[0m"
	$(Q)$(MD) $(dir $@)
	$(Q)$(CP) $(BIN_FILE) $@

$(RELEASE_FILE).hex: $(HEX_FILE)
	@$(ECHO) "  REL     \033[34m$(@:$(PWD)/%=%)\033[0m"
	$(Q)$(MD) $(dir $@)
	$(Q)$(CP) $(HEX_FILE) $@
