/**
 * @file retarget_heap.c
 * @author LokLiang (lokliang@163.com)
 * @brief
 * @version 0.1
 * @date 2023-05-01
 *
 * @copyright Copyright (c) 2023
 *
 */

#include <stddef.h>
#include <stdlib.h>

struct _reent;

#if defined(MIX_OS)

#include "os/os.h"

void *malloc(size_t size)
{
    return os_malloc(size);
}

void *calloc(size_t nmemb, size_t size)
{
    return os_calloc(size * nmemb);
}

void *realloc(void *ptr, size_t size)
{
    return os_realloc(ptr, size);
}

void free(void *ptr)
{
    os_free(ptr);
}

void *_malloc_r(struct _reent *reent, size_t __size)
{
    return os_malloc(__size);
}

void *_calloc_r(struct _reent *reent, size_t __nmemb, size_t __size)
{
    return os_calloc(__size * __nmemb);
}

void *_realloc_r(struct _reent *reent, void *__ptr, size_t __size)
{
    return os_realloc(__ptr, __size);
}

void _free_r(struct _reent *reent, void *__ptr)
{
    os_free(__ptr);
}

#elif defined(MIX_RTK)

#include "rtk_heap.h"

void *malloc(size_t size)
{
    return rtk_heap_malloc(size, 0);
}

void *calloc(size_t nmemb, size_t size)
{
    return rtk_heap_calloc(size * nmemb, 0);
}

void *realloc(void *ptr, size_t size)
{
    return rtk_heap_realloc(ptr, size, 0);
}

void free(void *ptr)
{
    rtk_heap_free(ptr);
}

void *_malloc_r(struct _reent *reent, size_t __size)
{
    return rtk_heap_malloc(__size, 0);
}

void *_calloc_r(struct _reent *reent, size_t __nmemb, size_t __size)
{
    return rtk_heap_calloc(__size * __nmemb, 0);
}

void *_realloc_r(struct _reent *reent, void *__ptr, size_t __size)
{
    return rtk_heap_realloc(__ptr, __size, 0);
}

void _free_r(struct _reent *reent, void *__ptr)
{
    rtk_heap_free(__ptr);
}

#else

#include "heap/heap.h"

void *malloc(size_t size)
{
    return heap_malloc(NULL, size);
}

void *calloc(size_t nmemb, size_t size)
{
    return heap_calloc(NULL, size * nmemb);
}

void *realloc(void *ptr, size_t size)
{
    return heap_realloc(NULL, ptr, size);
}

void free(void *ptr)
{
    heap_free(NULL, ptr);
}

void *_malloc_r(struct _reent *reent, size_t __size)
{
    return heap_malloc(NULL, __size);
}

void *_calloc_r(struct _reent *reent, size_t __nmemb, size_t __size)
{
    return heap_calloc(NULL, __size * __nmemb);
}

void *_realloc_r(struct _reent *reent, void *__ptr, size_t __size)
{
    return heap_realloc(NULL, __ptr, __size);
}

void _free_r(struct _reent *reent, void *__ptr)
{
    heap_free(NULL, __ptr);
}

#endif
