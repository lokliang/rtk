///////////////////////////////////////////////////////////////////////////////
// \author (c) Marco Paland (info@paland.com)
//             2014-2019, PALANDesign Hannover, Germany
//
// \license The MIT License (MIT)
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// \brief Tiny printk, sprintk and snprintk implementation, optimized for speed on
//        embedded systems with a very limited resources.
//        Use this instead of bloated standard/newlib printk.
//        These routines are thread safe and reentrant.
//
///////////////////////////////////////////////////////////////////////////////

#ifndef _PRINTK_H_
#define _PRINTK_H_

#include <stdarg.h>
#include <stddef.h>

#ifdef __cplusplus
extern "C"
{
#endif

#if defined(__GNUC__)

#ifndef __printf_like
#define __printf_like(f, a) __attribute__((format(printf, f, a)))
#endif

#else

#ifndef __printf_like
#define __printf_like(f, a)
#endif

#endif

#if defined(CONFIG_MICRO_PRINT)
#define PRINTK_DISABLE_SUPPORT_FLOAT
#define PRINTK_DISABLE_SUPPORT_LONG_LONG
#define PRINTK_DISABLE_SUPPORT_PTRDIFF_T
#endif

/**
 * Output a character to a custom device like UART, used by the printk() function
 * This function is declared here only. You have to write your custom implementation somewhere
 * \param character Character to output
 */
void _putchar(char character);

/**
 * Tiny printk implementation
 * You have to implement _putchar if you use printk()
 * To avoid conflicts with the regular printk() API it is overridden by macro defines
 * and internal underscore-appended functions like printk_() are used
 * \param format A string that specifies the format of the output
 * \return The number of characters that are written into the array, not counting the terminating null character
 */
#define printk printk_
__printf_like(1, 2) int printk_(const char *format, ...);

/**
 * Tiny sprintk implementation
 * Due to security reasons (buffer overflow) YOU SHOULD CONSIDER USING (V)SNPRINTK INSTEAD!
 * \param buffer A pointer to the buffer where to store the formatted string. MUST be big enough to store the output!
 * \param format A string that specifies the format of the output
 * \return The number of characters that are WRITTEN into the buffer, not counting the terminating null character
 */
#define sprintk sprintk_
__printf_like(2, 3) int sprintk_(char *buffer, const char *format, ...);

/**
 * Tiny snprintk/vsnprintk implementation
 * \param buffer A pointer to the buffer where to store the formatted string
 * \param count The maximum number of characters to store in the buffer, including a terminating null character
 * \param format A string that specifies the format of the output
 * \param va A value identifying a variable arguments list
 * \return The number of characters that COULD have been written into the buffer, not counting the terminating
 *         null character. A value equal or larger than count indicates truncation. Only when the returned value
 *         is non-negative and less than count, the string has been completely written.
 */
#define snprintk snprintk_
#define vsnprintk vsnprintk_
__printf_like(3, 4) int snprintk_(char *buffer, size_t count, const char *format, ...);
__printf_like(3, 0) int vsnprintk_(char *buffer, size_t count, const char *format, va_list va);

/**
 * Tiny vprintk implementation
 * \param format A string that specifies the format of the output
 * \param va A value identifying a variable arguments list
 * \return The number of characters that are WRITTEN into the buffer, not counting the terminating null character
 */
#define vprintk vprintk_
__printf_like(1, 0) int vprintk_(const char *format, va_list va);

/**
 * printk with output function
 * You may use this as dynamic alternative to printk() with its fixed _putchar() output
 * \param out An output function which takes one character and an argument pointer
 * \param arg An argument pointer for user data passed to output function
 * \param format A string that specifies the format of the output
 * \return The number of characters that are sent to the output function, not counting the terminating null character
 */
__printf_like(3, 4) int fctprintk(void (*out)(char character, void *arg), void *arg, const char *format, ...);

#ifdef __cplusplus
}
#endif

#endif // _PRINTK_H_
