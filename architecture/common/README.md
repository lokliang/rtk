[简要](../../README.md)

# 通用代码

## 目录结构
```bash
$(ROOT_PATH)
└── architecture/common     # 本目录
    ├── heap                # 动态内存管理程序目录
    ├── none_entry.c        # 封装了配置为裸机时的启动代码
    ├── retarget_heap.c     # 重定向堆管理接口（malloc、free等）
    └── retarget_printf.c   # 开源项目 printf 并重定向接口
```

## *heap* 动态内存管理程序目录
  - heap 目录下，是 RTK 实时内核依赖的重新实现的专用动态内存管理程序；
  - 相比标准库，添加了一些自定义的新特性：
    - 可对任意内存块进行动态管理；
    - 添加从内存结尾开始申请内存的能力；
    - 添加 键值--内存 映射功能；
    - 可配置为 碎片优先 或 速度优先 的内存分配策略；
  - 内存分配策略：未定义或配置 CONFIG_MEMM_TYPE = 1 时，使用碎片优先，配置 CONFIG_MEMM_TYPE = 2 时使用速度优先

## 启动过程
  > 若选中 MIX_OS = y:
  - main.c @ main() ==> os_start() ==> _work_app_main() ==> app_main()

  > 若选中 MIX_RTK = y:
  - main.c @ main() ==> rtk_entry.c @ rtk_entry() ==> rtk_entry.c @ _work_app_main() ==> app_main()

  > 若选中 MIX_K_KIT = y:
  - main.c @ main() ==> k_kit.c @ k_entry() ==> k_kit.c @ _work_app_main() ==> app_main()

  > 若使用裸机:
  - main.c @ main() ==> none_entry.c @ none_entry() ==> app_main()

## 重定向
  - 已实现重定向的接口：
  - memset(), memcpy();
  - malloc(), calloc(), realloc(), free();
  - printf(), vprintf(), sprintf(), snprintf(), vsnprintf();
