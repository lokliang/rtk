#ifndef __BOARD_CONFIG_H__
#define __BOARD_CONFIG_H__

#include "drivers/chip/_hal.h"

#define BOARD_NAME                   "MDK-SIMULATOR" // PCB丝印

extern hal_uart_hdl_t const g_board_uart_cons;

#endif
