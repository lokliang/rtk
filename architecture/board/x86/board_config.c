#include "board_config.h"

hal_uart_hdl_t const g_board_uart_cons = {
    .pin_txd = {255, _GPIO_DIR_IN, _GPIO_PUD_PULL_UP}, // 串口：TXD
    .pin_rxd = {255, _GPIO_DIR_IN, _GPIO_PUD_PULL_UP}, // 串口：RXD
    .id = 0,                                           // 串口ID
    .irq_prior = 0,
    .br = 115200, // 默认波特率
};

hal_uart_hdl_t const g_board_uart_iap = {
    .pin_txd = {255, _GPIO_DIR_IN, _GPIO_PUD_PULL_UP}, // 串口：TXD
    .pin_rxd = {255, _GPIO_DIR_IN, _GPIO_PUD_PULL_UP}, // 串口：RXD
    .id = 1,                                           // 串口ID
    .irq_prior = 0,
    .br = 115200, // 默认波特率
};
