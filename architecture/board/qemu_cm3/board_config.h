#ifndef __BOARD_CONFIG_H__
#define __BOARD_CONFIG_H__

#include "drivers/chip/_hal.h"

extern hal_uart_hdl_t const g_board_uart_cons;
extern hal_uart_hdl_t const g_board_uart_iap;

#endif
