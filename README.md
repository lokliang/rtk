# RTK 简要

- RTK (Real Time Kernel) 是一份用 C 编写，由 shell 和 makefile 构建，适用于常见的资源受限 MCU 的工程框架；
- 附带了一些基本例程，不依赖任何开发板即可快速体验；
- 目前 RTK 包括以下主要模块：
  - 主要用于构建工程的 shell 和 make 脚本
  - 收录的一些常见开源代码
  - 收录的 FreeRTOS 内核
  - 独立的微内核
  - RTK 实时内核
  - 实时内核抽象层
  - shell 模块
  - 一些基本例程

## 目录结构
```bash
$(ROOT_PATH)             # 工程根目录
├── architecture        # 工程构建目录
│   ├── arch            # 不同架构的编译规则。主要为 make 脚本
│   ├── board           # 具体开发板的固定配置
│   ├── chip            # 具体平台的启动代码
│   └── common          # 通用底层代码
├── bin                 # 自动生成的目录，输出二进制文件
├── debug               # 自动生成的目录，输出调试相关文件
├── drivers             # 硬件、设备驱动源文件
├── include             # 工程框架头文件
├── kernel              # 内核源码
├── library             # 静态库
├── release             # 自动生成的目录（使用 '$ ./build.sh release' 输出）
├── source              # 自定义的目录，如官方SDK、自收集的模块等
└── tools               # 工程构建脚本
```

## 内部文档
- [工程构建](tools/README.md)
- [公共接口](include/README.md)
- [通用代码](architecture/common/README.md)
- [微内核](kernel/rtk/src/k_kit/README.md)
- [实时内核](kernel/rtk/README.md)
- [shell](source/components/shell/README.md)
- [参考例程-APP](project/app/app_sample/README.md)
- [参考例程-自动初始化](project/samples/sample_init/README.md)
- [参考例程-内核](project/samples/sample_kernel/README.md)
- [参考例程-内核抽象层](project/samples/sample_os/README.md)
- [参考例程-模块模板](project/samples/templete_device/README.md)

## 开发环境

- **Linux (Ubuntu)**
- Keil-MDK

## 编译运行

为了更好地隔离用户代码与 RTK 框架，工程构建规则现已更新：  
用户工程无需再置于固定的 project 目录下，而只需在首次使用时执行一次 `export.sh` 脚本以设置必要的环境变量，之后可将用户工程放置于任意目录，然后通过执行 `rtk.build.sh` 来构造工程。

### 初始化设置

- 首次使用时，请在工程根目录下执行：
```bash
$ ./export.sh
```
该脚本将配置编译所需的环境变量，后续可直接在任意用户工程目录下使用 `rtk.build.sh` 构建工程。

### 在 Linux 编译下

- 将用户工程代码置于任意目录；
- 进入用户工程目录，在命令行中执行：
```bash
$ rtk.build.sh
```
- 执行过程中会提示选择相关配置项，如：
  - 项目路径（选择或输入对应工程名称）
  - 开发板、芯片、编译规则等细节配置  
  按照提示输入即可。
  
- 编译成功后生成对应的二进制文件。用户可直接运行生成的可执行文件，例如：
```bash
$ ./debug/your_project.elf
```

### 在 MDK 环境下

- 打开用户工程对应的 MDK 工程文件（例如：`project/app/app_sample/MDK/project.uvproj`）；
- 按 "F7" 编译并确认编译成功；
- 按 "Ctrl+F5" 启动仿真调试；
- 打开模拟串口1；
- 按 "F5" 开始运行；
- 在模拟串口窗口中将看到类似如下的打印信息，表示程序运行成功：
```
[INF] | [app] app_main -> app sample

sh:/>
```

## 补充说明

- 请确保首次执行 `export.sh` 后不要更改相关环境变量，否则可能影响后续使用 `rtk.build.sh` 构建工程；
- 如果需要添加或修改交叉编译器，请在配置过程中正确输入完整的编译器路径或名称；
- 运行过程中，配置选项（如路径、开发板、芯片信息）可通过输入序号或关键字快速选择；
- 若遇到权限不足问题，可使用 `chmod +x export.sh rtk.build.sh` 赋予执行权限。
