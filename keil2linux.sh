#!/bin/bash

# 根据 keil 工程，生成新项目。
# 注意：
# - > 变量 DestDir 指定了固定的输出目录
# - > 输出目录不要保存重要文件

DestDir=${PWD}/keil-project

function --last-cp() {
	echo "" >> ${1}/Makefile
	echo "# toolchain make rules" >> ${1}/Makefile
	echo 'include $(ROOT_PATH)/tools/make/config.mk' >> $1/Makefile
}

function --cp-keil-files() {
	# 解析KEIL工程，生成新项目到指定的目录下
	#
	# --cp-keil-files <keil uvproj file> <new dir>
	# param: keil uvproj file      keil的工程文件
	# param: new dir               目标生成目录

	# SrcFile: 来源工程位置
	local SrcFile=`readlink -e $1`

	# TarPath: 输出目录
	local TarPath=$2

	# TarDir: ${TarPath}/${TargetName}/src
	local TarDir=${TarPath}

	# 文件链接到的目标目录
	local OutDir=""

	local TargetName=""
	local GroupName=""
	local FileType=""

	if [ $# -lt 2 ]; then
		echo -e "\033[31m[ERROR]\033[0m ${BASH_SOURCE}:${LINENO} 少于 2 个参数"
		exit 1
	fi

	if [ ! -f ${SrcFile} ]; then
		echo -e "\033[31m[ERROR]\033[0m ${BASH_SOURCE}:${LINENO} 文件：'${SrcFile}' 不存在"
		return 1
	fi

	echo -e "Copy files from: ${SrcFile}"

	cd $(dirname ${SrcFile})
	SrcFile=$(basename ${SrcFile})

	for tmp in `sed -e "s/ //g" ${SrcFile} | grep -E '<TargetName>|<IncludePath>|<Define>|<GroupName>|<FileType>|<FilePath>'`; do

		tmp=${tmp//\\//}

		if [[ "$tmp" =~ "<TargetName>" ]]; then
			tmp=${tmp#*>}
			tmp=${tmp%</*}

			if [ "${TargetName}" != "" ]; then
				--last-cp ${TarDir}
			fi

			TargetName=${tmp}
			GroupName=""
			TarDir=${TarPath}/${TargetName}
			OutDir=${TarDir}/src/${GroupName}

			[ -d ${TarDir} ] && rm -rf ${TarDir}
			mkdir -p ${OutDir}
			OutDir=`readlink -e "${OutDir}"`

			echo -e "    From target: ${TargetName}"

			echo "# Automatically generated file by $(basename ${BASH_SOURCE})" > ${TarDir}/log.md
			echo "# Source keil project file: ${PWD}/${SrcFile}" >> ${TarDir}/log.md
			echo "" >> ${TarDir}/log.md
			echo "# Target: ${TargetName}" >> ${TarDir}/log.md

			echo "# Automatically generated file by $(basename ${BASH_SOURCE})" > ${TarDir}/Makefile
			echo "# Automatically generated file by $(basename ${BASH_SOURCE})" > ${TarDir}/prj.conf
		fi

		if [[ "$tmp" =~ "<IncludePath>" ]] && [[ ! "${tmp}" =~ "<IncludePath></IncludePath>" ]]; then
			tmp=${tmp#*>}
			tmp=${tmp%</*}
			echo "" >> ${TarDir}/Makefile
			echo "" >> ${TarDir}/log.md
			echo -e "## CINCLUDES" >> ${TarDir}/log.md
			for inc in ${tmp//;/ }; do
				if [ -d ${inc} ]; then
					inc=`readlink -e ${inc}`
					if !(grep -q -e "${inc}$" ${TarDir}/Makefile); then
						echo "- > ${inc}" >> ${TarDir}/log.md
						echo "EXT_INC_DIR += ${inc}" >> ${TarDir}/Makefile
					fi
				else
					echo -e "\033[31m[ERROR]\033[0m ${BASH_SOURCE}:${LINENO} 目录 ${inc////\\\\} 不存在"
				fi
			done
		fi

		if [[ "$tmp" =~ "<Define>" ]] && [[ ! "${tmp}" =~ "<Define></Define>" ]]; then
			tmp=${tmp#*>}
			tmp=${tmp%</*}
			for def in ${tmp//,/ }; do
				if [[ "${def}" =~ "=" ]]; then
					echo "${def//=/ = }" >> ${TarDir}/prj.conf
				else
					echo "${def} = y" >> ${TarDir}/prj.conf
				fi
			done
		fi

		if [[ "$tmp" =~ "<GroupName>" ]] && [[ ! "${tmp}" =~ "<GroupName></GroupName>" ]]; then
			tmp=${tmp#*>}
			tmp=${tmp%</*}

			GroupName=${tmp}
			OutDir=${TarDir}/src/${GroupName}
			mkdir -p ${OutDir}
			OutDir=`readlink -e "${OutDir}"`
			echo "" >> ${TarDir}/log.md
			echo "## Group: ${GroupName}" >> ${TarDir}/log.md

			FileType=""
		fi

		if [[ "$tmp" =~ "<FileType>" ]]; then
			tmp=${tmp#*>}
			tmp=${tmp%</*}
			FileType="${tmp}"
		fi

		if [[ "$tmp" =~ "<FilePath>" ]]; then
			tmp=${tmp#*>}
			tmp=${tmp%</*}

			if [ -f ${tmp} ]; then
				if [ "${FileType}" == "1" ] || [ "${FileType}" == "8" ]; then
					tmp=`readlink -e ${tmp}`
					# ln -f ${tmp} ${OutDir}/$(basename ${tmp})
					cp -axf ${tmp} ${OutDir}/$(basename ${tmp})
					echo "- > ${tmp}" >> ${TarDir}/log.md
				fi
				[ "${FileType}" == "2" ] && echo -e "[WRN] \033[33m$(basename ${tmp})\033[0m is asm file"
				[ "${FileType}" == "4" ] && echo -e "[WRN] \033[33m$(basename ${tmp})\033[0m is library file"
			else
				echo -e "\033[31m[ERROR]\033[0m ${BASH_SOURCE}:${LINENO} 文件 ${tmp////\\\\} 不存在"
			fi

			FileType=""
		fi

	done

	--last-cp ${TarDir}
}

if [ $# -ne 1 ]; then
	echo -e "\033[31m[ERROR]\033[0m ${BASH_SOURCE}:${LINENO} 未使用参数指定工程文件"
	exit 1
fi

clear

--cp-keil-files   $1      ${DestDir}

echo -e "The new projects output path: \033[34m${DestDir}\033[0m\n"
