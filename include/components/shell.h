/**
 * @file shell.h
 * @author LokLiang (lokliang@163.com)
 * @brief
 * @version 0.1
 * @date 2023-05-01
 *
 * @copyright Copyright (c) 2023
 *
 */

#ifndef __SHELL_H__
#define __SHELL_H__

#include "sys_types.h"

#if defined(MIX_SHELL)

#include "sh.h"

/* Macros -------------------------------------------------------------------------------------- */

#define SHELL_CMD_FN(NAME) SH_CMD_FN(NAME)

#define SHELL_CMD_CP_FN(NAME) SH_CMD_CP_FN(NAME)

#define SHELL_SETUP_CMD(CMD, HELP, FUNC, SUB_NAME) SH_SETUP_CMD(CMD, HELP, FUNC, SUB_NAME)

#define SHELL_DEF_CMD(NAME, ...) SH_DEF_CMD(NAME, ##__VA_ARGS__)

#define SHELL_DEF_SUB_CMD(SUB_NAME, ...) SH_DEF_SUB_CMD(SUB_NAME, ...)

#define SHELL_REGISTER_CMD(NAME, ...) SH_REGISTER_CMD(NAME, ##__VA_ARGS__)

/* Functions ----------------------------------------------------------------------------------- */

#define shell_set_prompt(HDL, PROMPT) sh_set_prompt(HDL, PROMPT); /* 设置提示符 */

#define shell_putc(HDL, C) sh_putc(HDL, C) /* 记录一个字符到内部缓存中并解析和执行 */

#define shell_putstr(HDL, C) sh_putstr(HDL, C) /* 记录字符串到内部缓存中并解析和执行 */

#define shell_print(HDL, FMT, ...) sh_echo(HDL, FMT, ##__VA_ARGS__)   /* 打印字符串 */
#define shell_log_err(HDL, FMT, ...) sh_echo(HDL, FMT, ##__VA_ARGS__) /* 打印字符串 */
#define shell_log_wrn(HDL, FMT, ...) sh_echo(HDL, FMT, ##__VA_ARGS__) /* 打印字符串 */
#define shell_log_inf(HDL, FMT, ...) sh_echo(HDL, FMT, ##__VA_ARGS__) /* 打印字符串 */
#define shell_log_dbg(HDL, FMT, ...) sh_echo(HDL, FMT, ##__VA_ARGS__) /* 打印字符串 */

/* End ----------------------------------------------------------------------------------------- */

#else /* #if defined(MIX_SHELL) */

#define SHELL_CMD_FN(NAME) __used static int NAME(void *sh_hdl, int argc, const char *argv[])
#define SHELL_CMD_CP_FN(NAME) __used static void NAME(void *sh_hdl, int argc, const char *argv[], bool flag)
#define SHELL_SETUP_CMD(CMD, HELP, FUNC, SUB_NAME)
#define SHELL_DEF_CMD(NAME, ...)
#define SHELL_DEF_SUB_CMD(SUB_NAME, ...)
#define SHELL_REGISTER_CMD(NAME, ...)
#define shell_set_prompt(HDL, PROMPT)
#define shell_putc(HDL, C)
#define shell_putstr(HDL, C)
#define shell_print(HDL, FMT, ...)

#endif /* #if defined(MIX_SHELL) */

#endif
