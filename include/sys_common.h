/**
 * @file sys_common.h
 * @author LokLiang (lokliang@163.com)
 * @brief 使用 mutex 对设备锁定和解锁的统一接口
 * @version 0.1
 * @date 2023-06-07
 *
 * @copyright Copyright (c) 2023
 *
 */

#ifndef __SYS_COMMON_H__
#define __SYS_COMMON_H__

#include "sys_types.h"

// #ifdef __cplusplus
// extern "C" {
// #endif /* #ifdef __cplusplus */

typedef struct sys_lock
{
    void *hdl;
} sys_lock_t;

#define SYS_LOG_FOREVER (~0)

int sys_lock(const sys_lock_t *lock_hdl, size_t wait_ms);
void sys_unlock(const sys_lock_t *lock_hdl);

void sys_delay(size_t sleep_ms);



// #ifdef __cplusplus
// } /* extern "C" */
// #endif /* #ifdef __cplusplus */

#endif
