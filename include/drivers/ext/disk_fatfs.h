/**
 * @file disk_fatfs.h
 * @author LokLiang (lokliang@163.com)
 * @brief 定义 FATFS 专用的移植接口数据结构
 * @version 0.1
 * @date 2023-06-16
 *
 * @copyright Copyright (c) 2023
 *
 */

#ifndef __DISK_FATFS_H__
#define __DISK_FATFS_H__

#include "sys_init.h"

#include "ff.h"
#include "diskio.h"

#ifdef __cplusplus
extern "C"
{
#endif /* #ifdef __cplusplus */

/* Macros --------------------------------------------------------------------*/

#ifndef CONFIG_MAX_VOL_NAME_LEN
#define CONFIG_MAX_VOL_NAME_LEN 16
#endif

/* Typedef -------------------------------------------------------------------*/

typedef const struct device_disk_fatfs *device_disk_fatfs_t;

typedef struct
{
    DSTATUS (*disk_initialize)(void);
    DSTATUS (*disk_status)(void);
    DRESULT (*disk_read)(BYTE *buff, LBA_t sector, UINT count);
    DRESULT (*disk_write)(const BYTE *buff, LBA_t sector, UINT count);
    DRESULT (*disk_ioctl)(BYTE cmd, void *buff);
} device_disk_fatfs_api_t;

typedef struct
{
    int id;
    TCHAR volume_name[CONFIG_MAX_VOL_NAME_LEN];
    TCHAR volume_str[CONFIG_MAX_VOL_NAME_LEN];
    FATFS fs;
} device_disk_fatfs_data_t;

struct device_disk_fatfs
{
    device_disk_fatfs_api_t api;
    device_disk_fatfs_data_t *data;
};

/* Function prototypes -------------------------------------------------------*/

__static_inline device_disk_fatfs_t device_disk_fatfs_binding(const char *name);
__static_inline const char *device_disk_fatfs_search(const char *prefix_name, unsigned int num);

/* Code ----------------------------------------------------------------------*/

__static_inline device_disk_fatfs_t device_disk_fatfs_binding(const char *name)
{
    return (device_disk_fatfs_t)device_binding(name);
}

__static_inline const char *device_disk_fatfs_search(const char *prefix_name, unsigned int num)
{
    return device_search(prefix_name, num);
}

#ifdef __cplusplus
} /* extern "C" */
#endif /* #ifdef __cplusplus */

#endif
