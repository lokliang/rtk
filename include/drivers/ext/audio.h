#ifndef __AUDIO_H__
#define __AUDIO_H__

#include "sys_types.h"

#ifdef __cplusplus
extern "C"
{
#endif

typedef struct
{
   void *handle;
} audio_handle_t;

types_err_t audio_create(audio_handle_t *audio_handle);
void audio_delete(audio_handle_t *audio_handle);

types_err_t audio_play(audio_handle_t *audio_handle);
types_err_t audio_pause(audio_handle_t *audio_handle);
types_err_t audio_resume(audio_handle_t *audio_handle);
types_err_t audio_stop(audio_handle_t *audio_handle);

bool audio_is_playing(audio_handle_t *audio_handle);

#ifdef __cplusplus
}
#endif

#endif
