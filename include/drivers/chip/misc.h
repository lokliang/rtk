#ifndef __MISC_H__
#define __MISC_H__

#ifdef __cplusplus
extern "C"
{
#endif

/**
 * @brief 让 CPU 空转延时
 * 
 * @param us 微秒
 */
void drv_misc_busy_wait(unsigned us);

/**
 * @brief 获取变量的位带映射地址
 *
 * @param mem 变量地址
 * @return 位带映射地址。如无返回 NULL
 */
unsigned *drv_misc_bitband(void *mem);

/**
 * @brief 读取 MCU 身份信息的 MD5 值
 * 
 * @param out[out] 输出
 * @retval 0 成功
 * @retval -1 失败
 */
int drv_misc_read_id_md5(unsigned char out[16]);

/**
 * @brief 设置中断向量地址
 *
 * @param vector 中断向量地址
 */
void drv_misc_set_vector(void *vector);

/**
 * @brief 获取中断向量地址
 *
 * @return void* 中断向量地址
 */
void *drv_misc_get_vector(void);


#ifdef __cplusplus
}
#endif

#endif
