[简要](../README.md)

# 公共接口
  - 公共接口目录为特殊目录，里面仅包含头文件，其路径对所有类型的模块可见。
  - 里面所有的头文件都具有共用性，很可能影响全局，在定义过程中应该特别谨慎，目前依然在探索中，并且可能需要长期维护和修改，务求尽可能在高效、易用、规范和通用中取得平衡。

## 目录结构
```bash
$(ROOT_PATH)
└── include             # 本目录
    ├── components      # 完整独立的软件模块
    ├── drivers         # 硬件驱动
    │   ├── chip        # 常用的片上外设驱动
    │   └── ext         # 常用的板载设备驱动
    ├── list            # 链表工具
    ├── os              # 实时内核抽象层
    ├── sys_init.h      # 自动初始化及数据结构的定义工具
    ├── sys_init.ld     # 自动初始化需求的链接脚本模块
    ├── sys_log.h       # 日志打印工具
    └── sys_types.h     # 集合一些常用数据类型
```

## 应用实例
  - [sys_init.h-实例1](../project/samples/sample_init/src/initialize_template.c)
  - [sys_init.h-实例2](../project/samples/templete_device/src/device_template.h)
  - [sys_init.ld-实例](../architecture/chip/x86/linux/linker_script/gcc.ld)
  - [sys_log.h-实例](../project/app/app_sample/src/app_entry.c)
  - [os-实例](../source/os/MIX_OS.prj.mk)
