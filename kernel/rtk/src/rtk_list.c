/**
 * @file rtk_list.c
 * @author LokLiang (lokliang@163.com)
 * @brief
 * @version 0.1
 * @date 2023-05-01
 *
 * @copyright Copyright (c) 2023
 *
 */

#include "prv_rtk_class.h"

// _rtk_list_atomic() 中的参数 state_insert 的函数体
static void _insert_yield(struct rtk_thread_handle *thread);
static void _insert_resume(struct rtk_thread_handle *thread);

static void _insert_pend(struct rtk_thread_handle *thread);
static void _remove_pend(struct rtk_thread_handle *thread);

static void _insert_suspend(struct rtk_thread_handle *thread);
static void _remove_suspend(struct rtk_thread_handle *thread);

static void _insert_deleted(struct rtk_thread_handle *thread);
static void _remove_deleted(struct rtk_thread_handle *thread);

static void _insert_sleep(struct rtk_thread_handle *thread);
static void _remove_sleep(struct rtk_thread_handle *thread);

static void _insert_wait_sig_sem(struct rtk_thread_handle *thread);
static void _remove_wait_sig_sem(struct rtk_thread_handle *thread);

static void _insert_wait_sig_mutex(struct rtk_thread_handle *thread);
static void _remove_wait_sig_mutex(struct rtk_thread_handle *thread);

static void _rtk_dlist_insert_prior(struct rtk_thread_handle *thread, dlist_t *list, dlist_node_t *node, int dev);

static struct rtk_thread_handle *_rtk_list_peek_self(flist_node_t *node);
static void _rtk_list_insert_head(rtk_list_t *list, struct rtk_list_node *node, rtk_list_fn state_remove);
static void _rtk_list_insert_tail(rtk_list_t *list, struct rtk_list_node *node, rtk_list_fn state_remove);
static void _rtk_list_insert_next(struct rtk_list_node *tar_node, struct rtk_list_node *src_node, rtk_list_fn state_remove);
static bool _rtk_list_is_empty(rtk_list_t *list);

static void _rtk_list_pend_timeup(int systime_tick);

/**
 * @brief 使线程执行有临界保护的链操作：改为所在的绪链的链尾
 *
 * @param thread 被操作的线程
 */
void _rtk_list_atomic_yield(struct rtk_thread_handle *thread)
{
    _rtk_list_atomic(thread, _insert_yield);
}

/**
 * @brief 使线程执行有临界保护的链操作：插入到就绪链的链尾
 *
 * @param thread 被操作的线程
 */
void _rtk_list_atomic_resume(struct rtk_thread_handle *thread)
{
    _rtk_list_atomic(thread, _insert_resume);
}

/**
 * @brief 使线程执行有临界保护的链操作：插入到就绪链的链尾
 *
 * @param thread 被操作的线程
 */
void _rtk_list_atomic_pend(struct rtk_thread_handle *thread)
{
    _rtk_list_atomic(thread, _insert_pend);
}

/**
 * @brief 使线程执行有临界保护的链操作：仅移出所在链
 *
 * @param thread 被操作的线程
 */
void _rtk_list_atomic_suspend(struct rtk_thread_handle *thread)
{
    _rtk_list_atomic(thread, _insert_suspend);
}

/**
 * @brief 使线程执行有临界保护的链操作：移出所在链并删除使用痕迹
 *
 * @param thread 被操作的线程
 */
void _rtk_list_atomic_deleted(struct rtk_thread_handle *thread)
{
    _rtk_list_atomic(thread, _insert_deleted);
}

/**
 * @brief 使线程执行有临界保护的链操作：插入到延时链
 *
 * @param thread 被操作的线程
 */
void _rtk_list_atomic_sleep(struct rtk_thread_handle *thread)
{
    _rtk_list_atomic(thread, _insert_sleep);
}

/**
 * @brief 使线程执行有临界保护的链操作：插入到等待信号的链，并补充执行信号量的动作
 *
 * @param thread 被操作的线程
 */
void _rtk_list_atomic_wait_sem(struct rtk_thread_handle *thread)
{
    _rtk_list_atomic(thread, _insert_wait_sig_sem);
}

/**
 * @brief 使线程执行有临界保护的链操作：插入到等待信号的链，并补充执行互斥量的动作
 *
 * @param thread 被操作的线程
 */
void _rtk_list_atomic_wait_mutex(struct rtk_thread_handle *thread)
{
    _rtk_list_atomic(thread, _insert_wait_sig_mutex);
}

/**
 * @brief 专用临界队列操作。
 * 使线程按指定函数执行针对不同的状态链的操作
 *
 * @param thread 被操作的线程
 * @param state_insert 执行链操作的内部的函数
 */
void _rtk_list_atomic(struct rtk_thread_handle *thread, void (*state_insert)(struct rtk_thread_handle *thread))
{
    _RTK_ASS_FALSE(thread == NULL, "");

    thread->state_node.state_insert = state_insert;

    for (;;)
    {
        size_t nest_int = rtk_cpu_interrupt_save();
        int systime_tick = cm_rtk->systime_tick;
        cm_rtk->nest_sched++;
        if (cm_rtk->nest_list++ == 0)
        {
            rtk_cpu_interrupt_restore(nest_int);

            /*
             * 此循环内通过 cm_rtk->nest_list 标记，实现原子操作，同时调度器被关闭
             */
            for (;;)
            {
                /* 把到达指定时间的线程节点插入到就绪链 */
                _rtk_list_pend_timeup(systime_tick);

                /* 执行链连接 */
                if (state_insert)
                {
                    state_insert(thread);
                    state_insert = NULL;
                }

                /*
                 * 通过短暂的关中，从 cm_rtk->slist_critical 中提取被中断重入时新插入的线程节点
                 */
                nest_int = rtk_cpu_interrupt_save();
                slist_node_t *critical_node = slist_take_head(&cm_rtk->slist_critical);
                if (critical_node)
                {
                    thread = _RTK_CONTAINER_OF(critical_node, struct rtk_thread_handle, critical_node);
                    state_insert = thread->state_node.state_insert;
                    rtk_cpu_interrupt_restore(nest_int);
                }
                else
                {
                    --cm_rtk->nest_list;
                    --cm_rtk->nest_sched;
                    rtk_cpu_interrupt_restore(nest_int);
                    break;
                }
            }

            if (systime_tick != *(volatile int *)&cm_rtk->systime_tick)
            {
                continue;
            }

            if (curr_thread_handle != next_thread_handle)
            {
                _rtk_scheduler_to_highest();
            }
        }
        else // cm_rtk->nest_list != NULL
        {
            /*
             * 根据 cm_rtk->nest_list 标记，中断重入时将执行此块，
             * 通过短暂的关中，将线程的节点插入到 cm_rtk->slist_critical 中
             */
            if (!slist_is_pending(&thread->critical_node))
            {
                slist_insert_tail(&cm_rtk->slist_critical, &thread->critical_node);
            }
            --cm_rtk->nest_list;
            --cm_rtk->nest_sched;
            rtk_cpu_interrupt_restore(nest_int);
        }

        return;
    }
}

/**
 * @brief 使用与 _rtk_list_atomic() 方式一样的临界策略，原子执行 _rtk_list_pend_timeup()
 */
void _rtk_list_atomic_pend_timeup(void)
{
    size_t nest_int = rtk_cpu_interrupt_save();
    cm_rtk->nest_sched++;
    if (cm_rtk->nest_list++ == 0)
    {
        rtk_cpu_interrupt_restore(nest_int);

        _rtk_list_pend_timeup(cm_rtk->systime_tick);

        nest_int = rtk_cpu_interrupt_save();
        --cm_rtk->nest_list;
        --cm_rtk->nest_sched;
        rtk_cpu_interrupt_restore(nest_int);

        if (curr_thread_handle != next_thread_handle)
        {
            _rtk_scheduler_to_highest();
        }
    }
    else // cm_rtk->nest_list != NULL
    {
        --cm_rtk->nest_list;
        --cm_rtk->nest_sched;
        rtk_cpu_interrupt_restore(nest_int);
    }
}

/**
 * @brief 比较时间，把就绪链中到达时间的线程句柄插入到就绪链中
 *
 * @param systime_tick 当前系统时间计数值
 */
static void _rtk_list_pend_timeup(int systime_tick)
{
    while ((int)(systime_tick - *(volatile int *)&cm_rtk->systime_early) >= 0)
    {
        struct rtk_thread_handle *test_thread = _rtk_list_peek_head(&cm_rtk->list_sleep);
        if (test_thread == NULL)
        {
            cm_rtk->systime_early = systime_tick + _SIGNED_MAX(cm_rtk->systime_early);
            return;
        }
        else
        {
            _insert_pend(test_thread);
        }
    }
}

/**
 * @brief 线程状态链专用，初始化链
 *
 * @param list
 *   @arg &cm_rtk->list_pend[n]
 *   @arg &cm_rtk->list_sleep
 */
void _rtk_list_init_list(rtk_list_t *list)
{
    flist_init_list(list);
}

/**
 * @brief 线程状态链专用，初始化节点
 *
 * @param node
 *   @arg &thread->state_node
 */
void _rtk_list_init_node(struct rtk_list_node *node)
{
    flist_init_node(&node->node);
    node->state_remove = NULL;
}

/**
 * @brief 线程状态链专用，查询链中首个节点对应的线程句柄实例
 *
 * @param list
 *   @arg &cm_rtk->list_pend[n]
 *   @arg &cm_rtk->list_sleep
 *
 * @return struct rtk_thread_handle* 线程句柄实例
 */
struct rtk_thread_handle *_rtk_list_peek_head(rtk_list_t *list)
{
    return _rtk_list_peek_self(flist_peek_head(list));
}

/**
 * @brief 线程状态链专用，查询链中最后一个节点对应的线程句柄实例
 *
 * @param list
 *   @arg &cm_rtk->list_pend[n]
 *   @arg &cm_rtk->list_sleep
 *
 * @return struct rtk_thread_handle* 线程句柄实例
 */
struct rtk_thread_handle *_rtk_list_peek_tail(rtk_list_t *list)
{
    return _rtk_list_peek_self(flist_peek_tail(list));
}

/**
 * @brief 线程状态链专用，查询下一个节点的线程句柄实例
 *
 * @param list
 *   @arg &cm_rtk->list_pend[n]
 *   @arg &cm_rtk->list_sleep
 *
 * @return struct rtk_thread_handle* 线程句柄实例
 */
struct rtk_thread_handle *_rtk_list_peek_next(struct rtk_list_node *node)
{
    return _rtk_list_peek_self(flist_peek_next(&node->node));
}

/**
 * @brief 线程状态链专用，查询上一个节点的线程句柄实例
 *
 * @param list
 *   @arg &cm_rtk->list_pend[n]
 *   @arg &cm_rtk->list_sleep
 *
 * @return struct rtk_thread_handle* 线程句柄实例
 */
struct rtk_thread_handle *_rtk_list_peek_prev(struct rtk_list_node *node)
{
    return _rtk_list_peek_self(flist_peek_prev(&node->node));
}

/**
 * @brief 线程状态链专用，取出首个节点并返回对应的线程句柄实例
 *
 * @param list
 *   @arg &cm_rtk->list_pend[n]
 *   @arg &cm_rtk->list_sleep
 *
 * @return struct rtk_thread_handle* 线程句柄实例
 */
struct rtk_thread_handle *_rtk_list_take(rtk_list_t *list)
{
    flist_node_t *node = flist_take_head(list);
    if (node)
    {
        struct rtk_list_node *state_node = _RTK_CONTAINER_OF(node, struct rtk_list_node, node);
        rtk_list_fn remove = state_node->state_remove;
        struct rtk_thread_handle *self_thread = _RTK_CONTAINER_OF(state_node, struct rtk_thread_handle, state_node);
        state_node->state_remove = NULL;
        if (remove)
        {
            remove(self_thread);
        }
        return self_thread;
    }
    return NULL;
}

/**
 * @brief 线程状态链专用，设置下一个节点为首个节点
 *
 * @param list
 *   @arg &cm_rtk->list_pend[n]
 *   @arg &cm_rtk->list_sleep
 *
 * @return true
 * @return false
 */
bool _rtk_list_next(rtk_list_t *list)
{
    _RTK_ASS_FALSE(list == NULL, "");
    if (flist_peek_head(list) == flist_peek_tail(list))
    {
        return false;
    }
    else
    {
        flist_set_next(list);
        return true;
    }
}

static void _insert_yield(struct rtk_thread_handle *thread)
{
    thread->slice_count = thread->control.slice_ticks;

    if (thread->curr_state == THREAD_STATE_PENDING)
    {
        rtk_list_t *list_pend = &cm_rtk->list_pend[thread->curr_prior];
        flist_node_t *node = flist_peek_tail(list_pend);
        _RTK_ASS_FALSE(list_pend == NULL, "");
        if (&thread->state_node.node != node && flist_peek_head(list_pend) != node)
        {
            flist_insert_tail(list_pend, &thread->state_node.node);
            size_t highest_prior = _rtk_prior_highest(&cm_rtk->prior_index_pend);
            next_thread_handle = _rtk_list_peek_head(&cm_rtk->list_pend[highest_prior]);
        }
    }
}

static void _insert_resume(struct rtk_thread_handle *thread)
{
    thread->wakeup_tick = cm_rtk->systime_tick;
    _insert_pend(thread);
}

static void _insert_pend(struct rtk_thread_handle *thread)
{
    size_t new_prior = thread->control.priority;
    dlist_node_t *prior_node = dlist_peek_head(&thread->blocked_list);

    if (prior_node)
    {
        struct rtk_thread_handle *wait_thread = _RTK_CONTAINER_OF(prior_node, struct rtk_thread_handle, sig_node);
        if (new_prior < wait_thread->curr_prior)
        {
            new_prior = wait_thread->curr_prior;
        }
    }

    if (thread->curr_state == THREAD_STATE_PENDING && thread->curr_prior == new_prior)
    {
        return;
    }

    thread->curr_state = THREAD_STATE_PENDING;

    rtk_list_t *list_pend = &cm_rtk->list_pend[new_prior];
    _rtk_list_insert_tail(list_pend, &thread->state_node, _remove_pend);
    _rtk_prior_set(&cm_rtk->prior_index_pend, new_prior);

    size_t highest_prior = _rtk_prior_highest(&cm_rtk->prior_index_pend);
    next_thread_handle = _rtk_list_peek_head(&cm_rtk->list_pend[highest_prior]);

    thread->curr_prior = new_prior;
}

static void _remove_pend(struct rtk_thread_handle *thread)
{
    thread->slice_count = thread->control.slice_ticks;

    size_t curr_prior = thread->curr_prior;
    rtk_list_t *list_pend = &cm_rtk->list_pend[curr_prior];
    if (_rtk_list_is_empty(list_pend))
    {
        _rtk_prior_reset(&cm_rtk->prior_index_pend, curr_prior);
    }

    size_t highest_prior = _rtk_prior_highest(&cm_rtk->prior_index_pend);
    if (highest_prior < sizeof(cm_rtk->list_pend) / sizeof(cm_rtk->list_pend[0]))
    {
        next_thread_handle = _rtk_list_peek_head(&cm_rtk->list_pend[highest_prior]);
    }
    else
    {
        next_thread_handle = cm_rtk->thread_idle.hdl;
    }
}

static void _insert_suspend(struct rtk_thread_handle *thread)
{
    if (thread->curr_state == THREAD_STATE_SUSPEND)
    {
        return;
    }
    thread->curr_state = THREAD_STATE_SUSPEND;

    /* 从原链表中移除并设置新的移除函数 */
    _rtk_list_insert_head(NULL, &thread->state_node, _remove_suspend);
}

static void _remove_suspend(struct rtk_thread_handle *thread)
{
    _RTK_UNUSE(thread);
}

static void _insert_deleted(struct rtk_thread_handle *thread)
{
    if (thread->curr_state == THREAD_STATE_CLOSED)
    {
        return;
    }
    thread->curr_state = THREAD_STATE_CLOSED;
    thread->thread_handle->hdl = NULL;

    _rtk_list_insert_head(NULL, &thread->state_node, _remove_deleted);
    rtk_thread_list_reset();
    slist_remove(&cm_rtk->slist_active, &thread->active_node);
}

static void _remove_deleted(struct rtk_thread_handle *thread)
{
    _RTK_UNUSE(thread);
}

static void _insert_sleep(struct rtk_thread_handle *thread)
{
    if (thread->curr_state == THREAD_STATE_DELAYED)
    {
        return;
    }
    thread->curr_state = THREAD_STATE_DELAYED;

    /* 按延时时间分组和顺序插入到对应的链表相应的位置 */
    int wakeup_tick = thread->wakeup_tick;
    rtk_list_t *list_sleep = &cm_rtk->list_sleep;
    struct rtk_thread_handle *test_thread = _rtk_list_peek_head(list_sleep);

    if (test_thread == NULL)
    {
        cm_rtk->systime_early = wakeup_tick;
        _rtk_list_insert_head(list_sleep, &thread->state_node, _remove_sleep);
    }
    else
    {
        struct rtk_thread_handle *tail_thread = _rtk_list_peek_tail(list_sleep);
        struct rtk_thread_handle *prev_thread = NULL;

        do
        {
            if ((int)(wakeup_tick - test_thread->wakeup_tick) < 0)
            {
                break;
            }

            prev_thread = test_thread;
            test_thread = _rtk_list_peek_next(&test_thread->state_node);

        } while (prev_thread != tail_thread);

        if (prev_thread == NULL)
        {
            cm_rtk->systime_early = wakeup_tick;
            _rtk_list_insert_head(list_sleep, &thread->state_node, _remove_sleep);
        }
        else
        {
            _rtk_list_insert_next(&prev_thread->state_node, &thread->state_node, _remove_sleep);
        }
    }
}

static void _remove_sleep(struct rtk_thread_handle *thread)
{
    _RTK_UNUSE(thread);
    struct rtk_thread_handle *test_thread = _rtk_list_peek_head(&cm_rtk->list_sleep);
    int systime_tick = cm_rtk->systime_tick;

    if (test_thread == NULL)
    {
        cm_rtk->systime_early = systime_tick + _SIGNED_MAX(cm_rtk->systime_early);
    }
    else if ((int)(systime_tick - test_thread->wakeup_tick) < 0)
    {
        cm_rtk->systime_early = test_thread->wakeup_tick;
    }
    else
    {
        cm_rtk->systime_early = systime_tick;
    }
}

static void _insert_wait_sig_sem(struct rtk_thread_handle *thread)
{
    if (thread->curr_state == THREAD_STATE_WAIT_SEM)
    {
        return;
    }
    thread->curr_state = THREAD_STATE_WAIT_SEM;

    if (thread->wait_forever)
    {
        _insert_suspend(thread);
    }
    else
    {
        _insert_sleep(thread);
    }

    thread->state_node.state_remove = _remove_wait_sig_sem;

    struct rtk_sem_handle *sem = thread->sem;
    if (sem->type == RTK_SEM_TYPE_FIFO)
    {
        dlist_insert_tail(&sem->sig_list, &thread->sig_node);
    }
    else
    {
        _rtk_dlist_insert_prior(
            thread,
            &sem->sig_list,
            &thread->sig_node,
            -(int)&((struct rtk_thread_handle *)0)->sig_node);
    }

    for (;;)
    {
        thread = NULL;
        size_t nest_int = rtk_cpu_interrupt_save();
        if (sem->value)
        {
            thread = rtk_sem_peek_waiting((rtk_sem_t *)&sem);
            if (thread && (thread->flag & _RTK_THREAD_FLAG_SEM) == 0)
            {
                thread->flag |= _RTK_THREAD_FLAG_SEM;
                --sem->value;
            }
        }
        rtk_cpu_interrupt_restore(nest_int);

        if (thread)
        {
            _insert_pend(thread);
        }
        else
        {
            break;
        }
    }
}

static void _remove_wait_sig_sem(struct rtk_thread_handle *thread)
{
    _remove_sleep(thread);
    dlist_remove(&thread->sem->sig_list, &thread->sig_node);
}

static void _insert_wait_sig_mutex(struct rtk_thread_handle *thread)
{
    struct rtk_mutex_handle *mutex = thread->mutex;
    _RTK_ASS_FALSE(mutex == NULL, "");

    struct rtk_thread_handle *hold_thread = mutex->hold_thread;
    _RTK_ASS_FALSE(hold_thread == NULL, "");
    _RTK_ASS_FALSE(hold_thread == thread, "");

    if (thread->wait_forever)
    {
        _insert_suspend(thread);
    }
    else
    {
        _insert_sleep(thread);
    }

    thread->curr_state = THREAD_STATE_WAIT_MUTEX;
    thread->state_node.state_remove = _remove_wait_sig_mutex;

    /*
     * 把 thread->sig_node 按优先级顺序，插入到 hold_thread->blocked_list
     * 用于已锁定的线程在解锁时，找到并唤醒被堵塞的线程
     * 用于已锁定的线程的优先级反转依据
     */
    _rtk_dlist_insert_prior(
        thread,
        &hold_thread->blocked_list,
        &thread->sig_node,
        -(int)&((struct rtk_thread_handle *)0)->sig_node);

    /*
     * 在 _insert_pend() 里将被自动分析和更新优先级
     */
    if (hold_thread->curr_state == THREAD_STATE_PENDING)
    {
        _insert_pend(hold_thread);
    }
}

static void _remove_wait_sig_mutex(struct rtk_thread_handle *thread)
{
    struct rtk_mutex_handle *mutex = thread->mutex;
    _RTK_ASS_FALSE(mutex == NULL, "");

    struct rtk_thread_handle *hold_thread = mutex->hold_thread;
    _RTK_ASS_FALSE(hold_thread == NULL, "");

    _remove_sleep(thread);

    dlist_remove(&hold_thread->blocked_list, &thread->sig_node);
    if (hold_thread->curr_state == THREAD_STATE_PENDING)
    {
        _insert_pend(hold_thread);
    }
}

static void _rtk_dlist_insert_prior(struct rtk_thread_handle *thread, dlist_t *list, dlist_node_t *node, int dev)
{
    dlist_node_t *test_node = dlist_peek_head(list);

    if (test_node == NULL)
    {
        dlist_insert_font(list, node);
    }
    else
    {
        dlist_node_t *tail_node = dlist_peek_tail(list);
        dlist_node_t *prev_node = NULL;

        do
        {
            struct rtk_thread_handle *test_thread = ((struct rtk_thread_handle *)&((uint8_t *)test_node)[dev]);

            if (thread->curr_prior > test_thread->curr_prior)
            {
                break;
            }

            prev_node = test_node;
            test_node = dlist_peek_next(list, test_node);

        } while (prev_node != tail_node);

        if (prev_node == NULL)
        {
            dlist_insert_font(list, node);
        }
        else
        {
            dlist_insert_next(list, prev_node, node);
        }
    }
}

static struct rtk_thread_handle *_rtk_list_peek_self(flist_node_t *node)
{
    if (node)
    {
        struct rtk_list_node *state_node = _RTK_CONTAINER_OF(node, struct rtk_list_node, node);
        struct rtk_thread_handle *self_thread = _RTK_CONTAINER_OF(state_node, struct rtk_thread_handle, state_node);
        return self_thread;
    }
    else
    {
        return NULL;
    }
}

static void _rtk_list_insert_head(rtk_list_t *list, struct rtk_list_node *node, rtk_list_fn state_remove)
{
    struct rtk_list_node *state_node = node;
    rtk_list_fn remove = state_node->state_remove;
    state_node->state_remove = state_remove;
    if (list != NULL)
    {
        flist_insert_font(list, &state_node->node);
    }
    else
    {
        flist_node_free(&state_node->node);
    }
    if (remove)
    {
        struct rtk_thread_handle *self_thread = _RTK_CONTAINER_OF(state_node, struct rtk_thread_handle, state_node);
        remove(self_thread);
    }
}

static void _rtk_list_insert_tail(rtk_list_t *list, struct rtk_list_node *node, rtk_list_fn state_remove)
{
    struct rtk_list_node *state_node = node;
    rtk_list_fn remove = state_node->state_remove;
    state_node->state_remove = state_remove;
    if (list != NULL)
    {
        flist_insert_tail(list, &state_node->node);
    }
    else
    {
        flist_node_free(&state_node->node);
    }
    if (remove)
    {
        struct rtk_thread_handle *self_thread = _RTK_CONTAINER_OF(state_node, struct rtk_thread_handle, state_node);
        remove(self_thread);
    }
}

static void _rtk_list_insert_next(struct rtk_list_node *tar_node, struct rtk_list_node *src_node, rtk_list_fn state_remove)
{
    _RTK_ASS_FALSE(tar_node == NULL, "");
    _RTK_ASS_FALSE(src_node == NULL, "");
    struct rtk_list_node *state_node = src_node;
    rtk_list_fn remove = state_node->state_remove;
    state_node->state_remove = state_remove;
    flist_insert_next(&tar_node->node, &state_node->node);
    if (remove)
    {
        struct rtk_thread_handle *self_thread = _RTK_CONTAINER_OF(state_node, struct rtk_thread_handle, state_node);
        remove(self_thread);
    }
}

static bool _rtk_list_is_empty(rtk_list_t *list)
{
    return (bool)!flist_peek_head(list);
}
