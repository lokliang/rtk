/**
 * @file rtk_entry.c
 * @author LokLiang (lokliang@163.com)
 * @brief
 * @version 0.1
 * @date 2023-05-01
 *
 * @copyright Copyright (c) 2023
 *
 */

#include "prv_rtk_class.h"
#include "sys_init.h"

#ifndef CONFIG_RTK_THREAD_MAIN_STACK_SIZE
#define CONFIG_RTK_THREAD_MAIN_STACK_SIZE 0x2000
#endif

#ifndef CONFIG_RTK_THREAD_MAIN_PRIORITY
#define CONFIG_RTK_THREAD_MAIN_PRIORITY (CONFIG_RTK_MAX_PRIORITY / 3)
#endif

#ifndef CONFIG_RTK_THREAD_TIMER_STACK_SIZE
#define CONFIG_RTK_THREAD_TIMER_STACK_SIZE 0x800
#endif

#define _INIT_EXPORT_LEADER(NAME, LEVEL, PRIOR)                           \
    static int __sys_init_fn_##NAME(void)                                 \
    {                                                                     \
        return 0;                                                         \
    }                                                                     \
    __used __section(".s_sys_init_t." #LEVEL "." #PRIOR) sys_init_t const \
        __sys_init_##NAME = {                                             \
            .fn = __sys_init_fn_##NAME,                                   \
            .fn_name = #NAME,                                             \
            .line = __LINE__,                                             \
            .file = __FILE__,                                             \
            .level = LEVEL,                                               \
            .prior = PRIOR,                                               \
    }

#define _MODULE_EXPORT_LEADER(LEVEL)                            \
    __used __section(".sys_module_data." #LEVEL) module_t const \
        __sys_module_leader_##LEVEL = {                         \
            .name = "__sys_module_leader_" #LEVEL,              \
            .obj = NULL,                                        \
            .type = (module_type_t)~0,                          \
            .line = __LINE__,                                   \
            .file = __FILE__,                                   \
    }

_INIT_EXPORT_LEADER(leader_0, 0, 0);  // sys_init_t __sys_init_leader_0
_INIT_EXPORT_LEADER(leader_1, 1, 0);  // sys_init_t __sys_init_leader_1
_INIT_EXPORT_LEADER(leader_e, 9, 99); // sys_init_t __sys_init_leader_e

_MODULE_EXPORT_LEADER(0); // module_t __sys_module_leader_0
_MODULE_EXPORT_LEADER(9); // module_t __sys_module_leader_9

static void _sys_init(const sys_init_t *start, const sys_init_t *end)
{
    while (start < end)
    {
        start->fn();
        start = &start[1];
    }
}

__weak int app_main(void)
{
    SYS_LOG_ERR("no app to run");
    return -1;
}

static void _work_app_main(void *arg)
{
    static uint8_t init_flag = 0;
    if (init_flag == 0)
    {
        init_flag = 1;
        _sys_init(&__sys_init_leader_1, &__sys_init_leader_e);
    }

    app_main();
}

/**
 * @brief 指定堆内存并运行内核（包含 rtk_scheduler_init() 和 rtk_scheduler_start()）。
 * 内核运行后，首个任务为 int app_main(void)
 * 具体行为可查看相关代码
 *
 * @param heap 堆内存地址
 * @param size 堆内存大小（字节）
 * @return int -1
 */
int rtk_entry(void *heap, size_t size)
{
    rtk_scheduler_init(heap, size);

    _sys_init(&__sys_init_leader_0, &__sys_init_leader_1);

    rtk_work_q_create(default_rtk_work_q_hdl,            // rtk_work_q_t *work_q_handle,
                      "app-work_q",                      // const char *name,
                      CONFIG_RTK_THREAD_MAIN_STACK_SIZE, // size_t stack_size,
                      CONFIG_RTK_THREAD_MAIN_PRIORITY);  // rtk_prior_t priority

#if (CONFIG_RTK_THREAD_TIMER_STACK_SIZE > 0)
    rtk_timer_q_open(CONFIG_RTK_THREAD_TIMER_STACK_SIZE, CONFIG_RTK_MAX_PRIORITY);
#endif

    static rtk_work_t _work_hdl_init;
    rtk_work_create(&_work_hdl_init, "work-main", _work_app_main, NULL, 3);
    rtk_work_submit(default_rtk_work_q_hdl, &_work_hdl_init, 0);

    rtk_scheduler_start();

    return -1;
}

/* 自动启动 shell ------------------------------------------------------------------ */

#if defined(MIX_SHELL)

#include "sh.h"
#include "board_config.h"

static rtk_work_t s_work_handler_shell;

static void _isr_console(void)
{
    kk_work_submit(WORK_Q_HDL, &s_work_handler_shell, 0);
}

static void _work_sh(void *arg)
{
    char c;
    while (drv_uart_poll_read(g_board_uart_cons.id, &c) > 0)
    {
        sh_putc(&g_uart_handle_vt100, c);
    }
    kk_work_later(10);
}

static int _create_shell_work(void)
{
    kk_work_create(&s_work_handler_shell, _work_sh, NULL, 0);
    kk_work_submit(WORK_Q_HDL, &s_work_handler_shell, 0);

    drv_uart_irq_callback_enable(g_board_uart_cons.id, _isr_console);
    drv_uart_irq_enable(g_board_uart_cons.id, true, false, 15);
    return 0;
}
INIT_EXPORT_APP(_create_shell_work, 0);

static int _show_shell_ver(void)
{
    sh_set_prompt(&g_uart_handle_vt100, "sh:/> ");
    sh_putstr_quiet(&g_uart_handle_vt100, "sh version");
    sh_putc(&g_uart_handle_vt100, '\r');
    return 0;
}
_INIT_EXPORT(_show_shell_ver, 9, 98);

#endif /* #if defined(MIX_SHELL) */
