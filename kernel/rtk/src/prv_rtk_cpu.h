/**
 * @file prv_rtk_cpu.h
 * @author LokLiang (lokliang@163.com)
 * @brief
 * @version 0.1
 * @date 2023-05-01
 *
 * @copyright Copyright (c) 2023
 *
 */

#ifndef __RTK_CPU_H__
#define __RTK_CPU_H__

#include <stddef.h>
#include <stdbool.h>

#include "prv_rtk_class.h"

#ifdef __cplusplus
extern "C" {
#endif /* #ifdef __cplusplus */

/**
 * @def 线程和调度
 */
void *rtk_cpu_stack_init(void *entry,       // 线程函数入口
                         void *arg,         // 线程参数
                         void *stack_begin, // 指栈底，如向下生长时， stack_begin 的地址将高于 stack_end
                         void *stack_end,   // 指栈顶，如向下生长时， stack_end 的地址将高于 stack_begin
                         void *exit);       // 线程结束时的返回函数的地址

void rtk_cpu_delete_thread(void *from); // 结束一个线程

void rtk_cpu_scheduler_start(void *to); // 开始多任务环境

void rtk_cpu_switch_to(void *from, void *to); // 执行一次上下文切换

size_t rtk_cpu_interrupt_save(void);         // 屏蔽中断，并返回递增有的值
void rtk_cpu_interrupt_restore(size_t mask); // 设置内部中断屏蔽计数器的值，值为0时恢复中断

/**
 * @def 系统时钟
 */
void rtk_tick_init(size_t ticks_per_sec); // 初始化滴答时钟，使每秒产生多少次中断

/**
 * @def 中断状态
 */
bool rtk_cpu_is_isr(void); // 查询当前是否运行在中断模式

#ifdef __cplusplus
} /* extern "C" */
#endif /* #ifdef __cplusplus */

#endif
