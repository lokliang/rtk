/**
 * @file rtk_timer.h
 * @author LokLiang (lokliang@163.com)
 * @brief
 * @version 0.1
 * @date 2023-05-01
 *
 * @copyright Copyright (c) 2023
 *
 */

#ifndef __RTK_TIMER_H__
#define __RTK_TIMER_H__

#include "rtk_def.h"
#include <stdbool.h>

#ifdef __cplusplus
extern "C"
{
#endif /* #ifdef __cplusplus */

rtk_err_t rtk_timer_q_open(size_t stack_size, rtk_prior_t priority); // 打开软定时器服务（将占用一个线程）。系统启动后该线程默认为未开启
rtk_err_t rtk_timer_q_close(void);                                   // 关闭软定时器服务
rtk_err_t rtk_timer_q_start(void);                                   // 启动软定时器服务（打开软定时器服务默认启动）
rtk_err_t rtk_timer_q_stop(void);                                    // 停止软定时器服务

typedef struct
{
    rtk_hdl_t hdl;
} rtk_timer_t;

typedef void (*rtk_timer_fn)(void *arg);

rtk_err_t rtk_timer_create(rtk_timer_t *timer_handle, rtk_timer_fn timer_route, void *arg, u8_t sub_prior); // 创建由软定时器服务管理的任务
rtk_err_t rtk_timer_delete(rtk_timer_t *timer_handle);                                                      // 删除由软定时器服务管理的任务

rtk_err_t rtk_timer_set_period(rtk_timer_t *timer_handle, bool periodic, rtk_tick_t period); // 设置重装值
rtk_err_t rtk_timer_start(rtk_timer_t *timer_handle, rtk_tick_t delay_ticks);                // 开始计时（创建时默认开始计时）
rtk_err_t rtk_timer_stop(rtk_timer_t *timer_handle);                                         // 停止计时

bool       rtk_timer_is_periodic(rtk_timer_t *timer_handle); // 查询当前是否自动重装
bool       rtk_timer_is_pending(rtk_timer_t *timer_handle);  // 获取任务是否在就绪或延时的状态
bool       rtk_timer_is_valid(rtk_timer_t *timer_handle);    // 查询线程句柄是否有效
rtk_tick_t rtk_timer_get_period(rtk_timer_t *timer_handle);  // 查询当前的自动重装值
rtk_tick_t rtk_timer_get_remain(rtk_timer_t *timer_handle);  // 查询距离下个执行的剩余时间

#ifdef __cplusplus
} /* extern "C" */
#endif /* #ifdef __cplusplus */

#endif
