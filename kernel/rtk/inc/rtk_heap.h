/**
 * @file rtk_heap.h
 * @author LokLiang (lokliang@163.com)
 * @brief
 * @version 0.1
 * @date 2023-05-01
 *
 * @copyright Copyright (c) 2023
 *
 */

#ifndef __RTK_RTK_HEAP_H__
#define __RTK_RTK_HEAP_H__

#include "rtk_def.h"

#ifdef __cplusplus
extern "C"
{
#endif /* #ifdef __cplusplus */

void *rtk_heap_malloc(size_t size, rtk_tick_t wait_ticks);           // 申请内存
void *rtk_heap_calloc(size_t size, rtk_tick_t wait_ticks);           // 申请内存并置0
void *rtk_heap_malloc_tail(size_t size, rtk_tick_t wait_ticks);      // 申请内存；优先从空闲块的尾部申请
void *rtk_heap_calloc_tail(size_t size, rtk_tick_t wait_ticks);      // 申请内存并置0；优先从空闲块的尾部申请
void *rtk_heap_realloc(void *p, size_t size, rtk_tick_t wait_ticks); // 重定义已申请的内存大小
void  rtk_heap_free(void *p);                                        // 释放内存

bool   rtk_heap_is_valid(void *p);   // 获取内存指针是否有效
size_t rtk_heap_block_size(void *p); // 已申请内存块的实际占用空间大小（不含所有控制信息）（字节）
size_t rtk_heap_space_size(void *p); // 已申请内存块的实际占用空间大小（含所有控制信息）（字节）

size_t rtk_heap_used_size(void); // 获取总已使用空间
size_t rtk_heap_free_size(void); // 获取总空闲空间（包含所有碎片）
size_t rtk_heap_block_max(void); // 获取当前最大的连续空间

rtk_err_t rtk_heap_info(size_t *dst_used, size_t *dst_free, size_t *dst_max_block); // 获取堆空间信息

void *rtk_heapk_malloc(size_t mem_size, const void *key_base, uint8_t key_size, rtk_tick_t wait_ticks); // 在散列表中申请一个带映射键值的内存。

void      rtk_heapk_clr_key(void *p); // 只删除关键字，但所申请的内存依然保留并可用
rtk_err_t rtk_heapk_free(void *p);    // 由 heapk_CreateTab() 所申请获得的指针

void rtk_heapk_list_reset(void);                                                                 // 重置列举指针
bool rtk_heapk_list_next(void **store_mem_base, void **store_key_base, uint8_t *store_key_size); // 列举下个已申请的内存信息

bool    rtk_heapk_is_valid(void *p);                                  // 获取内存指针是否有效
void   *rtk_heapk_block_base(const void *key_base, uint8_t key_size); // 通过键值查找 heapk_malloc() 的申请的内存地址
size_t  rtk_heapk_block_size(void *p);                                // 通过内存封装地址获取 heapk_malloc() 申请时设定的内存大小
size_t  rtk_heapk_space_size(void *p);                                // 已申请内存块的实际占用空间大小（含所有控制信息）（字节）
void   *rtk_heapk_key_base(void *p);                                  // 通过内存封装地址获取 heapk_malloc() 申请时设定的键值地址
size_t  rtk_heapk_key_size(void *p);                                  // 通过内存封装地址获取 heapk_malloc() 申请时设定的键值的有效字节数

#ifdef __cplusplus
} /* extern "C" */
#endif /* #ifdef __cplusplus */

#endif
