/**
 * @file rtk_semaphore.h
 * @author LokLiang (lokliang@163.com)
 * @brief
 * @version 0.1
 * @date 2023-05-01
 *
 * @copyright Copyright (c) 2023
 *
 */

#ifndef __RTK_SEMAPHORE_H__
#define __RTK_SEMAPHORE_H__

#include "rtk_def.h"

#ifdef __cplusplus
extern "C"
{
#endif /* #ifdef __cplusplus */

typedef struct
{
    rtk_hdl_t hdl;
} rtk_sem_t;

typedef enum
{
    RTK_SEM_TYPE_FIFO,
    RTK_SEM_TYPE_PRIOR,
} rtk_sem_type;

rtk_err_t rtk_sem_create(rtk_sem_t *sem_handle, const char *name, size_t init_value, size_t max_value, rtk_sem_type type);
rtk_err_t rtk_sem_delete(rtk_sem_t *sem_handle);
rtk_err_t rtk_sem_clr(rtk_sem_t *sem_handle);

rtk_err_t rtk_sem_release(rtk_sem_t *sem_handle);
rtk_err_t rtk_sem_take(rtk_sem_t *sem_handle, rtk_tick_t wait_ticks);

rtk_err_t rtk_sem_set_value(rtk_sem_t *sem_handle, size_t new_value);
size_t    rtk_sem_get_value(rtk_sem_t *sem_handle);
void     *rtk_sem_peek_waiting(rtk_sem_t *sem_handle);

bool rtk_sem_is_valid(rtk_sem_t *sem_handle);

#ifdef __cplusplus
} /* extern "C" */
#endif /* #ifdef __cplusplus */

#endif
