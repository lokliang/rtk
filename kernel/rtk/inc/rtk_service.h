/**
 * @file rtk_service.h
 * @author LokLiang (lokliang@163.com)
 * @brief
 * @version 0.1
 * @date 2023-05-01
 *
 * @copyright Copyright (c) 2023
 *
 */

#ifndef __RTK_SERVER_H__
#define __RTK_SERVER_H__

#include "rtk_def.h"
#include "rtk_thread.h"

#ifdef __cplusplus
extern "C"
{
#endif /* #ifdef __cplusplus */

void rtk_isr_hardfault_check(void); // hard fault 时可调用以帮助诊断一些问题

void rtk_service_tick_pause(void); // 阻止内部滴答定时器服务的执行
void rtk_service_tick_start(void); // 允许内部滴答定时器服务的执行

rtk_err_t rtk_service_mem_check_start(rtk_tick_t pre_ticks, rtk_prior_t priority); // 创建内存（堆栈）监视服务（将占用一个线程）。系统启动后该线程默认为未开启
rtk_err_t rtk_service_mem_check_stop(void);                                        // 关闭内存（堆栈）监视服务

rtk_thread_t *rtk_service_get_idle_thread(void); // 返回空闲线程句柄

void rtk_service_print_sysinfo(void); // 打印出所有线程的状态

void rtk_service_print_trace(rtk_thread_t *thread_handle); // 打印接口调用痕迹

u8_t rtk_get_cpu_usage(void); // 获取 CPU 的使用率(%)

rtk_hdl_t rtk_find_handle(const char *name); // 查找任何包含名称的句柄

unsigned rtk_get_sys_ticks(void); // 获取系统从启动 rtk_tick_start() 服务以来经历的节拍数（需要定时器服务已启动）

unsigned rtk_get_remain_ticks(void); // 获取距离最近一个线程剩余的休眠时间

unsigned rtk_ticks_to_msec(unsigned ticks);
unsigned rtk_msec_to_ticks(unsigned ms);

#ifdef __cplusplus
} /* extern "C" */
#endif /* #ifdef __cplusplus */

#endif
