/**
 * @file rtk_scheduler.h
 * @author LokLiang (lokliang@163.com)
 * @brief
 * @version 0.1
 * @date 2023-05-01
 *
 * @copyright Copyright (c) 2023
 *
 */

#ifndef __RTK_SCHEDULER_H__
#define __RTK_SCHEDULER_H__

#include "rtk_def.h"

#ifdef __cplusplus
extern "C"
{
#endif /* #ifdef __cplusplus */

int rtk_entry(void *heap, size_t size); // 指定堆内存并运行内核（包含 rtk_scheduler_init() 和 rtk_scheduler_start()）。

rtk_err_t rtk_scheduler_init(void *heap, size_t size);
rtk_err_t rtk_scheduler_start(void);

void rtk_int_entry(void); // 标记进入中断
void rtk_int_exit(void);  // 标记退出中断

bool rtk_is_isr_context(void); // 获取当前是否在中断状态

size_t rtk_sched_suspend(void);
void   rtk_sched_resume(size_t nest);
void   rtk_sched_resume_auto(void);

size_t rtk_int_save(void);
void   rtk_int_restore(size_t nest);
void   rtk_int_restore_auto(void);

typedef enum
{
    RTK_SCHED_STATE_NOTSTART,
    RTK_SCHED_STATE_RUNNING,
    RTK_SCHED_STATE_SUSPEND,
} rtk_scheduler_state_t;

rtk_scheduler_state_t rtk_scheduler_state(void);

#ifdef __cplusplus
} /* extern "C" */
#endif /* #ifdef __cplusplus */

#endif
