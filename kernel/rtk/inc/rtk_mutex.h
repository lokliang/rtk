/**
 * @file rtk_mutex.h
 * @author LokLiang (lokliang@163.com)
 * @brief
 * @version 0.1
 * @date 2023-05-01
 *
 * @copyright Copyright (c) 2023
 *
 */

#ifndef __RTK_MUTEX_H__
#define __RTK_MUTEX_H__

#include "rtk_def.h"
#include "rtk_thread.h"

#ifdef __cplusplus
extern "C"
{
#endif /* #ifdef __cplusplus */

typedef struct
{
    rtk_hdl_t hdl;
} rtk_mutex_t;

rtk_err_t rtk_mutex_create(rtk_mutex_t *mutex_handle, const char *name); // 申请互斥量内存
rtk_err_t rtk_mutex_delete(rtk_mutex_t *mutex_handle);                   // 释放 rtk_mutex_create() 所申请的互斥量（注意在释放前应确保互斥量状态为空闲）。

rtk_err_t rtk_mutex_lock(rtk_mutex_t *mutex_handle, rtk_tick_t wait_ticks); // 获取令牌。TimeOut: 表示在多少个系统节拍内取得权限，若值为 0 表示无限等待
rtk_err_t rtk_mutex_unlock(rtk_mutex_t *mutex_handle);                      // 归还由 rtk_mutex_lock() 取得的上一个令牌。
rtk_err_t rtk_mutex_unlock_all(void);                                       // 归还当前线程的所有令牌。

rtk_thread_t *rtk_mutex_get_holder(rtk_mutex_t *mutex_handle); // 查询正在取得访问的线程句柄

bool rtk_mutex_is_valid(rtk_mutex_t *mutex_handle); // 查询互斥量是否胖猪

#ifdef __cplusplus
} /* extern "C" */
#endif /* #ifdef __cplusplus */

#endif
