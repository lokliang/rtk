/**
 * @file rtk_def.h
 * @author LokLiang (lokliang@163.com)
 * @brief
 * @version 0.1
 * @date 2023-05-01
 *
 * @copyright Copyright (c) 2023
 *
 */

#ifndef __RTK_DEF_H__
#define __RTK_DEF_H__

#include "sys_types.h"
#include <stdbool.h>

#ifdef __cplusplus
extern "C"
{
#endif /* #ifdef __cplusplus */

typedef void* rtk_hdl_t;

typedef int rtk_tick_t;

typedef int8_t rtk_prior_t;

typedef enum
{
    RTK_OK      = 0,
    RTK_FAIL    = 1,  // 操作流程错误
    RTK_TIMEOUT = 2,  // 超时
    RTK_NOMEM   = 3,  // 内存不足
    RTK_E_ISR   = -1, // 不允许中断使用
    RTK_E_HDL   = -2, // 句柄错误
    RTK_E_MEM   = -3, // 内存错误
    RTK_E_STACK = -4, // 内存损坏
    RTK_E_PARAM = -5, // 参数错误
} rtk_err_t;

typedef enum
{
    THREAD_STATE_CLOSED = 0, // 线程已关闭
    THREAD_STATE_RUNNING,    // 线程为运行状态
    THREAD_STATE_PENDING,    // 线程为就绪状态
    THREAD_STATE_DELAYED,    // 线程为延时（定时唤醒）状态
    THREAD_STATE_SUSPEND,    // 线程为挂起状态
    THREAD_STATE_WAIT_SEM,   // 线程为等待信号量
    THREAD_STATE_WAIT_MUTEX, // 线程为等待互斥量
} rtk_thread_state_t;

#define RTK_WAIT_FOREVER (~0U)

#ifdef __cplusplus
} /* extern "C" */
#endif /* #ifdef __cplusplus */

#endif
