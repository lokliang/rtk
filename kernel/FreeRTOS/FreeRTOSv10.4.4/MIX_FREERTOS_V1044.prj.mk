
MIX_FREERTOS-INC ?= y
MIX_FREERTOS-SRC ?= n

## heap_1 heap_2 heap_3 heap_4 heap_5
MIX_FREERTOS-HEAP_NAME ?= 

## ARM_CM0 ARM_CM3 ARM_CM4F Posix RV
MIX_FREERTOS-CPU_NAME ?= 

## RISCV_no_extensions RISCV_MTIME_CLINT_no_extensions RV32I_CLINT_no_extensions Pulpino_Vega_RV32M1RM
MIX_FREERTOS-EXT_NAME ?= 

## name for choice
MIX_FREERTOS_CPU-$(MIX_FREERTOS-CPU_NAME) = y
MIX_FREERTOS_EXT-$(MIX_FREERTOS-SRC)-$(MIX_FREERTOS-EXT_NAME) = y
MIX_FREERTOS_HEAP-$(MIX_FREERTOS-SRC)-$(MIX_FREERTOS-HEAP_NAME) = y

## include
inc-$(MIX_FREERTOS-INC) += Source/include/

## source
src-$(MIX_FREERTOS-SRC) += Source/

## heap
src-$(MIX_FREERTOS_HEAP-y-heap_1) += Source/portable/MemMang/heap_1.c
src-$(MIX_FREERTOS_HEAP-y-heap_2) += Source/portable/MemMang/heap_2.c
src-$(MIX_FREERTOS_HEAP-y-heap_3) += Source/portable/MemMang/heap_3.c
src-$(MIX_FREERTOS_HEAP-y-heap_4) += Source/portable/MemMang/heap_4.c
src-$(MIX_FREERTOS_HEAP-y-heap_5) += Source/portable/MemMang/heap_5.c

## portable
inc-$(MIX_FREERTOS_CPU-ARM_CM0) += Source/portable/GCC/ARM_CM0
src-$(MIX_FREERTOS_CPU-ARM_CM0) += Source/portable/GCC/ARM_CM0
src-$(MIX_FREERTOS_CPU-ARM_CM0) += rtk_cpu_cm.c

inc-$(MIX_FREERTOS_CPU-ARM_CM3) += Source/portable/GCC/ARM_CM3
src-$(MIX_FREERTOS_CPU-ARM_CM3) += Source/portable/GCC/ARM_CM3
src-$(MIX_FREERTOS_CPU-ARM_CM3) += rtk_cpu_cm.c

inc-$(MIX_FREERTOS_CPU-ARM_CM4F) += Source/portable/GCC/ARM_CM4F
src-$(MIX_FREERTOS_CPU-ARM_CM4F) += Source/portable/GCC/ARM_CM4F
src-$(MIX_FREERTOS_CPU-ARM_CM4F) += rtk_cpu_cm.c

inc-$(MIX_FREERTOS_CPU-Posix) += Source/portable/ThirdParty/GCC/Posix/*
src-$(MIX_FREERTOS_CPU-Posix) += Source/portable/ThirdParty/GCC/Posix/*
src-$(MIX_FREERTOS_CPU-Posix) += rtk_cpu_posix.c

inc-$(MIX_FREERTOS_CPU-RV) += Source/portable/GCC/RISC-V/
src-$(MIX_FREERTOS_CPU-RV) += Source/portable/GCC/RISC-V/
src-$(MIX_FREERTOS_CPU-RV) += rtk_cpu_risc-v.c
inc-$(MIX_FREERTOS_EXT-y-RISCV_no_extensions)             += Source/portable/GCC/RISC-V/chip_specific_extensions/RISCV_no_extensions/
inc-$(MIX_FREERTOS_EXT-y-RISCV_MTIME_CLINT_no_extensions) += Source/portable/GCC/RISC-V/chip_specific_extensions/RISCV_MTIME_CLINT_no_extensions/
inc-$(MIX_FREERTOS_EXT-y-RV32I_CLINT_no_extensions)       += Source/portable/GCC/RISC-V/chip_specific_extensions/RV32I_CLINT_no_extensions/
inc-$(MIX_FREERTOS_EXT-y-Pulpino_Vega_RV32M1RM)           += Source/portable/GCC/RISC-V/chip_specific_extensions/Pulpino_Vega_RV32M1RM/
