#undef CONFIG_SYS_LOG_LEVEL
#define CONFIG_SYS_LOG_LEVEL SYS_LOG_LEVEL_INF
#define SYS_LOG_DOMAIN "app"
#include "sys_log.h"

int app_main(void)
{
    SYS_LOG_INF("app sample");
    return 0;
}
