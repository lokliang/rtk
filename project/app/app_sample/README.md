[简要](../../../README.md)

# 参考例程-APP

## 工程说明

1. 演示一个新工程的开始
2. 演示 prj-none.conf, prj-kit.conf, prj-rtk.conf 三种基本配置
   - prj-none.conf 裸机运行
   - prj-kit.conf 基于微内核运行
   - prj-rtk.conf 基于实时内核 RTK 运行
3. 演示组件：fatfs, xz, shell

## 运行环境

- Ubuntu
- MDK仿真

## 配置和运行

- 编译
```bash
$ ./build.sh
```

### 配置一

- 配置
```bash
Proj:  project/app/app_sample
Board: x86
Chip:  x86/linux
Rule:  x86/ubuntu64
ToolChain: gcc
prj-conf = prj-none.conf
```
```bash
Proj:  project/app/app_sample
Board: x86
Chip:  x86/linux
Rule:  x86/ubuntu64
ToolChain: gcc
prj-conf = prj-kit.conf
```
```bash
Proj:  project/app/app_sample
Board: x86
Chip:  x86/linux
Rule:  x86/ubuntu64
ToolChain: gcc
prj-conf = prj-rtk.conf
```

- 运行
```bash
$ ./debug/project.elf
[INF] | [app] app_main -> app sample

sh:/> 
```

### 配置二

- 配置
```bash
Proj:  project/app/app_sample
Board: qemu_cm3
Chip:  arm/qemu_cm3
Rule:  arm/cm3
ToolChain: arm-none-eabi-gcc
prj-conf = prj.conf
```
```bash
Proj:  project/app/app_sample
Board: qemu_cm3
Chip:  arm/qemu_cm3
Rule:  arm/cm3
ToolChain: arm-none-eabi-gcc
prj-conf = prj.conf
```
```bash
Proj:  project/app/app_sample
Board: qemu_cm3
Chip:  arm/qemu_cm3
Rule:  arm/cm3
ToolChain: arm-none-eabi-gcc
prj-conf = prj-rtk.conf
```

- 运行
```bash
$ qemu-system-arm -kernel ./debug/project.elf -machine mps2-an385 -d cpu_reset -nographic -monitor null -semihosting --semihosting-config enable=on,target=native -serial stdio -s
[INF] | [app] app_main -> app sample
```
