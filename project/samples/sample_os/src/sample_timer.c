#include "samples.h"

SHELL_CMD_FN(_os_sample_timer);

SHELL_REGISTER_CMD(
    register_sample_timer,
    SHELL_SETUP_CMD("sample-timer", "OS API example of: timer", _os_sample_timer, NULL), //
);

static os_timer_t s_timer1_handle;
static os_timer_t s_timer2_handle;
static os_timer_t s_timer3_handle;

static void _timer_1(void *arg)
{
    sh_t *sh_hdl = arg;
    os_time_t time = os_get_sys_time();
    printf("%s: time up. sys run time: %d.%03d second\r\n", __FUNCTION__, time / 1000, time % 1000);
}

static void _timer_2(void *arg)
{
    sh_t *sh_hdl = arg;
    os_time_t time = os_get_sys_time();
    printf("%s: time up. sys run time: %d.%03d second\r\n", __FUNCTION__, time / 1000, time % 1000);
}

static void _timer_3(void *arg)
{
    sh_t *sh_hdl = arg;
    os_time_t time = os_get_sys_time();
    printf("%s: time up. sys run time: %d.%03d second\r\n", __FUNCTION__, time / 1000, time % 1000);
}

SHELL_CMD_FN(_os_sample_timer)
{
    tag();

    do
    {
        printf("create timers\r\n");
        os_timer_create(&s_timer1_handle, _timer_1, sh_hdl);
        os_timer_create(&s_timer2_handle, _timer_2, sh_hdl);
        os_timer_create(&s_timer3_handle, _timer_3, sh_hdl);

        printf("set the period\r\n");
        os_timer_set_period(&s_timer1_handle, OS_TIMER_ONCE, 100);
        os_timer_set_period(&s_timer2_handle, OS_TIMER_PERIODIC, 200);
        os_timer_set_period(&s_timer3_handle, OS_TIMER_PERIODIC, 300);

        printf("start timers\r\n");
        os_timer_start(&s_timer1_handle);
        os_timer_start(&s_timer2_handle);
        os_timer_start(&s_timer3_handle);

        os_thread_sleep(2000);

        printf("stop timers\r\n");
        os_timer_stop(&s_timer1_handle);
        os_timer_stop(&s_timer2_handle);
        os_timer_stop(&s_timer3_handle);

        printf("sample complete. delete timers\r\n");
        os_timer_delete(&s_timer1_handle);
        os_timer_delete(&s_timer2_handle);
        os_timer_delete(&s_timer3_handle);

        return 0;

    } while (0);
}
