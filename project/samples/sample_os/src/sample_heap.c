#include "samples.h"

SHELL_CMD_FN(_os_sample_heap);

SHELL_REGISTER_CMD(
    register_sample_heap,
    SHELL_SETUP_CMD("sample-heap", "OS API example of: heap", _os_sample_heap, NULL), //
);

SHELL_CMD_FN(_os_sample_heap)
{
    tag();

    printf("allocation memory\r\n");
    void *mem = os_malloc(100);

    if (mem)
    {
        printf("operate success. memory addr = %p\r\n", mem);

        printf("now release this memory\r\n");
        os_free(mem);

        return 0;
    }
    else
    {
        printf("operate fail!\r\n");
        return -1;
    }
}
