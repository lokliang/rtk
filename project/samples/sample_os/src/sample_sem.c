#include "samples.h"

SHELL_CMD_FN(_os_sample_semaphore);

SHELL_REGISTER_CMD(
    register_sample_semaphore,
    SHELL_SETUP_CMD("sample-semaphore", "OS API example of: semaphore", _os_sample_semaphore, NULL), //
);

static os_thread_t s_thread1_handle;
static os_sem_t s_sem_handle;

static void _thread_1(void *arg)
{
    sh_t *sh_hdl = arg;
    os_thread_t thread;
    const char *thread_name;

    thread.handle = os_thread_get_self();
    thread_name = os_thread_get_name(&thread);
    printf("this is '%s' running\r\n", thread_name);

    printf("%s: waiting the samphore ..\r\n", thread_name);
    if (os_sem_take(&s_sem_handle, OS_WAIT_FOREVER) != OS_OK)
    {
        SYS_LOG_ERR("takeing semaphore fail!");
        os_thread_delete(NULL);
        return;
    }

    printf("%s: take samphore success, waiting to take again ...\r\n", thread_name);
    if (os_sem_take(&s_sem_handle, OS_WAIT_FOREVER) != OS_OK)
    {
        SYS_LOG_ERR("takeing semaphore fail!");
        os_thread_delete(NULL);
        return;
    }

    printf("%s: take samphore success\r\n", thread_name);

    printf("%s: run complete\r\n", thread_name);
    os_thread_delete(NULL);
}

SHELL_CMD_FN(_os_sample_semaphore)
{
    tag();

    os_thread_t thread;
    const char *thread_name;

    thread.handle = os_thread_get_self();
    thread_name = os_thread_get_name(&thread);

    do
    {
        printf("create a semaphore object\r\n");
        if (os_sem_create(&s_sem_handle, 0, 1) != OS_OK)
        {
            break;
        }

        printf("you can release semaphore any time\r\n");
        os_sem_release(&s_sem_handle);

        printf("create and wait thread-1 to run ...\r\n");
        os_thread_create(&s_thread1_handle,
                         "thread-1",
                         _thread_1,
                         sh_hdl,
                         0x400,
                         OS_PRIORITY_LOW);
        os_thread_sleep(100);

        printf("this is '%s' running\r\n", os_thread_get_name(&thread));

        printf("release semaphore again and wait ...\r\n");
        os_sem_release(&s_sem_handle);
        os_thread_sleep(100);

        printf("%s: run complete\r\n", thread_name);

        printf("objects can be deleted when they are no longer used\r\n");
        os_sem_delete(&s_sem_handle);

        return 0;

    } while (0);

    os_sem_delete(&s_sem_handle);

    printf("operate fail!\r\n");
    return -1;
}
