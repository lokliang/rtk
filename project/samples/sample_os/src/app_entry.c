#define SYS_LOG_DOMAIN "app"
#include "sys_log.h"

int app_main(void)
{
#if !defined(MIX_OS)
    SYS_LOG_WRN("Not configured os.");
    SYS_PRINT("\tThis sample program is the demonstration code of the OS.\r\n"
              "\tYou need to use the following command to configure a correct program:\r\n"
              "\t\033[34m./build.sh -p prj -p os\033[0m\r\n");
#endif
    return 0;
}
