[简要](../../../README.md)

# 参考例程-内核抽象层

## 工程说明

1. 演示定义 shell 命令
2. 演示 OS 内核抽象层接口
3. 演示分别使用 FreeRTOS 和 RTK 实现 OS 接口
4. 演示使用多个配置文件
   - prj.conf 配置一
   - os-fr.conf 配置二：使用 FreeRTOS 实现 OS 接口的配置
   - os-rtk.conf 配置二：使用 RTK 实现 OS 接口的配置
5. 通过 shell 命令，演示微内核 kit 和实时内核 RTK 的基本接口功能

## 运行环境

- Ubuntu
- MDK仿真

## 配置和运行

- 编译
```bash
$ ./build.sh -n -p prj -p os
```

### 配置一

- 配置
```bash
Proj:  project/samples/sample_os
Board: x86
Chip:  x86/linux
Rule:  x86/ubuntu64
ToolChain: gcc
prj-conf = prj.conf
os-conf = os-rtk.conf
```

- 运行
```bash
$ ./debug/project.elf

sh:/> 
```

### 配置二

- 配置
```bash
Proj:  project/samples/sample_os
Board: qemu_cm3
Chip:  arm/qemu_cm3
Rule:  arm/cm3
ToolChain: arm-none-eabi-gcc
prj-conf = prj.conf
os-conf = os-fr.conf
```
```bash
Proj:  project/samples/sample_os
Board: qemu_cm3
Chip:  arm/qemu_cm3
Rule:  arm/cm3
ToolChain: arm-none-eabi-gcc
prj-conf = prj.conf
os-conf = os-rtk.conf
```

- 运行
```bash
$ qemu-system-arm -kernel ./debug/project.elf -machine mps2-an385 -d cpu_reset -nographic -monitor null -semihosting --semihosting-config enable=on,target=native -serial stdio -s

sh:/>
```
