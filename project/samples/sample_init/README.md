[简要](../../../README.md)

# 参考例程-自动初始化

## 工程说明

1. 演示一个新工程的开始
2. 演示自动初始化宏的使用效果
   - INIT_EXPORT_BOARD
   - INIT_EXPORT_PREV
   - INIT_EXPORT_DEVICE
   - INIT_EXPORT_COMPONENT
   - INIT_EXPORT_ENV
   - INIT_EXPORT_APP

## 运行环境

- Ubuntu
- MDK仿真

## 配置和运行

- 编译
```bash
$ ./build.sh
```

### 配置一

- 配置
```bash
Proj:  project/samples/sample_init
Board: x86
Chip:  x86/linux
Rule:  x86/ubuntu64
ToolChain: gcc
prj-conf = prj.conf
```

- 运行
```bash
$ ./debug/project.elf
[INF] | [init] _init_board_template -> done.
[INF] | [init] _init_prev_template -> done.
[INF] | [init] _init_device_template -> done.
[INF] | [init] _init_component_template -> done.
[INF] | [init] _init_env_template -> done.
[INF] | [init] _init_app_template1 -> done.
[INF] | [init] _init_app_template2 -> done.
[INF] | [init] _init_app_template3 -> done.
```

### 配置二

- 配置
```bash
Proj:  project/samples/sample_init
Board: qemu_cm3
Chip:  arm/qemu_cm3
Rule:  arm/cm3
ToolChain: arm-none-eabi-gcc
prj-conf = prj.conf
```

- 运行
```bash
$ qemu-system-arm -kernel ./debug/project.elf -machine mps2-an385 -d cpu_reset -nographic -monitor null -semihosting --semihosting-config enable=on,target=native -serial stdio -s
[INF] | [init] _init_board_template -> done.
[INF] | [init] _init_prev_template -> done.
[INF] | [init] _init_device_template -> done.
[INF] | [init] _init_component_template -> done.
[INF] | [init] _init_env_template -> done.
[INF] | [init] _init_app_template1 -> done.
[INF] | [init] _init_app_template2 -> done.
[INF] | [init] _init_app_template3 -> done.
```
