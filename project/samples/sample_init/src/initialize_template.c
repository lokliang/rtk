#include "sys_init.h"
#include "sys_log.h"

static int _init_board_template(void)
{
    SYS_LOG_INF("done.");
    return 0;
}
INIT_EXPORT_BOARD(_init_board_template);

static int _init_prev_template(void)
{
    SYS_LOG_INF("done.");
    return 0;
}
INIT_EXPORT_PREV(_init_prev_template);

static int _init_device_template(void)
{
    SYS_LOG_INF("done.");
    return 0;
}
INIT_EXPORT_DEVICE(_init_device_template);

static int _init_component_template(void)
{
    SYS_LOG_INF("done.");
    return 0;
}
INIT_EXPORT_COMPONENT(_init_component_template);

static int _init_env_template(void)
{
    SYS_LOG_INF("done.");
    return 0;
}
INIT_EXPORT_ENV(_init_env_template);

static int _init_app_template3(void)
{
    SYS_LOG_INF("done.");
    return 0;
}
static int _init_app_template1(void)
{
    SYS_LOG_INF("done.");
    return 0;
}
static int _init_app_template2(void)
{
    SYS_LOG_INF("done.");
    return 0;
}
INIT_EXPORT_APP(_init_app_template2, 11);
INIT_EXPORT_APP(_init_app_template1, 10);
INIT_EXPORT_APP(_init_app_template3, 12);
