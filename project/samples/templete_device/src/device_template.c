#include "device_template.h"

static void _set_data(device_template_t dev, int data);
static int _get_data(device_template_t dev);

static device_template_cfg_t s_config;

static struct device_template const s_device = {
    .api = {
        .set_data = _set_data,
        .get_data = _get_data,
    },
    .cfg = &s_config,
};
OBJ_EXPORT_DEVICE(s_device, "DRV1");

static void _set_data(device_template_t dev, int data)
{
    dev->cfg->data = data;
}

static int _get_data(device_template_t dev)
{
    return dev->cfg->data;
}
