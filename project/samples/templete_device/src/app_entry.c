#undef CONFIG_SYS_LOG_LEVEL
#define CONFIG_SYS_LOG_LEVEL SYS_LOG_LEVEL_INF
#define SYS_LOG_DOMAIN "app"
#include "sys_log.h"

#include "device_template.h"
#include "board_config.h"

int app_main(void)
{
    device_template_t dev = device_template_binding("DRV1");

    for (int i = 0; i < 3; i++)
    {
        template_set_data(dev, i + '0');
        int result = template_get_data(dev);
        SYS_LOG_DBG("result = %d\r\n", result);
        SYS_ASSERT(result == i + '0', "result:%d i:%d", result, i);
    }

    SYS_LOG_INF("complete\r\n");

    return 0;
}
