#ifndef __DEVICE_TEMPLATE_H__
#define __DEVICE_TEMPLATE_H__

/* Includes ------------------------------------------------------------------*/

#include "sys_init.h"

#ifdef __cplusplus
extern "C"
{
#endif /* #ifdef __cplusplus */

/* Macros --------------------------------------------------------------------*/

/* Typedef -------------------------------------------------------------------*/

typedef const struct device_template *device_template_t;

typedef struct
{
    void (*set_data)(device_template_t dev, int data);
    int (*get_data)(device_template_t dev);
} device_template_api_t;

typedef struct
{
    int data;
} device_template_cfg_t;

struct device_template
{
    device_template_api_t api;
    device_template_cfg_t *cfg;
};

/* Function prototypes -------------------------------------------------------*/

__static_inline device_template_t device_template_binding(const char *name);
__static_inline const char *device_template_search(const char *prefix_name, unsigned int num);

__static_inline void template_set_data(device_template_t dev, int data);
__static_inline int template_get_data(device_template_t dev);

/* Code ----------------------------------------------------------------------*/

__static_inline device_template_t device_template_binding(const char *name)
{
    return (device_template_t)device_binding(name);
}

__static_inline const char *device_template_search(const char *prefix_name, unsigned int num)
{
    return device_search(prefix_name, num);
}

__static_inline void template_set_data(device_template_t dev, int data)
{
    if (__likely(dev))
        dev->api.set_data(dev, data);
}

__static_inline int template_get_data(device_template_t dev)
{
    if (__likely(dev))
        return dev->api.get_data(dev);
    else
        return -1;
}

#ifdef __cplusplus
} /* extern "C" */
#endif /* #ifdef __cplusplus */

#endif
