[简要](../../../README.md)

# 参考例程-模块模板

## 工程说明

1. 创建设备类型对象的参考模板
2. 演示绑定对象和使用

## 运行环境

- Ubuntu
- MDK仿真

## 配置和运行

- 编译
```bash
$ ./build.sh
```

### 配置一

- 配置
```bash
Proj:  project/samples/templete_device
Board: x86
Chip:  x86/linux
Rule:  x86/ubuntu64
ToolChain: gcc
prj-conf = prj.conf
```

- 运行
```bash
 ./debug/project.elf
[INF] | [app] device_template_binding -> Object: 'DRV1'

[INF] | [app] app_main -> complete

```

### 配置二

- 配置
```bash
Proj:  project/samples/templete_device
Board: qemu_cm3
Chip:  arm/qemu_cm3
Rule:  arm/cm3
ToolChain: arm-none-eabi-gcc
prj-conf = prj.conf
```

- 运行
```bash
$ qemu-system-arm -kernel ./debug/project.elf -machine mps2-an385 -d cpu_reset -nographic -monitor null -semihosting --semihosting-config enable=on,target=native -serial stdio -s
[INF] | [app] device_template_binding -> Object: 'DRV1'

[INF] | [app] app_main -> complete

```
