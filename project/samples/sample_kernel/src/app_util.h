#ifndef __APP_UTIL_H__
#define __APP_UTIL_H__

#include "sys_types.h"
#include "sys_log.h"
#include "components/shell.h"

#if defined(MIX_RTK)
#include "rtk.h"
#elif defined(MIX_K_KIT)
#include "kk.h"
#endif

#define tag() printf("\033[34m%s:%d\033[0m\r\n", _FILENAME(__FILE__), __LINE__)

#define printf(...) shell_print(sh_hdl, __VA_ARGS__)

#endif
