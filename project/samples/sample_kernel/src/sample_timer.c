#include "samples.h"

SHELL_CMD_FN(_kk_sample_timer);

SHELL_REGISTER_CMD(
    register_sample_timer,
    SHELL_SETUP_CMD("sample-timer", "Kernel API example of: timer", _kk_sample_timer, NULL), //
);

static kk_timer_t s_timer1_handle;
static kk_timer_t s_timer2_handle;
static kk_timer_t s_timer3_handle;

static void _timer_1(void *arg)
{
    sh_t *sh_hdl = arg;
    kk_time_t time = kk_get_sys_ticks();
    printf("%s: time up. sys run time: %d.%03d second\r\n", __FUNCTION__, time / 1000, time % 1000);
}

static void _timer_2(void *arg)
{
    sh_t *sh_hdl = arg;
    kk_time_t time = kk_get_sys_ticks();
    printf("%s: time up. sys run time: %d.%03d second\r\n", __FUNCTION__, time / 1000, time % 1000);
}

static void _timer_3(void *arg)
{
    sh_t *sh_hdl = arg;
    kk_time_t time = kk_get_sys_ticks();
    printf("%s: time up. sys run time: %d.%03d second\r\n", __FUNCTION__, time / 1000, time % 1000);
}

SHELL_CMD_FN(_kk_sample_timer)
{
    tag();

    do
    {
        printf("create timers\r\n");
        kk_timer_create(&s_timer1_handle, _timer_1, sh_hdl);
        kk_timer_create(&s_timer2_handle, _timer_2, sh_hdl);
        kk_timer_create(&s_timer3_handle, _timer_3, sh_hdl);

        printf("set the period and start timers\r\n");
        kk_timer_start(&s_timer1_handle, false, 100);
        kk_timer_start(&s_timer2_handle, true, 200);
        kk_timer_start(&s_timer3_handle, true, 300);

        kk_work_sleep(2000);

        printf("stop timers\r\n");
        kk_timer_stop(&s_timer1_handle);
        kk_timer_stop(&s_timer2_handle);
        kk_timer_stop(&s_timer3_handle);

        printf("sample complete. delete timers\r\n");
        kk_timer_delete(&s_timer1_handle);
        kk_timer_delete(&s_timer2_handle);
        kk_timer_delete(&s_timer3_handle);

        return 0;

    } while (0);
}
