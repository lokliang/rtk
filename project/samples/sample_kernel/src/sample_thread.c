#if defined(MIX_RTK)

#include "samples.h"

SHELL_CMD_FN(_rtk_sample_thread);

SHELL_REGISTER_CMD(
    register_sample_thread,
    SHELL_SETUP_CMD("sample-thread", "Kernel API example of: thread", _rtk_sample_thread, NULL), //
);

static rtk_thread_t s_thread1_handle;
static rtk_thread_t s_thread2_handle;

static void _thread_1(void *arg)
{
    sh_t *sh_hdl = arg;
    rtk_thread_t *thread;
    const char *thread_name;

    thread = rtk_thread_get_self();
    thread_name = rtk_thread_get_name(thread);

    while (1)
    {
        rtk_tick_t time = rtk_get_sys_ticks();
        printf("this is '%s' running. sys run time: %d.%03d second\r\n", thread_name, time / 1000, time % 1000);
        rtk_thread_sleep(333);
    }
}

static void _thread_2(void *arg)
{
    sh_t *sh_hdl = arg;
    rtk_thread_t *thread;
    const char *thread_name;

    thread = rtk_thread_get_self();
    thread_name = rtk_thread_get_name(thread);

    while (1)
    {
        rtk_tick_t time = rtk_get_sys_ticks();
        printf("this is '%s' running. sys run time: %d.%03d second\r\n", thread_name, time / 1000, time % 1000);
        rtk_thread_sleep(1000);
    }
}

SHELL_CMD_FN(_rtk_sample_thread)
{
    tag();

    do
    {
        printf("create threads\r\n");
        rtk_thread_create(&s_thread1_handle,
                          "thread-1",
                          _thread_1,
                          sh_hdl,
                          0x400,
                          1,
                          0);
        rtk_thread_create(&s_thread2_handle,
                          "thread-2",
                          _thread_2,
                          sh_hdl,
                          0x400,
                          1,
                          0);

        rtk_thread_sleep(3000);

        printf("sample complete. delete threads\r\n");
        rtk_thread_delete(&s_thread1_handle);
        rtk_thread_delete(&s_thread2_handle);

        return 0;

    } while (0);
}

#endif /* #if defined(MIX_RTK) */
