#include "samples.h"

SHELL_CMD_FN(_kk_sample_queue);

SHELL_REGISTER_CMD(
    register_sample_queue,
    SHELL_SETUP_CMD("sample-queue", "Kernel API example of: queue", _kk_sample_queue, NULL), //
);

#define QUEUE_LEN 10

struct sample_struct
{
    uint8_t index;
    uint32_t data;
};

static kk_queue_t s_queue_handle;

SHELL_CMD_FN(_kk_sample_queue)
{
    tag();

    struct sample_struct queue_data;

    do
    {
        printf("create a queue object\r\n");
        if (kk_queue_create(&s_queue_handle, QUEUE_LEN, sizeof(struct sample_struct)) != 0)
        {
            break;
        }

        printf("copy data to the queue object\r\n");
        queue_data.index = 1;
        queue_data.data = 0x12345678;
        if (kk_queue_send(&s_queue_handle, &queue_data) != 0)
        {
            break;
        }

        printf("copy data to the queue object agaen\r\n");
        queue_data.index = 2;
        queue_data.data = 0x9abcdef0;
        if (kk_queue_send(&s_queue_handle, &queue_data) != 0)
        {
            break;
        }

        printf("try to copy data from queue object\r\n");
        if (kk_queue_recv(&s_queue_handle, &queue_data) != 0)
        {
            break;
        }
        if (queue_data.index != 1)
        {
            break;
        }
        printf("the test data value = 0x%x\r\n", queue_data.data);

        printf("try to copy data from queue object again\r\n");
        if (kk_queue_recv(&s_queue_handle, &queue_data) != 0)
        {
            break;
        }
        if (queue_data.index != 2)
        {
            break;
        }
        printf("the test data value = 0x%x\r\n", queue_data.data);

        printf("return -1 when the queue object is empty\r\n");
        if (kk_queue_recv(&s_queue_handle, &queue_data) == 0)
        {
            break;
        }

        printf("objects can be deleted when they are no longer used\r\n");
        kk_queue_delete(&s_queue_handle);

        printf("operate success\r\n");
        return 0;

    } while (0);

    kk_queue_delete(&s_queue_handle);

    printf("operate fail!\r\n");
    return -1;
}
