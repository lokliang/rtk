[简要](../../../README.md)

# 参考例程-内核

## 工程说明

1. 演示定义 shell 命令
2. 通过 shell 命令，演示微内核 kit 和实时内核 RTK 的基本接口功能

## 运行环境

- Ubuntu
- MDK仿真

## 配置和运行

- 编译
```bash
$ ./build.sh
```

### 配置一

- 配置
```bash
Proj:  project/samples/sample_kernel
Board: x86
Chip:  x86/linux
Rule:  x86/ubuntu64
ToolChain: gcc
prj-conf = prj-kit.conf
```
```bash
Proj:  project/samples/sample_kernel
Board: x86
Chip:  x86/linux
Rule:  x86/ubuntu64
ToolChain: gcc
prj-conf = prj-rtk.conf
```

- 运行
```bash
$ ./debug/project.elf

sh:/> 
```

### 配置二

- 配置
```bash
Proj:  project/samples/sample_kernel
Board: qemu_cm3
Chip:  arm/qemu_cm3
Rule:  arm/cm3
ToolChain: arm-none-eabi-gcc
prj-conf = prj-kit.conf
```
```bash
Proj:  project/samples/sample_kernel
Board: qemu_cm3
Chip:  arm/qemu_cm3
Rule:  arm/cm3
ToolChain: arm-none-eabi-gcc
prj-conf = prj-rtk.conf
```

- 运行
```bash
$ qemu-system-arm -kernel ./debug/project.elf -machine mps2-an385 -d cpu_reset -nographic -monitor null -semihosting --semihosting-config enable=on,target=native -serial stdio -s

sh:/> 
```
