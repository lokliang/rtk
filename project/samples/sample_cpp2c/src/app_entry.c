#include "cpp2c.h"

#define SYS_LOG_DOMAIN "app"
#include "sys_log.h"

int app_main(void)
{
    handle_t hdl;

    hdl = demo_dog_init();
    demo_dog_sound(hdl);
    demo_dog_deinit(hdl);

    hdl = demo_cat_init();
    demo_cat_sound(hdl);
    demo_cat_deinit(hdl);

    return 0;
}
