/**
 * @file cpp_polymorphism.h
 * @author LokLiang (lokliang@163.com)
 * @brief C++ 示例
 * @version 0.1
 * @date 2024-12-17
 *
 * @copyright Copyright (c) 2024
 *
 * https://www.runoob.com/cplusplus/cpp-polymorphism.html
 *
 * 多态
 *
 */

#define CONFIG_SYS_LOG_LEVEL SYS_LOG_LEVEL_DBG
#define SYS_LOG_DOMAIN "TEST"
#include "sys_log.h"

// 基类 Animal
class Animal
{
public:
    Animal()
    {
        SYS_LOG_INF("Animal born");
    }

    // 虚函数 sound，为不同的动物发声提供接口
    virtual void sound() const
    {
        SYS_LOG_INF("Animal makes a sound");
    }

    // 在具有多态行为的基类中，析构函数应该声明为 virtual，以确保在删除派生类对象时调用派生类的析构函数，防止资源泄漏。
    virtual ~Animal()
    {
        SYS_LOG_INF("Animal destroyed");
    }
};

// 派生类 Dog，继承自 Animal
class Dog : public Animal
{
public:
    Dog()
    {
        SYS_LOG_INF("Dog born");
    }

    // 重写 sound 方法
    void sound() const override
    {
        SYS_LOG_INF("Dog barks");
    }

    ~Dog()
    {
        SYS_LOG_INF("Dog destroyed");
    }
};

// 派生类 Cat，继承自 Animal
class Cat : public Animal
{
public:
    Cat()
    {
        SYS_LOG_INF("Cat born");
    }

    // 重写 sound 方法
    void sound() const override
    {
        SYS_LOG_INF("Cat meows");
    }

    ~Cat()
    {
        SYS_LOG_INF("Cat destroyed");
    }
};
