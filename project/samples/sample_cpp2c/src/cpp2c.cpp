/**
 * @file cpp2c.cpp
 * @author LokLiang (lokliang@163.com)
 * @brief 使用 C 调用 C++ 示例
 * @version 0.1
 * @date 2024-12-17
 *
 * @copyright Copyright (c) 2024
 *
 */

#include "cpp_polymorphism.h"

extern "C"
{
#include "cpp2c.h"

    handle_t demo_dog_init(void)
    {
        return new Dog();
    }

    void demo_dog_sound(handle_t hdl)
    {
        ((Dog *)hdl)->sound();
    }

    void demo_dog_deinit(handle_t hdl)
    {
        delete (Dog *)hdl;
    }

    handle_t demo_cat_init(void)
    {
        return new Cat();
    }

    void demo_cat_sound(handle_t hdl)
    {
        ((Cat *)hdl)->sound();
    }

    void demo_cat_deinit(handle_t hdl)
    {
        delete (Cat *)hdl;
    }
}
