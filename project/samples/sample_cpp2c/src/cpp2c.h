/**
 * @file cpp2c.h
 * @author LokLiang (lokliang@163.com)
 * @brief 使用 C 调用 C++ 示例
 * @version 0.1
 * @date 2024-12-17
 *
 * @copyright Copyright (c) 2024
 *
 */

#pragma once

typedef void *handle_t;

handle_t demo_dog_init(void);
void demo_dog_sound(handle_t hdl);
void demo_dog_deinit(handle_t hdl);

handle_t demo_cat_init(void);
void demo_cat_sound(handle_t hdl);
void demo_cat_deinit(handle_t hdl);
