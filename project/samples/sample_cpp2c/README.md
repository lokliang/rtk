[简要](../../../README.md)

# 参考例程-C代码调用C++代码

## 工程说明

1. 演示一用 C 写的代码如何调用 C++ 代码的一种简单思路。

## 运行环境

- Ubuntu

## 配置和运行

- 编译
```bash
$ ./build.sh
```

### 配置

- 配置
```bash
Proj:  project/samples/sample_cpp2c
Board: x86
Chip:  x86/linux
Rule:  x86/ubuntu64
ToolChain: gcc
prj-conf = prj.conf
```

- 运行
```bash
$ ./debug/project.elf
[INF] | [TEST] cpp_polymorphism.h:11    Animal =>       Animal born
[INF] | [TEST] cpp_polymorphism.h:33    Dog =>  Dog born
[INF] | [TEST] cpp_polymorphism.h:39    sound =>        Dog barks
[INF] | [TEST] cpp_polymorphism.h:44    ~Dog => Dog destroyed
[INF] | [TEST] cpp_polymorphism.h:23    ~Animal =>      Animal destroyed
[INF] | [TEST] cpp_polymorphism.h:11    Animal =>       Animal born
[INF] | [TEST] cpp_polymorphism.h:54    Cat =>  Cat born
[INF] | [TEST] cpp_polymorphism.h:60    sound =>        Cat meows
[INF] | [TEST] cpp_polymorphism.h:65    ~Cat => Cat destroyed
[INF] | [TEST] cpp_polymorphism.h:23    ~Animal =>      Animal destroyed
```
